#!/usr/bin/env python

# blank areas of a fits image given a region file

# @author: Francesco de Gasperin, modified by Alexander Drabent

def input2strlist_nomapfile(invar):
   """ 
   from bin/download_IONEX.py
   give the list of MSs from the list provided as a string
   """
   str_list = None
   if type(invar) is str:
       if invar.startswith('[') and invar.endswith(']'):
           str_list = [f.strip(' \'\"') for f in invar.strip('[]').split(',')]
       else:
           str_list = [invar.strip(' \'\"')]
   elif type(invar) is list:
       str_list = [str(f).strip(' \'\"') for f in invar]
   else:
       raise TypeError('input2strlist: Type '+str(type(invar))+' unknown!')
   return str_list

def getPhaseCentre(filename):
    """
    Get the phase centre (in degrees) of the first source of an MS.
    """

    from casacore import tables
    import numpy as np

    field_no = 0
    ant_no   = 0
    with tables.table(filename + "/FIELD", ack = False) as field_table:
        direction = field_table.getcol("PHASE_DIR")
    RA        = direction[ant_no, field_no, 0]
    Dec       = direction[ant_no, field_no, 1]

    if (RA < 0):
        RA += 2 * np.pi

    return (np.degrees(RA), np.degrees(Dec))

def main(msinput, outfile, fwhm, pbcut = None, tonull = False):
    """
    Create a ds9 region of the beam
    outfile : str
        output file
    pbcut : float, optional
        diameter of the beam
    tonull : bool, optional
        arrive to the first null, not the FWHM
    freq: min,max,med 
        which frequency to use to estimate the beam size
    """

    import pyregion
    from pyregion.parser_helper import Shape

    print('Making PB region: ' + outfile)

    msname = input2strlist_nomapfile(msinput)[0]
    ra, dec = getPhaseCentre(msname)

    if pbcut is None:
        radius = fwhm / 2.
    else:
        radius = pbcut / 2.

    if tonull: radius *= 2 # rough estimation

    s = Shape('circle', None)
    s.coord_format = 'fk5'
    s.coord_list = [ ra, dec, radius ] # ra, dec, radius
    s.coord_format = 'fk5'
    s.attr = ([], {'width': '2', 'point': 'cross',
                   'font': '"helvetica 16 normal roman"'})
    s.comment = 'color=red text="beam"'

    regions = pyregion.ShapeList([s])
    regions.write(outfile)

if __name__=='__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Set to "blankval" all the pixels inside the given region')
    
    parser.add_argument('msinput', type=str, nargs='+', help='Input Measurement Set')
    parser.add_argument('--outfile', type=str, help='Name of output file')
    parser.add_argument('--fwhm', type=float, help="The FHWM of the image")
    parser.add_argument('--pbcut', type=float, default=None, help='Diameter of the beam, default: None')
    parser.add_argument('--tonull', type=bool, default=False, help='arrive to the first null, not the FWHM, default: False')
        
    args = parser.parse_args()

    main(args.msinput, args.outfile, fwhm = args.fwhm, pbcut = args.pbcut, tonull = args.tonull)