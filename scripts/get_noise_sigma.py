#! /usr/bin/env python
"""
Script to estimate noise of input/output data
"""

import numpy as np
import argparse
import casacore.tables as ctab
import subprocess as sb
import time,glob
import astropy.time as atime


# default DP3
default_DP3='DP3'

class NoiseSampler:
    DP3=default_DP3

    def __init__(self,inMS,outMS,timesec,Nf=3,path_DP3=None):
        self.in_mslist=glob.glob(inMS)
        self.in_col='DATA'
        self.out_mslist=glob.glob(outMS)
        self.out_col='DATA'
        self.in_extracted_ms=list()
        self.out_extracted_ms=list()
        self.timesec=timesec
        self.Nf=Nf
        if path_DP3:
            self.DP3=path_DP3

        self.in_sI_sigma=None
        self.out_sI_sigma=None

    def extract_dataset(self):
        self.in_mslist.sort()
        self.out_mslist.sort()
        msname=self.in_mslist[0]
        # Parset for extracting and averaging
        parset_sample='extract_sample.parset'
        parset=open(parset_sample,'w+')
        # in order to overcome storage manager issues, make a 
        # copy of data, only antenna 1
        parset.write('steps=[fil]\n'
           +'fil.type=filter\n'
           +'fil.baseline=\"0,0 &&\"\n'
           +'fil.remove=True\n'
           +'msin.datacolumn=DATA\n')
        parset.close()
        # remove old files
        sb.run('rm -rf L_SB0.MS',shell=True)
        # now process first selected MS
        MS='L_SB0.MS'
        proc1=sb.Popen(self.DP3+' '+parset_sample+' msin='+msname+' msout='+MS, shell=True)
        proc1.wait()
        msname=MS

        tt=ctab.table(msname,readonly=True)
        starttime= tt[0]['TIME']
        endtime=tt[tt.nrows()-1]['TIME']
        self.N=tt.nrows()
        tt.close()
        Nms=len(self.in_mslist)

        assert(Nms==len(self.out_mslist))
        # need to have at least Nf MS
        assert(Nms>=self.Nf)

        parset=open(parset_sample,'w+')
        # sample time interval
        t_start=np.random.rand()*(endtime-starttime)+starttime
        t_end=t_start+self.timesec
        t0=atime.Time(t_start/(24*60*60),format='mjd',scale='utc')
        dt=t0.to_datetime()
        str_tstart=str(dt.year)+'/'+str(dt.month)+'/'+str(dt.day)+'/'+str(dt.hour)+':'+str(dt.minute)+':'+str(dt.second)
        t0=atime.Time(t_end/(24*60*60),format='mjd',scale='utc')
        dt=t0.to_datetime()
        str_tend=str(dt.year)+'/'+str(dt.month)+'/'+str(dt.day)+'/'+str(dt.hour)+':'+str(dt.minute)+':'+str(dt.second)

        parset.write('steps=[fil]\n'
           +'fil.type=filter\n'
           +'fil.baseline=[CR]S*&\n'
           +'fil.remove=True\n'
           +'msin.datacolumn=DATA\n'
           +'msin.starttime='+str_tstart+'\n'
           +'msin.endtime='+str_tend+'\n')
        parset.close()

        # process subset of MS from mslist
        in_submslist=list()
        out_submslist=list()
        in_submslist.append(self.in_mslist[0])
        out_submslist.append(self.out_mslist[0])
        aa=list(np.random.choice(np.arange(1,Nms-1),self.Nf-2,replace=False))
        aa.sort()
        for ms_id in aa:
          in_submslist.append(self.in_mslist[ms_id])
          out_submslist.append(self.out_mslist[ms_id])
        in_submslist.append(self.in_mslist[-1])
        out_submslist.append(self.out_mslist[-1])

        # remove old files
        sb.run('rm -rf inL_SB*.MS',shell=True)
        sb.run('rm -rf outL_SB*.MS',shell=True)
        # now process each of selected MS
        self.in_extracted_ms=list()
        self.out_extracted_ms=list()
        for ci in range(self.Nf):
           MS='inL_SB'+str(ci)+'.MS'
           proc1=sb.Popen(self.DP3+' '+parset_sample+' msin='+in_submslist[ci]+' msout='+MS, shell=True)
           proc1.wait()
           self.in_extracted_ms.append(MS)
           self.reset_weights(MS)
           MS='outL_SB'+str(ci)+'.MS'
           proc1=sb.Popen(self.DP3+' '+parset_sample+' msin='+out_submslist[ci]+' msout='+MS, shell=True)
           proc1.wait()
           self.out_extracted_ms.append(MS)
           self.reset_weights(MS)

    # reset weights to 1, after applying weight to datum
    def reset_weights(self,msname):
        tt=ctab.table(msname,readonly=False)
        data=tt.getcol('DATA')
        if 'WEIGHT_SPECTRUM' in tt.colnames():
          weight=tt.getcol('WEIGHT_SPECTRUM')
        else:
          weight=tt.getcol('IMAGING_WEIGHT')
        # weight ~sigma^2, std(weight) ~ sigma^2, so sqrt()
        weight_std=np.sqrt(weight.std())
        data *=weight_std
        weight /=weight_std
        tt.putcol('DATA',data)
        tt.putcol('WEIGHT_SPECTRUM',weight)
        tt.close()

    def get_noise_all(self):
        self.in_sI_sigma=np.zeros(self.Nf)
        self.out_sI_sigma=np.zeros(self.Nf)
        for ms,ii in zip(self.in_extracted_ms,np.arange(self.Nf)):
           self.in_sI_sigma[ii]=self.get_noise_var(ms,self.in_col)
        for ms,ii in zip(self.out_extracted_ms,np.arange(self.Nf)):
           self.out_sI_sigma[ii]=self.get_noise_var(ms,self.out_col)

    # extract noise info
    def get_noise_var(self,msname,column):
        tt=ctab.table(msname,readonly=True)
        t1=tt.query('ANTENNA1 != ANTENNA2',columns=column+',FLAG')
        data0=t1.getcol(column)
        # set nans to 0
        flag=t1.getcol('FLAG')
        flag[np.isnan(flag)]=1
        data=data0*(1-flag)
        tt.close()
        # set nans to 0
        data[np.isnan(data)]=0.
        # form IQUV
        sI=(data[:,:,0]+data[:,:,3])*0.5
        #sQ=(data[:,:,0]-data[:,:,3])*0.5
        #sU=(data[:,:,1]-data[:,:,2])*0.5
        #sV=(data[:,:,1]+data[:,:,2])*0.5
        #return sI.std(),sQ.std(),sU.std(),sV.std()
        return sI.std()


    def __del__(self):
        for ms in self.in_extracted_ms:
           sb.run('rm -rf '+ms,shell=True)
        for ms in self.out_extracted_ms:
           sb.run('rm -rf '+ms,shell=True)
        sb.run('rm -rf L_SB0.MS',shell=True)

def main(args):
    ns=NoiseSampler(args.input,args.output,args.time_interval,Nf=args.subbands,path_DP3=args.DP3)
    ns.extract_dataset()
    ns.get_noise_all()
    print(f'Noise STD for {ns.Nf} subbands: input {np.mean(ns.in_sI_sigma)} output {np.mean(ns.out_sI_sigma)}')

if __name__=='__main__':
    parser=argparse.ArgumentParser(
      description='Estimate noise from sampled data',
      formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument('--input',type=str,metavar='\'*.MS\'',
        help='absolute path of MS pattern to use as input')
    parser.add_argument('--output',type=str,metavar='\'*.MS\'',
        help='absolute path of MS pattern to use as output (must match the input size)')
    parser.add_argument('--subbands',default=3,type=int,metavar='f',
        help='number of random subbands to process (minimum 3)')
    parser.add_argument('--time_interval',type=float,default=30.,metavar='t',
        help='total random time interval to sample in seconds')
    parser.add_argument('--DP3',type=str,metavar='DP3',default=default_DP3,
        help='path to DP3 command')

    args=parser.parse_args()
    if args.input and args.output:
      main(args)
    else:
      parser.print_help()
