#!/usr/bin/env python

# blank areas of a fits image given a region file

# @author: Francesco de Gasperin, modified by Alexander Drabent

def flatten(f, channel = 0, freqaxis = 0):
    """
    Flatten a fits file so that it becomes a 2D image. Return new header and data
    """
    from astropy import wcs
    import numpy as np

    naxis=f[0].header['NAXIS']
    if (naxis < 2):
        raise RadioError("Can\'t make map from this")
    if (naxis == 2):
        return f[0].header,f[0].data

    w               = wcs.WCS(f[0].header)
    wn              = wcs.WCS(naxis = 2)

    wn.wcs.crpix[0] = w.wcs.crpix[0]
    wn.wcs.crpix[1] = w.wcs.crpix[1]
    wn.wcs.cdelt    = w.wcs.cdelt[0:2]
    wn.wcs.crval    = w.wcs.crval[0:2]
    wn.wcs.ctype[0] = w.wcs.ctype[0]
    wn.wcs.ctype[1] = w.wcs.ctype[1]

    header = wn.to_header()
    header["NAXIS"] = 2
    header["NAXIS1"] = f[0].header['NAXIS1']
    header["NAXIS2"] = f[0].header['NAXIS2']
    copy=('EQUINOX','EPOCH')
    for k in copy:
        r = f[0].header.get(k)
        if (r):
            header[k] = r

    slicing = []
    for i in range(naxis,0,-1):
        if (i <= 2):
            slicing.append(np.s_[:],)
        elif (i == freqaxis):
            slicing.append(channel)
        else:
            slicing.append(0)

    # slice=(0,)*(naxis-2)+(np.s_[:],)*2
    return header, f[0].data[tuple(slicing)]

def main(input_image, region, outfile = None, inverse = False, blankval = 0., operation = "AND"):
    """
    Set to "blankval" all the pixels inside the given region
    if inverse=True, set to "blankval" pixels outside region.
    If a list of regions is provided the operation is applied to each region one after the other

    input_image: fits file
    region: ds9 region or list of regions
    outfile: output name
    inverse: reverse final *combined* mask
    blankval: pixel value to set
    operation: how to combine multiple regions with AND or OR
    """

    import astropy.io.fits as pyfits
    import numpy as np
    import pyregion


    if outfile == None: outfile = input_image[0]
    if not type(region) is list: region=[region]

    # open fits
    with pyfits.open(input_image[0]) as fits:
        origshape    = fits[0].data.shape
        header, data = flatten(fits)
        sum_before   = np.sum(data)
        if (operation == 'AND'):
            total_mask = np.ones(shape = data.shape).astype(bool)
        if (operation == 'OR'):
            total_mask = np.zeros(shape = data.shape).astype(bool)
        for this_region in region:
            # extract mask
            r    = pyregion.open(this_region)
            mask = r.get_mask(header=header, shape=data.shape)
            if (operation == 'AND'):
                total_mask = total_mask & mask
            if (operation == 'OR'):
                total_mask = total_mask | mask
        if (inverse):
            total_mask = ~total_mask
        data[total_mask] = blankval
        # save fits
        fits[0].data = data.reshape(origshape)
        fits.writeto(outfile, overwrite=True)

    print("%s: Blanking (%s): sum of values: %f -> %f" % (outfile, region, sum_before, np.sum(data)))

if __name__=='__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Set to "blankval" all the pixels inside the given region')
    
    parser.add_argument('input_image', type=str, nargs='+', help='Input FITS image')
    parser.add_argument('region', type=str, nargs='+', help='Input region file')
    parser.add_argument('--outfile', type=str, default=None, help='Name of output file, default: None')
    parser.add_argument('--inverse', type=bool, default=False, help='Reverse final combined mask')
    parser.add_argument('--blankval', type=float, default=0., help='Pixel value to be set, default: 0.0')
    parser.add_argument('--operation', type=str, default='AND', help='How to combine multiple regions with AND or OR, default: AND')
        
    args = parser.parse_args()

    main(args.input_image, args.region, outfile = args.outfile, inverse = args.inverse, blankval = args.blankval, operation = args.operation)