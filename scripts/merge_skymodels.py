#!/usr/bin/env python
"""
Append a LOFAR skymodel to an existing one
"""

import os, logging
import lsmtool

########################################################################

def main(inmodel1, inmodel2, outmodel = 'output.skymodel', radius=30/3600, keep='from2'):
    """
    Merge two LOFAR skymodel text files.

    Parameters
    ----------
    inmodel1 : str
        Name (path) of input model #1
    inmodel2 : str
        Name (path) of input model #2
    outmodel : str, optional
        Name (path) of output merged model
    radius : float, optional
        Matching radius in degrees for determining duplicates
    keep : str, optional
        When duplicates are found, keep those from model #1 ('from1') or from
        model #2 ('from2')
    """

    logging.info('Reading ' + inmodel1)
    s1 = lsmtool.load(inmodel1)
    logging.info('Reading ' + inmodel2)
    s2 = lsmtool.load(inmodel2)

    if s1.getPatchNames() is None:
        s1.group('single')
    if s2.getPatchNames() is None:
        s2.group('single')

    logging.info('Adding skymodel ' + inmodel2 + ' to ' + inmodel1)

    # The following call will merge the two models, identifing (by position) duplicate
    # sources/components that are present in both
    s1.concatenate(s2, matchBy='position', radius=radius, keep=keep)
    s1.setPatchPositions()
    s1.write(outmodel)

    return(0)

########################################################################
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Merge two LOFAR skymodel text files.')

    parser.add_argument('--inmodel1', type=str, default=None, help='input/output skymodel.')
    parser.add_argument('--inmodel2', type=str, default=None, help='skymodel to append')
    parser.add_argument('--outmodel', type=str, default='output.skymodel', help='output skymodel name')
    parser.add_argument('--radius', type=float, default=30/3600, help='matching radius in degrees')
    parser.add_argument('--keep', type=str, default='from2', help='keep duplicates from model 1 or 2')

    args = parser.parse_args()

    format_stream = logging.Formatter("%(asctime)s\033[1m %(levelname)s:\033[0m %(message)s","%Y-%m-%d %H:%M:%S")
    format_file   = logging.Formatter("%(asctime)s %(levelname)s: %(message)s","%Y-%m-%d %H:%M:%S")
    logging.root.setLevel(logging.INFO)

    main(inmodel1 = os.path.expandvars(args.inmodel1), inmodel2 = os.path.expandvars(args.inmodel2),
         outmodel = os.path.expandvars(args.outmodel), radius=args.radius, keep=args.keep)
