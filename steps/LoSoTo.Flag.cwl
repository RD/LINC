#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.2
id: losoto_flag

doc: |
  This operation for LoSoTo implement a flagging procedure
  WEIGHT: compliant


requirements:
  InlineJavascriptRequirement:
    expressionLib:
      - { $include: utils.js}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: $(get_losoto_config('FLAG').join('\n'))

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - --verbose
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: astronrd/linc

inputs:
  - id: input_h5parm
    type: File
  - id: soltab
    type: string
    doc: "Solution table"
  - id: axesToFlag
    type: string[]
    doc: Axes along which to smooth+find outlier (e.g. ['time', 'freq']), max 2 values.
  - id: order
    type: int[]
    doc: |
      Order of the function fitted during detrending.
      Array must have same size of axesToFlag.
      If mode=smooth these are the window of the running median (0=all axis).
  - id: maxCycles
    type: int?
    doc: Max number of independent flagging cycles, by default 5.
  - id: maxRms
    type: float?
    doc: Rms to clip outliers, by default 5.
  - id: maxRmsNoise
    type: float?
    doc: |
      Do a running rms and then flag those regions that have a rms higher
      than MaxRmsNoise*rms_of_rmses, by default 0 (ignored).
  - id: fixRms
    type: float?
    doc: Instead of calculating rms use this value, by default 0 (ignored).
  - id: fixRmsNoise
    type: float?
    doc: |
      Instead of calculating rms of the rmses use this value
      (it will not be multiplied by the MaxRmsNoise), by default 0 (ignored).
  - id: windowNoise
    type: int?
    doc: Window size for the running rms, by default 11.

  - id: replace
    type: boolean?
    doc: |
      Replace bad values with the interpolated ones,
      instead of flagging them. By default False.
  - id: preflagzeros
    type: boolean?
    doc:
      Flag zeros/ones (bad solutions in BBS/DP3).
      They should be flagged at import time. By default False.
  - id: mode
    type: string?
    doc: |
      Detrending/fitting algorithm: smooth / poly / spline.
      By default smooth.
  - id: refAnt
    type: string?
    doc: Reference antenna, by default None.
  - id: ncpu
    type: int?
    doc: Number of cpu to use, by default all available.

outputs:
  - id: output_h5parm
    type: File
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
  - id: log
    type: File[]
    outputBinding:
      glob: '$(inputs.input_h5parm.basename)-losoto*.log'

stdout: $(inputs.input_h5parm.basename)-losoto.log
stderr: $(inputs.input_h5parm.basename)-losoto_err.log
