class: CommandLineTool
cwlVersion: v1.2
id: plot_demix
baseCommand:
  - plot_demixing_solutions.py
inputs:
  - id: instrument_tables
    type: Directory[]?
    inputBinding:
      position: 0
      prefix: '--instrument_tables'
      itemSeparator: ','
      valueFrom: "[$(self.map(function(directory){ return directory.path; }).join(','))]"
  - id: clip_solutions
    type: float?

label: plot_demix_solutions

outputs:
  - id: demix_images
    doc: Output image
    type: File[]
    outputBinding:
      glob: 'demix_solutions_*.png'
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'plot_demix_solutions*.log'

hints:
  - class: DockerRequirement
    dockerPull: 'astronrd/linc'
requirements:
  - class: InlineJavascriptRequirement
stdout: plot_demix_solutions.log
stderr: plot_demix_solutions_err.log
