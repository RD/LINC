class: CommandLineTool
cwlVersion: v1.2
id: compare_station_list
baseCommand:
  - python3
  - compare_station_list.py
inputs:
    - id: msin
      type: Directory[]
      doc: MS to compare with
      inputBinding:
        position: 0
    - id: h5parmdb
      type: File?
      doc: H5parm database to compare with
    - id: solset_name
      type: string?
      doc: Name of the H5parm solset
      default: 'sol000'
    - id: filter
      type: string?
      default: '*&'
      doc: Filter these baselines for the comparison
    - id: station_list
      type: string?
      doc: Provide a station list manually
requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: false
      - entryname: compare_station_list.py
        entry: |
          null = None
          import sys
          import json
          from compareStationList import main as compareStationList

          mss = sys.argv[1:]
          inputs = json.loads(r"""$(inputs)""")
          if inputs['h5parmdb']:
             h5parmdb = inputs['h5parmdb']['path']
          else:
             h5parmdb = None
          solset_name = inputs['solset_name']
          filter = inputs['filter']
          station_list = inputs['station_list']

          output = compareStationList(mss, h5parmdb, solset_name, filter, station_list)

          filter_out = output['filter']
          match_out  = output['match']
          cwl_output = {"filter_out": filter_out, "match_out": match_out}

          with open('./out.json', 'w') as fp:
              json.dump(cwl_output, fp)
outputs:
  - id: filter_out
    type: string
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).filter_out)
  - id: match_out
    type: string
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).match_out)
  - id: msout
    doc: Output MS
    type: Directory[]
    outputBinding:
      outputEval: $(inputs.msin)
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'compareStationList*.log'
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
stdout: compareStationList.log
stderr: compareStationList_err.log
