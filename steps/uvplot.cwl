class: CommandLineTool
cwlVersion: v1.2
id: uvplot
baseCommand:
  - plot_uvcov.py
inputs:
  - id: MSfiles
    type:
      - Directory[]
      - Directory
    inputBinding:
      position: 2
      shellQuote: false
    doc: List of MS for UV-plotting
  - id: output_name
    type: string?
    default: 'uv-coverage.png'
    inputBinding:
      position: 1
      prefix: '--output'
      shellQuote: false
    doc: Name of the output image
  - id: title
    default: 'uv-coverage'
    type: string?
    inputBinding:
      position: 1
      prefix: '--title'
      shellQuote: false
    doc: Title of the plots
  - id: limits
    default: ',,,'
    type: string?
    inputBinding:
      position: 1
      prefix: '--limits'
      shellQuote: false
  - id: timeslots
    default: '0,10,0'
    type: string?
    inputBinding:
      position: 1
      prefix: '--timeslots'
      shellQuote: false
  - id: antennas
    default: '-1'
    type: string?
    inputBinding:
      position: 1
      prefix: '--antennas'
      shellQuote: false
  - id: wideband
    default: true
    type: boolean?
    inputBinding:
      position: 0
      prefix: '--wideband True'
      shellQuote: false
outputs:
  - id: output_image
    doc: Plot of the uv-coverage
    type: File[]
    outputBinding:
      glob: "*$(inputs.output_name.split('.').pop())"
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'uvplot*.log'
label: uvplot
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
requirements:
  - class: ShellCommandRequirement
  - class: InlineJavascriptRequirement
stdout: uvplot.log
stderr: uvplot_err.log