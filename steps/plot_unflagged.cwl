class: CommandLineTool
cwlVersion: v1.2
id: plot_unflagged
baseCommand:
  - python3
inputs:
  - id: frequencies
    type: float[]
  - id: unflagged_fraction
    type: float[]
      
label: plot_unflagged
arguments:
  - '-c'
  - |
    import json
    import numpy
    import matplotlib.pyplot as plt

    inputs = json.loads(r"""$(inputs)""")

    freq_list = numpy.array(inputs['frequencies']) / 1e6 # MHz
    frac_list = inputs['unflagged_fraction']

    # Plot the unflagged fraction vs. frequency
    plt.scatter(freq_list, frac_list)
    plt.xlabel('frequency [MHz]')
    plt.ylabel('unflagged fraction')
    plt.savefig('unflagged_fraction.png')

outputs:
  - id: output_imag
    doc: Output image
    type: File
    outputBinding:
      glob: 'unflagged_fraction.png'
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'plot_unflagged_fraction*.log'

hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
requirements:
  - class: InlineJavascriptRequirement
stdout: plot_unflagged_fraction.log
stderr: plot_unflagged_fraction_err.log