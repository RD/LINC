class: CommandLineTool
cwlVersion: v1.2
id: taql
baseCommand:
  - taql
inputs:
  - id: msin
    doc: Input Measurement Set
    type: Directory
    inputBinding:
      position: 2
      shellQuote: false
  - id: task
    doc: Input main task name
    default: "UPDATE"
    type: string?
    inputBinding:
      position: 1
      prefix: "'"
      separate: false
      shellQuote: false
  - id: command
    doc: Command with respect to the input task
    type: string
    inputBinding:
      position: 3
      shellQuote: false
      valueFrom: $(inputs.command+"'")
outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)
  - id: logfile
    doc: Output logfile
    type: File[]
    outputBinding:
      glob: 'taql*.log'
label: TaQL
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
requirements:
  - class: ShellCommandRequirement
  - class: InplaceUpdateRequirement
    inplaceUpdate: true 
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
  - class: InlineJavascriptRequirement
  - class: ResourceRequirement
    coresMin: 2

stdout: taql.log
stderr: taql_err.log