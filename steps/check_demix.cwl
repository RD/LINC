class: CommandLineTool
cwlVersion: v1.2
id: check_demix
baseCommand:
  - python3
  - check_demix.py
inputs:
  - id: skymodel
    type: File?
  - id: demix_sources
    type: string[]?
    default:
      - VirA_4_patch
      - CygAGG
      - CasA_4_patch
      - TauAGG
  - id: clip_sources
    type: string[]?
    default:
      - VirA_4_patch
      - CygAGG
      - CasA_4_patch
      - TauAGG
  - id: demix
    type: boolean?
    inputBinding:
        valueFrom: '$(self ? 1 : 0)'
  - id: Ateam_separation_file
    type: File
  - id: clipAteam
    type: boolean?
    default: true

outputs:
  - id: out_demix
    type: boolean
    outputBinding:
      loadContents: true
      glob: 'out.json'
      outputEval: '$(JSON.parse(self[0].contents).demix == 0 ? false : true)'
  - id: out_demix_sources
    type: string[]
    outputBinding:
      loadContents: true
      glob: 'out.json'
      outputEval: '$(JSON.parse(self[0].contents).demix_sources)'
  - id: out_clip_sources
    type: string[]
    outputBinding:
      loadContents: true
      glob: 'out.json'
      outputEval: '$(JSON.parse(self[0].contents).clip_sources)'
  - id: logfile
    type: File
    outputBinding:
      glob: check_demix.log
label: check_demix

requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
     - entryname: check_demix.py
       entry: |
        import json
        import os
        import lsmtool
        import numpy, math
        null = None

        targets = {'CasA'   : {'ra' : 6.123487680622104,  'dec' : 1.0265153995604648},   \
                   'CygA'   : {'ra' : 5.233686575770755,  'dec' : 0.7109409582180791},   \
                   'TauA'   : {'ra' : 1.4596748493730913, 'dec' : 0.38422502335921294},  \
                   'HerA'   : {'ra' : 4.4119087330382163, 'dec' : 0.087135562905816893}, \
                   'VirA'   : {'ra' : 3.276086511413598,  'dec' : 0.21626589533567378},  \
                   'Sun'    : {'ra' : 0,                  'dec' : 0                  },  \
                   'Jupiter': {'ra' : 0,                  'dec' : 0                  },  \
                   'Moon'   : {'ra' : 0,                  'dec' : 0                  }}

        inputs = json.loads(r"""$(inputs)""")

        demix                 = inputs['demix']
        demix_sources         = inputs['demix_sources']
        clip_sources          = inputs['clip_sources']
        skymodel              = inputs['skymodel']
        Ateam_separation_file = inputs['Ateam_separation_file']["path"]
        tolerance_arcmin      = 6.0
        clipAteam             = inputs['clipAteam']
        
        if not clipAteam:
            clip_sources = []
        if skymodel is None:
            skymodel = os.path.expandvars("$LINC_DATA_ROOT/skymodels/A-Team_Midres.skymodel")
        else:
            skymodel = skymodel["path"]
        s = lsmtool.load(skymodel)

        f = open(Ateam_separation_file, 'r')
        Ateam_separation = json.load(f)
        f.close()      

        if demix is None:
            demix      = 0
            demix_list = []
            for item in Ateam_separation:
                ra  = math.degrees(targets[item['source']]['ra'])
                dec = math.degrees(targets[item['source']]['dec'])
                dist_deg = s.getDistance(ra, dec, byPatch=True)
                if any(dist_deg * 60.0 < tolerance_arcmin):
                    for index in numpy.where(dist_deg * 60 < tolerance_arcmin)[0]:
                        patch_name = s.getPatchNames()[index]
                        if patch_name in demix_sources:
                            demix_list.append(patch_name)
                            demix = 1
            demix_sources = demix_list
        elif demix == 0:
            demix_sources = []
        if demix == 1:
            patches = s.getPatchPositions()
            clip_list = clip_sources.copy()
            for item in clip_list: ## change check to here
                (ra, dec) = patches[item]
                dist_deg = s.getDistance(ra.value, dec.value, byPatch=True)
                if any(dist_deg * 60.0 < 6.0):
                    for index in numpy.where(dist_deg * 60 < tolerance_arcmin)[0]:
                        patch_name = s.getPatchNames()[index]
                        if patch_name in demix_sources:
                            clip_sources.remove(item)
        
        print("The following sources will be demixed: ", demix_sources)
        print("The following sources will be clipped: ", clip_sources)

        cwl_output = {"demix_sources" : demix_sources,
                       "clip_sources" : clip_sources,
                       "demix"        : demix}

        with open('./out.json', 'w') as fp:
            json.dump(cwl_output, fp)

hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc

stdout: check_demix.log
stderr: check_demix_err.log
