class: CommandLineTool
cwlVersion: v1.2
id: make_beam_reg
label: make_beam_reg
baseCommand:
  - make_beam_reg.py
inputs:
  - id: input_ms
    type:
      - Directory
      - type: array
        items: Directory
    inputBinding:
      position: 0
    doc: Input image
  - id: outfile
    type: string
    doc: Output region file name
    inputBinding:
      position: 1
      prefix: --outfile
  - id: fwhm
    type: float
    inputBinding:
      position: 2
      prefix: '--fwhm'
    doc: The FHWM of the image
  - id: pb_cut
    type: float?
    inputBinding:
      position: 2
      prefix: '--pbcut'
    doc: Diameter of the beam
  - id: to_null
    type: boolean?
    default: False
    inputBinding:
      position: 2
      prefix: '--tonull'
    doc: arrive to the first null, not the FWHM
outputs:
  - id: region
    doc: output region file
    type: File
    outputBinding:
      glob: $(inputs.outfile)
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'make_beam_reg*.log'
hints:
 - class: DockerRequirement
   dockerPull: astronrd/linc
stderr: make_beam_reg.log
stdout: make_beam_reg_err.log
