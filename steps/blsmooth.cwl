class: CommandLineTool
cwlVersion: v1.2
id: blsmooth
label: BLsmooth
baseCommand:
  - BLsmooth.py
inputs:
  - id: msin
    type: Directory
    inputBinding:
      position: 1
    doc: Input measurement set
  - default: 0.01
    id: ionfactor
    type: float?
    inputBinding:
      position: 0
      prefix: '-f'
    doc: Gives an indication on how strong is the ionosphere
  - id: do_smooth
    type: boolean?
    default: true
    doc: 'If true performs smoothing'
    inputBinding:
      prefix: -S
      valueFrom: "$(self ? 'True': 'False')"
      separate: true
  - default: 1.0
    id: bscalefactor
    type: float?
    inputBinding:
      position: 0
      prefix: '-s'
    doc: Gives an indication on how the smoothing varies with
  - default: DATA
    id: in_column_name
    type: string?
    inputBinding:
      position: 0
      prefix: '-i'
    doc: Column name to smooth
  - default: SMOOTHED_DATA
    id: out_column
    type: string?
    inputBinding:
      position: 0
      prefix: '-o'
    doc: Output column
  - default: false
    id: weight
    type: boolean?
    inputBinding:
      position: 0
      prefix: '-w'
    doc: >-
      Save the newly computed WEIGHT_SPECTRUM, this action permanently modify
      the MS!
  - default: false
    id: restore
    type: boolean?
    inputBinding:
      position: 0
      prefix: '-r'
    doc: If WEIGHT_SPECTRUM_ORIG exists then restore it before smoothing
  - default: false
    id: nobackup
    type: boolean?
    inputBinding:
      position: 0
      prefix: '-b'
    doc: Do not backup the old WEIGHT_SPECTRUM in WEIGHT_SPECTRUM_ORIG
  - default: false
    id: onlyamp
    type: boolean?
    inputBinding:
      position: 0
      prefix: '-a'
    doc: Smooth only amplitudes
  - id: notime
    default: false
    type: boolean?
    inputBinding:
      position: 0
      prefix: '-t'
    doc: Do not smooth in time
  - id: nofreq
    default: false
    type: boolean?
    inputBinding:
      position: 0
      prefix: '-q'
    doc: Do not smooth in frequency
  - id: chunks
    default: 8
    type: int?
    inputBinding:
      position: 0
      prefix: '-c'
    doc: Split the I/O in n chunks. If you run out of memory, set this to a value > 2.
  - id: ncpu
    default: 4
    type: int?
    inputBinding:
      position: 0
      prefix: '-n'
    doc: Number of cores
outputs:
  - id: msout
    doc: MS set output
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)
  - id: logfile
    type: File
    outputBinding:
      glob: BLsmooth.log
hints:
 - class: DockerRequirement
   dockerPull: astronrd/linc
requirements:
 - class: InitialWorkDirRequirement
   listing:
    - entry: $(inputs.msin)
      writable: true
 - class: InplaceUpdateRequirement
   inplaceUpdate: true 
 - class: InlineJavascriptRequirement
stderr: BLsmooth.log
