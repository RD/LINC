class: CommandLineTool
cwlVersion: v1.2
id: applytarget
baseCommand:
  - DP3
inputs:
  - id: msin
    type: Directory
    inputBinding:
      position: 0
      prefix: msin=
      separate: false
    doc: Input Measurement Set
  - id: msout_name
    type: string
    default: '.'
    inputBinding:
      position: 0
      prefix: msout=
      separate: false
    doc: Name of output MS
  - default: DATA
    id: msin_datacolumn
    type: string
    inputBinding:
      position: 0
      prefix: msin.datacolumn=
      separate: false
    doc: Input data Column
  - id: parmdb
    type: File
    inputBinding:
      position: 0
      prefix: applycal.parmdb=
      separate: false
    doc: >-
      Path of parmdb in which the parameters are stored. This can also be an
      H5Parm file, in that case the filename has to end in '.h5'
  - id: msout_datacolumn
    type: string
    inputBinding:
      position: 0
      prefix: msout.datacolumn=
      separate: false
    doc: Output data column
  - default: gain
    id: correction
    type: string
    inputBinding:
      position: 0
      prefix: applycal.correction=
      separate: false
    doc: >
      Type of correction to perform. When using H5Parm, this is for now the name
      of the soltab; the type will be deduced from the metadata in that soltab,
      except for full Jones, in which case correction should be 'fulljones'.
  - default: 'sol000'
    id: solset
    type: string
    inputBinding:
      position: 0
      prefix: applycal.solset=
      separate: false
  - id: storagemanager
    type: string
    default: ""
    inputBinding:
      prefix: msout.storagemanager=
      separate: false
  - id: databitrate
    type: int?
    inputBinding:
       prefix: msout.storagemanager.databitrate=
       separate: false
  - id: updateweights
    type: string?
    inputBinding:
      position: 0
      prefix: applycal.updateweights=
      separate: false
  - id: save2json
    default: true
    type: boolean?
    inputBinding:
      position: 0
      prefix: count.savetojson=True
  - id: jsonfilename
    type: string?
    default: 'out.json'
    inputBinding:
      prefix: count.jsonfilename=
      separate: false
outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: '$(inputs.msout_name=="."?inputs.msin.basename:inputs.msout_name)'
  - id: flagged_fraction_dict
    type: string
    outputBinding:
        loadContents: true
        glob: $(inputs.jsonfilename)
        outputEval: $(JSON.parse(self[0].contents).flagged_fraction_dict)
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'applycal_$(inputs.correction).log'
stdout: applycal_$(inputs.correction).log
stderr: applycal_$(inputs.correction)_err.log
arguments:
  - 'steps=[applycal,count]'
requirements:
  - class: InlineJavascriptRequirement
  - class: ResourceRequirement
    coresMin: 2
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
