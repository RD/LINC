class: CommandLineTool
cwlVersion: v1.2
id: predict
baseCommand:
  - DP3
inputs:
  - id: max_dp3_threads
    type: int?
    inputBinding:
      position: 0
      prefix: numthreads=
      separate: false
  - id: msin
    type: Directory
    inputBinding:
      position: 0
      prefix: msin=
      separate: false
    doc: Input Measurement Set
  - default: DATA
    id: msin_datacolumn
    type: string?
    inputBinding:
      position: 0
      prefix: msin.datacolumn=
      separate: false
    doc: Input data Column
  - default: MODEL_DATA
    id: msout_datacolumn
    type: string?
    inputBinding:
      position: 0
      prefix: msout.datacolumn=
      separate: false
  - id: sources_db
    type:
      - File
      - Directory
    inputBinding:
      position: 0
      prefix: predict.sourcedb=
      separate: false
  - default: null
    id: sources
    type: string[]?
    inputBinding:
      position: 0
      prefix: predict.sources=
      separate: false
      itemSeparator: ','
      valueFrom: "[$(self.join(','))]"
  - default: false
    id: usebeammodel
    type: boolean?
    inputBinding:
      position: 0
      prefix: predict.usebeammodel=True
  - id: usechannelfreq
    default: true
    type: boolean?
    inputBinding:
      valueFrom: $(!self)
      position: 0
      prefix: predict.usechannelfreq=False
  - default: false
    id: onebeamperpatch
    type: boolean?
    inputBinding:
      position: 0
      prefix: predict.onebeamperpatch=True
  - default: null
    id: filter_baselines
    type: string?
    inputBinding:
      position: 0
      prefix: filter.baseline=
      separate: false
      valueFrom: $(self)
  - default: false
    id: filter_remove
    type: boolean?
    inputBinding:
      position: 0
      prefix: filter.remove=True
  - id: writefullresflag
    type: boolean?
    default: false
    inputBinding:
       prefix: msout.writefullresflag=True
  - default: default
    id: beammode
    type: string?
    inputBinding:
      position: 0
      prefix: predict.beammode=
      separate: false
  - id: overwrite
    type: boolean?
    default: false
    inputBinding:
       prefix: msout.overwrite=True
  - id: storagemanager
    type: string?
    default: ""
    inputBinding:
       prefix: msout.storagemanager=
       separate: false
  - id: databitrate
    type: int?
    inputBinding:
       prefix: msout.storagemanager.databitrate=
       separate: false
outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'filter_predict*.log'
arguments:
  - steps=[filter,predict,count]
  - msout=.
requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
  - class: InplaceUpdateRequirement
    inplaceUpdate: true
  - class: InlineJavascriptRequirement
  - class: ResourceRequirement
    coresMin: $(inputs.max_dp3_threads)
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
stdout: filter_predict.log
stderr: filter_predict_err.log
