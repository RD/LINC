class: CommandLineTool
cwlVersion: v1.2
id: dp3_addcol
baseCommand:
  - DP3
inputs:
  - id: max_dp3_threads
    doc: maximum amount of threads to be used for processing
    type: int?
    inputBinding:
      position: 0
      prefix: numthreads=
      separate: false
  - id: msin
    type: Directory
    inputBinding:
      position: 0
      prefix: msin=
      separate: false
    doc: Input Measurement Set
  - default: DATA
    id: msin_datacolumn
    type: string?
    inputBinding:
      position: 0
      prefix: msin.datacolumn=
      separate: false
    doc: Input data Column
  - id: msout_datacolumn
    doc: Output data column name
    default: CORRECTED_DATA
    type: string?
    inputBinding:
      position: 0
      prefix: msout.datacolumn=
      separate: false
outputs:
  - id: msout
    doc: Output Measurement Set
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)
  - id: logfile
    doc: Output logfile
    type: File[]
    outputBinding:
      glob: 'dp3_addcol*.log'
arguments:
  - steps=[]
  - msout=.
requirements:
  - class: InplaceUpdateRequirement
    inplaceUpdate: true 
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
  - class: ResourceRequirement
    coresMin: $(inputs.max_dp3_threads)
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
stdout: dp3_addcol.log
stderr: dp3_addcol_err.log
