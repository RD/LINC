#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.2
id: calib_rot_diag
baseCommand: DP3

arguments:
  - steps=[ddecal,count]

inputs:
  - id: max_dp3_threads
    doc: maximum amount of threads to be used for processing
    type: int?
    inputBinding:
      position: 0
      prefix: numthreads=
      separate: false
  - id: msin
    type: Directory[]
    doc: Input Measurement Set(s)
    inputBinding:
      position: 0
      prefix: msin=
      separate: false
      itemSeparator: ","
      valueFrom: "[$(self.map(function(directory){ return directory.basename; }).join(','))]"
  - id: msin_fname
    doc: Input Measurement Set string (including dummy.ms)
    type: string[]?
    inputBinding:
      position: 1
      prefix: msin=
      separate: false
      itemSeparator: ','
      valueFrom: "[$(self.join(','))]"
  - id: msin_datacolumn
    type: string?
    default: DATA
    doc: Input data Column
    inputBinding:
      prefix: msin.datacolumn=
      separate: false
  - id: msin_baseline
    type: string?
    doc: Baselines to be selected
    inputBinding:
      prefix: msin.baseline=
      separate: false

  - id: output_name_h5parm
    doc: Filename of output H5Parm
    type: string?
    default: instrument.h5
    inputBinding: 
      prefix: ddecal.h5parm=
      separate: false
  - id: msout_name
    type: string?
    doc: Output Measurement Set
    default: ''
    inputBinding:
      prefix: msout=
      separate: false

#--------------------
  - id: propagate_solutions
    doc: Initialize solver with the solutions of the previous time slot 
    type: boolean?
    default: true
    inputBinding:
      prefix: ddecal.propagatesolutions=True
  - id: propagate_converged_only
    doc: Propagate solutions of the previous time slot only if the solve converged
    type: boolean?
    default: true
    inputBinding:
      prefix: ddecal.propagateconvergedonly=True
  - id: modeldatacolumns
    doc: Use model data from the measurement set.
    type: string[]?
    default: []
    inputBinding:
      prefix: ddecal.modeldatacolumns=
      separate: false
      valueFrom: "[$(self.join(','))]"
      shellQuote: false
  - id: extradatacolumns
    doc: Optional extra data columns to read 
    type: string[]?
    default: []
    inputBinding:
      prefix: msin.extradatacolumns=
      separate: false
      valueFrom: "[$(self.join(','))]"
      shellQuote: false
  - id: directions
    doc: List of directions to calibrate on
    type: string[]?
    default: []
    inputBinding:
      prefix: ddecal.directions=
      separate: false
      valueFrom: "[$(self.join(','))]"
      shellQuote: false
  - id: reusemodel
    doc: List of model data to be reused from previous (ddecal) step(s). The list should contain the model data names, as set by a previous step
    type: string[]?
    default: []
    inputBinding:
      prefix: ddecal.reusemodel=
      separate: false
      valueFrom: "[$(self.join(','))]"
      shellQuote: false

  - id: flagunconverged
    type: boolean?
    default: false
    doc: |
      Flag unconverged solutions (i.e., those from solves that did not converge
      within maxiter iterations).
    inputBinding:
      prefix: ddecal.flagunconverged=True
  - id: flagdivergedonly
    default: true
    type: boolean?
    doc: |
      Flag only the unconverged solutions for which divergence was detected.
      At the moment, this option is effective only for rotation+diagonal
      solves, where divergence is detected when the amplitudes of any station
      are found to be more than a factor of 5 from the mean amplitude over all
      stations.
      If divergence for any one station is detected, all stations are flagged
      for that solution interval. Only effective when flagunconverged=true
      and mode=rotation+diagonal.
    inputBinding:
      prefix: ddecal.flagdivergedonly=True
  - id: storagemanager
    doc: What storage manager to use.
    type: string?
    default: ""
    inputBinding:
      prefix: msout.storagemanager=
      separate: false
  - id: mode
    type:
      type: enum
      symbols:
        - scalarcomplexgain
        - scalarphase
        - scalaramplitude
        - tec
        - tecandphase
        - fulljones
        - diagonal
        - phaseonly
        - amplitudeonly
        - rotation
        - rotation+diagonal
        - diagonalphase
    doc: |
      Type of constraint to apply. Options are
    inputBinding:
      prefix: ddecal.mode=
      separate: false
  - id: nchan
    doc: Number of channels in each channel block, for which the solution is assumed to be constant.
    type: int?
    default: 1
    inputBinding:
      position: 0
      prefix: ddecal.nchan=
      separate: false
  - id: maxiter
    doc: Maximum number of iterations
    type: int?
    default: 50
    inputBinding:
      position: 0
      prefix: ddecal.maxiter=
      separate: false
  - id: stepsize
    doc: Stepsize between iterations
    type: float?
    default: 0.2
    inputBinding:
      position: 0
      prefix: ddecal.stepsize=
      separate: false
  - id: tolerance
    doc: Controls the accuracy to be reached.
    type: float?
    default: 1e-3
    inputBinding:
      position: 0
      prefix: ddecal.tolerance=
      separate: false
  - id: uvmmax
    type: float?
    default: 1e15
    inputBinding:
      position: 0
      prefix: ddecal.uvmmax=
      separate: false
  - id: uvlambdamin
    doc: Ignore baselines / channels with UV < uvlambdamin wavelengths.
    type: float?
    default: 300.
    inputBinding:
      position: 0
      prefix: ddecal.uvlambdamin=
      separate: false
  - id: solint
    doc: Solution interval in timesteps
    type: int?
    default: 1
    inputBinding:
      position: 0
      prefix: ddecal.solint=
      separate: false
  - id: minvisratio
    doc: Minimum number of visibilities within a solution interval
    type: float?
    default: 0.0
    inputBinding:
      position: 0
      prefix: ddecal.minvisratio=
      separate: false
  - id: coreconstraint
    doc: When unequal to 0, all stations within the given distance from the reference station (0) will be constrained to have the same solution (in metres)
    default: 0.0
    type: float?
    inputBinding:
      position: 0
      prefix: ddecal.coreconstraint=
      separate: false
  - id: smoothnessconstraint
    doc: Kernel size in Hz
    type: float?
    default: 0.0
    inputBinding:
      position: 0
      prefix: ddecal.smoothnessconstraint=
      separate: false
  - id: smoothnessreffrequency
    doc: An optional reference frequency (in Hz) for the smoothness constraint
    type: float?
    default: 0.0
    inputBinding:
      position: 0
      prefix: ddecal.smoothnessreffrequency=
      separate: false
  - id: antennaconstraint
    doc: A list of lists specifying groups of antennas that are to be constrained to have the same solution.
    type: string[]?
    default: []
    inputBinding:
      position: 0
      prefix: ddecal.antennaconstraint=
      valueFrom: "[$(self.join(','))]"
      separate: false
  - id: approximatetec
    doc: Uses an approximation stage in which the phases are constrained with the piece-wise fitter, to solve local minima problems.
    type: boolean?
    default: false
    inputBinding:
      position: 0
      prefix: ddecal.approximatetec=True
  - id: maxapproxiter
    doc: Maximum number of iterations during approximating stage
    type: int?
    inputBinding:
      position: 0
      prefix: ddecal.maxapproxiter=
      separate: false
  - id: approxtolerance
    doc: Tolerance at which the approximating first stage is considered to be converged and the second full-constraining stage is started.
    type: float?
    inputBinding:
      position: 0
      prefix: ddecal.approxtolerance=
      separate: false
  - id: sourcedb
    type:
      - File?
      - Directory?
    inputBinding:
      position: 0
      prefix: ddecal.sourcedb=
      separate: false
  - id: usebeammodel
    default: false
    type: boolean?
    inputBinding:
      position: 0
      prefix: ddecal.usebeammodel=True
      separate: false
  - id: usechannelfreq
    default: true
    type: boolean?
    inputBinding:
      valueFrom: $(!self)
      position: 0
      prefix: ddecal.usechannelfreq=False
      separate: false
  - id: beammode
    type: string?
    inputBinding:
      position: 0
      prefix: ddecal.beammode=
      separate: false

#--------------------
  - id: save2json
    doc: Save statistics as JSON file
    default: true
    type: boolean?
    inputBinding:
      position: 0
      prefix: count.savetojson=True
  - id: jsonfilename
    doc: Define output file name for JSON statistics
    type: string?
    default: 'out.json'
    inputBinding:
      prefix: count.jsonfilename=
      separate: false

outputs:
  - id: msout ## needed due to cwltool issue 1785
    doc: Output Measurement Set 
    type: Directory[]
    outputBinding:
      outputEval: $(inputs.msin)

  - id: h5parm
    doc: Filename of output H5Parm (to be read by e.g. losoto)
    type: File
    outputBinding:
      glob: $(inputs.output_name_h5parm)
      
  - id: flagged_fraction_dict
    doc: Dictionary that contains the flagged fraction per station
    type: string
    outputBinding:
        loadContents: true
        glob: $(inputs.jsonfilename)
        outputEval: $(JSON.parse(self[0].contents).flagged_fraction_dict)

  - id: logfile
    doc: Output logfile
    type: File[]
    outputBinding:
      glob: 'ddecal*.log'

hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
requirements:
  - class: InlineJavascriptRequirement
  - class: ResourceRequirement
    coresMin: $(inputs.max_dp3_threads)
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: false

stdout: ddecal.log
stderr: ddecal_err.log
