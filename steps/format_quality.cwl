class: ExpressionTool
cwlVersion: v1.2
inputs:
- id: plots
  type: File[]
- id: summary
  type: File
  
outputs:
- id: quality
  type: Any
expression: |
  ${
      return {
       "quality": {
         "plots": inputs.plots,
         "details": {},
         "sensitivity": "N/A",
         "uv-coverage": "N/A",
         "observing-conditions": "N/A",
         "summary_file": inputs.summary
       }
    }
  }
  
requirements:
- class: InlineJavascriptRequirement
