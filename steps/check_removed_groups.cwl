class: CommandLineTool
cwlVersion: v1.2
id: check_removed_groups
baseCommand:
  - python3
arguments:
    - position: 0
      valueFrom: check_removed_groups.py
inputs:
  - id: groups_specification
    type: File
    inputBinding:
      position: 1
  - id: groupnames
    type: string[]
  - id: removed_bands
    type: string[]?
outputs:
  - id: out_groups_specification
    type: File
    outputBinding:
      glob: 'out.json'
  - id: out_groupnames
    type: string[]
    outputBinding:
      loadContents: true
      glob: 'groupnames.json'
      outputEval: '$(JSON.parse(self[0].contents).groupnames)'
label: check_removed_groups

requirements:
  - class: InitialWorkDirRequirement
    listing:
     - entryname: check_removed_groups.py
       entry: |
        import sys
        import json
        import os

        null = []

        inputs = json.loads(r"""$(inputs)""")
        removed_bands = inputs['removed_bands']
        groupnames = inputs['groupnames']
        json_file = sys.argv[1]

        with open(json_file, 'r') as f_stream:
            all_groups = json.load(f_stream)

        for removed_band in removed_bands:
            if removed_band == 'None':
                continue
            all_groups.pop(removed_band)
            groupnames.remove(removed_band)

        cwl_output = {"groupnames" : groupnames}

        with open('./out.json', 'w') as fp:
            json.dump(all_groups, fp)

        with open('./groupnames.json', 'w') as fp:
            json.dump(cwl_output, fp)

stdout: check_removed_groups.log
stderr: check_removed_groups_err.log