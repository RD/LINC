class: CommandLineTool
cwlVersion: v1.2
id: make_mask
label: make_mask
baseCommand:
  - make_mask.py
inputs:
  - id: image
    type: File
    inputBinding:
      position: 0
    doc: Input image
  - id: mask_name
    type: string?
    inputBinding:
      position: 1
      prefix: '-m'
    doc: Mask name (default=imagename with mask in place of image)
  - id: threshpix
    type: int?
    default: 5
    doc: Threshold pixel
    inputBinding:
      prefix: '-p'
      position: 1
  - id: adaptive_thresh
    type: int?
    default: 50
    inputBinding:
      position: 1
      prefix: '-a'
    doc: Adaprtive threshold
  - id: atrous_do
    type: boolean?
    default: false
    inputBinding:
      position: 1
      prefix: '-t'
    doc: BDSF extended source detection
  - id: rmsbox
    type: string?
    default: '100,10'
    inputBinding:
      position: 0
      prefix: '-r'
    doc: rms box size
  - id: write_srl
    type: boolean?
    default: false
    inputBinding:
      position: 1
      prefix: '-s'
    doc: Write SRL skymodel
  - id: write_gaul
    type: boolean?
    default: false
    inputBinding:
      position: 1
      prefix: '-g'
    doc: Write BBS gaul skymodel
  - id: write_ds9
    type: boolean?
    default: false
    inputBinding:
      position: 1
      prefix: '-d'
    doc: Write ds9 regions
  - id: combinemask
    type: File?
    inputBinding:
      position: 1
      prefix: '-c'
    doc: Mask to add to the found one
  - id: frequency
    type: float?
    inputBinding:
      position: 1
      prefix: '-f'
outputs:
  - id: mask
    doc: output mask
    type: File
    outputBinding:
      glob: $(inputs.mask_name)
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'make_mask*.log'
hints:
 - class: DockerRequirement
   dockerPull: astronrd/linc
requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.image)
        writable: false
stderr: make_mask.log
stdout: make_mask_err.log
