class: CommandLineTool
cwlVersion: v1.2
id: merge_skymodels
baseCommand:
  - merge_skymodels.py
inputs:
  - id: inmodel1
    type:
      - File?
      - string?
    default: '$LINC_DATA_ROOT/skymodels/A-Team_Midres.skymodel'
    inputBinding:
      prefix: --inmodel1=
      separate: false
      shellQuote: false
  - id: inmodel2
    type:
      - File?
      - string?
    default: '$LINC_DATA_ROOT/skymodels/A-Team_Midres.skymodel'
    inputBinding:
      prefix: --inmodel2=
      separate: false
      shellQuote: false
  - id: outmodel
    type: string?
    default: 'output.skymodel'
    inputBinding:
      prefix: --outmodel=
      separate: false
      shellQuote: false
  - id: radius
    type: float?
    default: 0.00833
    inputBinding:
      prefix: --radius=
      separate: false
      shellQuote: false
    doc: Matching radius in degrees for identifying duplicate components
  - id: keep
    type: string?
    default: 'from2'
    inputBinding:
      prefix: --keep=
      separate: false
      shellQuote: false
    doc: Keep duplicate components from model 1 or model 2

outputs:
  - id: skymodel_out
    type: File
    outputBinding:
      glob: $(inputs.outmodel)
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'merge_skymodels*.log'
label: merge_skymodels
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
stdout: merge_skymodels.log
stderr: merge_skymodels_err.log
