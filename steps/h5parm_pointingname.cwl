class: CommandLineTool
cwlVersion: v1.2
id: h5parm_pointingname
baseCommand:
  - h5parm_pointingname.py
inputs:
  - id: h5parmFile
    type: File
    inputBinding:
      position: 0
    doc: List of h5parm files
  - default: 'target'
    id: solsetName
    type: string?
    inputBinding:
      position: 0
      prefix: '--solsetName'
    doc: Input solset name
  - default: 'POINTING'
    id: pointing
    type: string?
    inputBinding:
      position: 0
      prefix: '--pointing'
    doc: Name of the pointing
outputs:
  - id: outh5parm
    doc: Output h5parm
    type: File
    outputBinding:
      glob: $(inputs.h5parmFile.basename)
  - id: log
    type: File[]
    outputBinding:
      glob: 'h5parm_pointingname*.log'
label: h5parm_pointingname
requirements:
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.h5parmFile)
        writable: true
  - class: InlineJavascriptRequirement
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
stdout: h5parm_pointingname.log
stderr: h5parm_pointingname_err.log
