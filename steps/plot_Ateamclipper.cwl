class: CommandLineTool
cwlVersion: v1.2
id: plot_Ateamclipper
baseCommand:
  - plot_Ateamclipper.py
inputs:
  - id: clipper_output
    type: File?
    inputBinding:
      position: 1
  - id: outputimage
    type: string?
    default: Ateamclipper.png
    inputBinding:
      position: 2
outputs:
  - id: output_imag
    doc: Output image
    type: File
    outputBinding:
      glob: $(inputs.outputimage)
label: plot_Ateamclipper
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
