class: CommandLineTool
cwlVersion: v1.2
id: sort_times_into_freqGroups
baseCommand:
  - python3
  - sort_times.py
inputs:
    - id: msin
      type: Directory[]
      doc: MS to sort
      inputBinding:
        position: 0
    - id: numbands
      type: int?
      default: 10
      doc: Number of how many files should be grouped together in frequency.
    - id: DP3fill
      type: boolean?
      default: True
      doc: Add dummy file-names for missing frequencies, so that DP3 can fill the data with flagged dummy data.
    - id: stepname
      type: string?
      default: '.ms'
      doc: Add this step-name into the file-names of the output files.
    - id: firstSB
      type: int?
      default: null
      doc: If set, then reference the grouping of files to this station-subband. As if a file with this station-subband would be included in the input files.
    - id: mergeLastGroup
      type: boolean?
      default: False
      doc: Add dummy file-names for missing frequencies, so that DP3 can fill the data with flagged dummy data.
    - id: truncateLastSBs
      type: boolean?
      default: True
      doc: Add dummy file-names for missing frequencies, so that DP3 can fill the data with flagged dummy data.
requirements:
  - class: InlineJavascriptRequirement
  - class: InitialWorkDirRequirement
    listing:
      - entryname: sort_times.py
        entry: |
          import sys
          import json
          from sort_times_into_freqGroups import main as sort_times_into_freqGroups

          mss = sys.argv[1:]
          inputs = json.loads(r"""$(inputs)""")

          numbands = inputs['numbands']
          stepname = inputs['stepname']
          DP3fill = inputs['DP3fill']
          mergeLastGroup = inputs['mergeLastGroup']
          truncateLastSBs = inputs['truncateLastSBs']
          firstSB = inputs['firstSB']

          output = sort_times_into_freqGroups(mss, numbands, DP3fill, stepname, mergeLastGroup, truncateLastSBs, firstSB)

          filenames  = output['filenames']
          groupnames = output['groupnames']
          total_bandwidth = output['total_bandwidth']
          midfreq    = (output['maxfreq'] + output['minfreq']) / 2.
          minfreq    = output['minfreq']

          cwl_output = {}
          cwl_output['groupnames'] = groupnames
          cwl_output['total_bandwidth'] = total_bandwidth
          cwl_output['midfreq'] = midfreq
          cwl_output['minfreq'] = minfreq

          with open('./filenames.json', 'w') as fp:
              json.dump(filenames, fp)

          with open('./out.json', 'w') as fp:
              json.dump(cwl_output, fp)

outputs:
  - id: filenames
    type: File
    outputBinding:
        glob: 'filenames.json'
  - id: groupnames
    type: string[]
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).groupnames)
  - id: total_bandwidth
    type: float
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).total_bandwidth)
  - id: midfreq
    type: float
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).midfreq)
  - id: minfreq
    type: float
    outputBinding:
        loadContents: true
        glob: 'out.json'
        outputEval: $(JSON.parse(self[0].contents).minfreq)
  - id: logfile
    type: File
    outputBinding:
      glob: sort_times_into_freqGroups.log
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
stdout: sort_times_into_freqGroups.log
stderr: sort_times_into_freqGroups_err.log
