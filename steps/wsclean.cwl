class: CommandLineTool
cwlVersion: v1.2
id: wsclean
baseCommand:
  - wsclean
inputs:
  - id: msin
    type: Directory[]
    inputBinding:
      position: 2
      shellQuote: false
      itemSeparator: ','
      valueFrom: $(concatenate_path_wsclean(self))
  - id: image_size
    default:
      - 2500
      - 2500
    type: 
      - int[]?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-size'
  - id: image_scale
    default: '36asec'
    type: string?
    inputBinding:
      position: 1
      prefix: '-scale'
      shellQuote: false
  - id: niter
    default: 1000
    type: int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-niter'
  - id: nmiter
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-nmiter'
  - id: auto_threshold
    default: false
    type: 
      - boolean?
      - float?
    inputBinding:
      shellQuote: false
      position: 1
      prefix: '-auto-threshold'
  - id: auto_mask
    default: false
    type: 
      - boolean?
      - float?
    inputBinding:
      shellQuote: false
      position: 1
      prefix: '-auto-mask'
  - id: multiscale
    default: false
    type: 
      - boolean?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-multiscale'
  - id: multiscale_scales
    default: false
    type: 
      - boolean?
      - string?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-multiscale-scales'
  - id: multiscale_scale_bias
    default: false
    type: float?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-multiscale-scale-bias'
  - id: mgain
    default: false
    type: 
      - boolean?
      - float?
    inputBinding:
      shellQuote: false
      position: 1
      prefix: '-mgain'
  - id: ncpu
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-j'
  - id: parallel-gridding
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-parallel-gridding'
  - id: parallel-deconvolution
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-parallel-deconvolution'
  - id: parallel-reordering
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-parallel-reordering'
  - id: channels-out
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-channels-out'
  - id: deconvolution-channels
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-deconvolution-channels'
  - id:  fit-spectral-pol
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-fit-spectral-pol'
  - id: join-channels
    default: false
    type: boolean?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-join-channels'
  - id: use-wgridder
    default: false
    type: boolean?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-use-wgridder'
  - id: taper-gaussian
    default: false
    type: 
      - boolean?
      - string?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-taper-gaussian'
  - id: weighting
    default: false
    type: 
      - boolean?
      - string?
    inputBinding:
      position: 1
      shellQuote: false
      prefix: '-weight'
  - id: maxuvw-m
    default: false
    type: 
      - boolean?
      - int?
    inputBinding:
      position: 1
      prefix: '-maxuvw-m'
      shellQuote: false
  - id: minuv-l
    default: false
    type:
      - boolean?
      - int?
    inputBinding:
      position: 1
      prefix: '-minuv-l'
      shellQuote: false
  - id: maxuv-l
    default: false
    type:
      - boolean?
      - int?
    inputBinding:
      position: 1
      prefix: '-maxuv-l'
      shellQuote: false
  - id: tempdir
    type: 
      - boolean?
      - string?
    default: false
    inputBinding:
      position: 1
      prefix: '-temp-dir'
      shellQuote: false
  - id: no_model_update
    default: false
    type: boolean?
    inputBinding:
      position: 1
      prefix: '-no-update-model-required'
      shellQuote: false
  - id: no_fit_beam
    default: false
    type: boolean?
    inputBinding:
      position: 1
      prefix: '-no-fit-beam'
      shellQuote: false
  - id: circular_beam
    default: false
    type: boolean?
    inputBinding:
      position: 1
      prefix: '-circular-beam'
      shellQuote: false
  - id: beam_size
    default: false
    type:
      - boolean?
      - float?
    inputBinding:
      position: 1
      prefix: '-beam-size'
      shellQuote: false
  - id: local_rms
    default: false
    type: boolean?
    inputBinding:
      position: 1
      prefix: '-local-rms'
      shellQuote: false
  - id: image_name
    default: 'pointing'
    type:
      - string?
      - File?
    inputBinding:
      position: 1
      prefix: '-name'
      shellQuote: false
  - id: save_source_list
    default: false
    type: boolean?
    inputBinding:
      position: 1
      prefix: '-save-source-list'
      shellQuote: false
  - id: do_predict
    default: false
    type: boolean?
    inputBinding:
      position: 1
      prefix: '-predict'
      shellQuote: false
  - id: padding
    type: float?
    inputBinding:
      position: 1
      prefix: -padding
  - id: baseline_averaging
    default: false
    type: float?
    inputBinding:
      position: 1
      prefix: -baseline-averaging
  - id: reuse_dirty
    type: File[]?
    inputBinding:
      position: 1
      prefix: '-reuse-dirty'
      shellQuote: false
      valueFrom: $(self[0].nameroot.replaceAll('-dirty','').replaceAll('-0000',''))
  - id: reuse_psf
    type:
      - File[]?
    inputBinding:
      position: 1
      prefix: '-reuse-psf'
      shellQuote: false
      valueFrom: $(self[0].nameroot.replaceAll('-psf','').replaceAll('-0000',''))
  - id: fits_mask
    type: File?
    inputBinding:
      position: 1
      prefix: '-fits-mask'
      shellQuote: false
  - id: fits_model
    type: File[]?
  - id: fits_image
    type: File?
outputs:
  - id: sourcelist
    type: File?
    outputBinding:
      glob: [$(inputs.image_name)-sources.txt]
  - id: dirty_image
    type: File[]?
    outputBinding:
      glob: [$(inputs.image_name)-*-dirty.fits, $(inputs.image_name)-dirty.fits]
  - id: psf_image
    type:
      - File[]?
    outputBinding:
      glob: [$(inputs.image_name)-*-psf.fits, $(inputs.image_name)-psf.fits]
  - id: image
    type: File
    outputBinding:
      glob: [$(inputs.image_name)-MFS-image.fits, $(inputs.image_name)-image.fits]
  - id: model
    type: File[]?
    outputBinding:
      glob: [$(inputs.image_name)-*-model.fits, $(inputs.image_name)-model.fits]
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'wsclean*.log'
  - id: msout
    type: Directory[]
    outputBinding:
      outputEval: $(inputs.msin)
label: WSClean
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
requirements:
  - class: InplaceUpdateRequirement
    inplaceUpdate: true 
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: true
      - entry: $(inputs.reuse_psf)
        writable: false
      - entry: $(inputs.reuse_dirty)
        writable: false
      - entry: $(inputs.fits_model)
        writable: false
      - entry: $(inputs.fits_image)
        writable: false
  - class: ShellCommandRequirement
  - class: InlineJavascriptRequirement
    expressionLib:
      - { $include: 'utils.js' }
stdout: wsclean.log
stderr: wsclean_err.log