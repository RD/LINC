
class: CommandLineTool
cwlVersion: v1.2
id: losoto_polalign

doc: |
    Estimate polarization misalignment as delay
   
requirements:
  InlineJavascriptRequirement:
    expressionLib:
      - { $include: utils.js}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: $(get_losoto_config('POLALIGN').join('\n'))

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - --verbose
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: astronrd/linc

inputs:
  - id: input_h5parm
    type: File
  - id: soltab
    type: string
    doc: "Solution table"
  - id: soltabout
    default: phasediff
    type: string?
    doc: output table name (same solset)
  - id: maxResidual
    type: float?
    default: 1
    doc: Maximum acceptable rms of the residuals in radians before flagging. Set to zero to avoid the check.
  - id: fitOffset
    type: boolean?
    default: false
    doc: Assume that together with a delay each station has also a differential phase offset (important for old LBA observations).
    
  - id: average
    type: boolean?
    default: false
    doc: Mean-average in time the resulting delays/offset.
  - id: replace
    type: boolean?
    default: false
    doc: replace using smoothed value instead of flag bad data? Smooth must be active.
  - id: refAnt
    type: string?
    doc: Reference antenna, if not set use the first one.
    
outputs:
  - id: output_h5parm
    type: File
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
  - id: log
    type: File[]
    outputBinding:
      glob: '$(inputs.input_h5parm.basename)-losoto*.log'

stdout: $(inputs.input_h5parm.basename)-losoto.log
stderr: $(inputs.input_h5parm.basename)-losoto_err.log
