class: CommandLineTool
cwlVersion: v1.2
id: blank_image_reg
label: blank_image_reg
baseCommand:
  - blank_image_reg.py
inputs:
  - id: input_image
    type: File
    inputBinding:
      position: 0
    doc: Input image
  - id: region
    type: File?
    inputBinding:
      position: 1
    doc: Region file
  - id: outfile
    type: string?
    doc: Output file name
    inputBinding:
      prefix: '--outfile'
      position: 2
  - id: inverse
    type: int?
    inputBinding:
      position: 2
      prefix: '--inverse'
    doc: reverse final combined mask
  - id: blankval
    type: float?
    default: 0.0
    inputBinding:
      position: 2
      prefix: '--blankval'
    doc: pixel value to be set
  - id: operation
    type: string?
    default: 'AND'
    inputBinding:
      position: 2
      prefix: '--operation'
    doc: how to combine multiple regions with AND or OR
outputs:
  - id: image
    doc: input image as output
    type: File
    outputBinding:
      glob: $(inputs.input_image.basename)
  - id: output_image
    doc: output image as given as output file name
    type: File
    outputBinding:
      glob: $(inputs.outfile)
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'blank_image_reg*.log'
hints:
 - class: DockerRequirement
   dockerPull: astronrd/linc
requirements:
 - class: InplaceUpdateRequirement
   inplaceUpdate: true 
 - class: InitialWorkDirRequirement
   listing:
     - entry: $(inputs.input_image)
       writable: true
stderr: blank_image_reg.log
stdout: blank_image_reg_err.log
