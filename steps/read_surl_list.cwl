class: ExpressionTool
cwlVersion: v1.2
id: read_surl_file
inputs:
  - id: surl_list
    type: File
    doc: input surl file
    inputBinding:
      loadContents: True
outputs:
  - id: surls
    type: string[]

expression: |
  ${
    var surl_list = inputs.surl_list.contents.split("\n");
    surl_list = surl_list.filter(function (el) { return el != ''; } );
    return {'surls': surl_list}
  }
label: ReadSurlList

requirements:
  - class: InlineJavascriptRequirement
