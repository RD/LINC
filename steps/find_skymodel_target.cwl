class: CommandLineTool
cwlVersion: v1.2
id: find_skymodel_target_py
baseCommand:
  - python3
  - find_sky.py
inputs:
    - id: msin
      type: Directory[]
      doc: MS containing the target
      inputBinding:
        position: 0
    - id: SkymodelPath
      type: File?
      doc: File containing or putting the skymodel
    - id: Radius
      type: float?
      doc: Radius of the skymodel
      default: 5.0
    - id: Source
      type: string?
      doc: Source of the skymodel
      default: 'TGSS'
    - id: DoDownload
      type: boolean?
      doc: Download a new skymodel if given path is empty
      default: true
    - id: targetname
      type: string?
      default: 'pointing'
    - id: fluxlimit
      type: float?
requirements:
  - class: InlineJavascriptRequirement
  - class: NetworkAccess
    networkAccess: true
  - class: InitialWorkDirRequirement
    listing:
      - entryname: find_sky.py
        entry: |
          import sys
          import shutil
          import os
          import json
          
          null = None

          from download_skymodel_target import main as download_skymodel_target

          mss = sys.argv[1:]
          inputs = json.loads(r"""$(inputs)""")
          targetname = inputs['targetname']
          fluxlimit = inputs['fluxlimit']

          SkymodelPath = inputs['SkymodelPath']
          if SkymodelPath is None:
              pass
          else:
              SkymodelPath = SkymodelPath["path"]
              shutil.copyfile(SkymodelPath, targetname + ".skymodel")

          SkymodelPath = os.getcwd() + "/" + targetname + ".skymodel"
          Radius = inputs['Radius']
          Source = inputs['Source']
          DoDownload = str(inputs['DoDownload'])

          output = download_skymodel_target(mss, SkymodelPath, Radius, DoDownload, Source, targetname, fluxlimit)

outputs:
  - id: skymodel
    type: File
    outputBinding:
      glob: $(inputs.targetname).skymodel
  - id: logfile
    type: File
    outputBinding:
      glob: find_skymodel_target.log    
        
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc

stdout: find_skymodel_target.log