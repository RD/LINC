class: CommandLineTool
cwlVersion: v1.2
id: dp3concat
baseCommand:
  - DP3
inputs:
  - id: msin
    type: Directory[]
    doc: Input Measurement Set
  - id: msin_fname
    doc: Input Measurement Set string (including dummy.ms)
    type: string[]?
    inputBinding:
      position: 1
      prefix: msin=
      separate: false
      itemSeparator: ','
      valueFrom: "[$(self.join(','))]"
  - id: msout_name
    type: string
    inputBinding:
      prefix: msout=
      separate: false
      shellQuote: false
      position: 0
  - id: msin_datacolumn
    type: string?
    inputBinding:
      prefix: msin.datacolumn=
      separate: false
      shellQuote: false
      position: 0
    doc: Input data Column
    default: DATA
  - id: msout_datacolumn
    type: string?
    inputBinding:
      prefix: msout.datacolumn=
      separate: false
      shellQuote: false
      position: 0
    default: DATA
  - id: baseline
    default: null
    type: string?
    inputBinding:
      position: 0
      prefix: msin.baseline=
      shellQuote: true
      separate: false
      valueFrom: $(self)
  - id: chunkduration
    type: float?
    inputBinding:
      prefix: msout.chunkduration=
      separate: false
      shellQuote: false
      position: 0    
  - id: filter_baselines
    type: string?
    inputBinding:
      prefix: filter.baseline=
      separate: false
      shellQuote: true
      position: 0
      valueFrom: $(self)
    default: null
  - id: filter_remove
    type: boolean?
    inputBinding:
      prefix: filter.remove=True
      shellQuote: false
      position: 0
    default: false
  - id: writefullresflag
    type: boolean?
    inputBinding:
      prefix: msout.writefullresflag=True
      shellQuote: false
      position: 0
    default: false
  - id: overwrite
    type: boolean?
    inputBinding:
      prefix: msout.overwrite=True
      shellQuote: false
      position: 0
    default: false
  - id: storagemanager
    type: string?
    inputBinding:
      prefix: msout.storagemanager=
      separate: false
      shellQuote: false
      position: 0
    default: ''
  - id: databitrate
    type: int?
    inputBinding:
      prefix: msout.storagemanager.databitrate=
      separate: false
      shellQuote: false
      position: 0
  - id: missingdata
    type: boolean?
    inputBinding:
      prefix: msin.missingdata=True
      separate: false
      shellQuote: false
      position: 0
  - id: avg_timeresolution
    type: int?
    inputBinding:
      prefix: average.timeresolution=
      separate: false
      shellQuote: false
      position: 0
    default: 1
  - id: avg_freqresolution
    type: string?
    inputBinding:
      prefix: average.freqresolution=
      separate: false
      shellQuote: false
      position: 0
    default: 12.205kHz
  - id: save2json
    default: true
    type: boolean?
    inputBinding:
      position: 0
      prefix: count.savetojson=True
  - id: jsonfilename
    type: string?
    default: 'out.json'
    inputBinding:
      prefix: count.jsonfilename=
      separate: false
  - id: steps
    type: string?
    default: ''
  - id: parmdb
    type: File?
    inputBinding:
      position: 0
      prefix: applycal.parmdb=
      separate: false
  - id: correction
    type: string?
    default: phase000
    inputBinding:
      position: 0
      prefix: applycal.correction=
      separate: false
  - id: solset
    default: 'sol000'
    type: string?
    inputBinding:
      position: 0
      prefix: applycal.solset=
      separate: false
  - id: flag_transfer_source_ms
    type: Directory?
    inputBinding:
      position: 0
      prefix: flagtransfer.source_ms=
      separate: false
  - id: flag_transfer_baselines
    type: string?
    inputBinding:
      prefix: flagtransfer.baseline=
      separate: false
      shellQuote: true
      position: 0
      valueFrom: $(self)
    default: null
outputs:
  - id: msout
    doc: Output Measurement Set
    type:
      - Directory[]
    outputBinding:
      glob: "$(inputs.msout_name.split('.')[0])*"
  - id: flagged_fraction_dict
    type: string
    outputBinding:
        loadContents: true
        glob: $(inputs.jsonfilename)
        outputEval: $(JSON.parse(self[0].contents).flagged_fraction_dict)
  - id: logfile
    type: File[]
    outputBinding:
      glob: concat*.log
arguments:
  - steps=[$(inputs.steps)]
  - msin.orderms=False
requirements:
  - class: ShellCommandRequirement
  - class: InlineJavascriptRequirement
  - class: ResourceRequirement
    coresMin: 8
hints:
  - class: DockerRequirement
    dockerPull: astronrd/linc
  - class: InitialWorkDirRequirement
    listing:
      - entry: $(inputs.msin)
        writable: false
stdout: concat.log
stderr: concat_err.log
