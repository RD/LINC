#!/usr/bin/env cwl-runner

class: CommandLineTool
cwlVersion: v1.2
id: losoto_interpolate

doc: |
  This operation for LoSoTo implements regridding and linear interpolation of data for an axis.
  WEIGHT: compliant




requirements:
  InlineJavascriptRequirement:
    expressionLib:
      - { $include: utils.js}
  InitialWorkDirRequirement:
    listing:
      - entryname: 'parset.config'
        entry: $(get_losoto_config('INTERPOLATE').join('\n'))

      - entryname: $(inputs.input_h5parm.basename)
        entry: $(inputs.input_h5parm)
        writable: true

baseCommand: "losoto"

arguments:
  - --verbose
  - $(inputs.input_h5parm.basename)
  - parset.config

hints:
  DockerRequirement:
    dockerPull: astronrd/linc

inputs:
  - id: input_h5parm
    type: File
  - id: soltab
    type: string
    doc: "Solution table"

  - id: axisToRegrid
    type: string
    doc: Name of the axis for which regridding/interpolation will be done
  - id: outSoltab
    type: string
    doc: Output sol tab name
  - id: newdelta
    type: string?
    doc: Fundamental width between samples after regridding. E.g., "100kHz" or "10s"
    default: "100kHz"

  - id: delta
    type: string?
    doc: |
      Fundamental width between samples in axisToRegrid. E.g., "100kHz" or "10s". If "",
      it is calculated from the axisToRegrid values

  - id: maxFlaggedWidth
    type: int?
    doc: |
      Maximum allowable width in number of samples (after regridding) above which
      interpolated values are flagged (e.g., maxFlaggedWidth = 5 would allow gaps of
      5 samples or less to be interpolated across but gaps of 6 or more would be
      flagged)
  - id: log
    type: boolean?
    doc: Interpolation is done in log10 space, by default False


outputs:
  - id: output_h5parm
    type: File
    outputBinding:
      glob: $(inputs.input_h5parm.basename)
  - id: logfile
    type: File[]
    outputBinding:
      glob: '$(inputs.input_h5parm.basename)-losoto*.log'

stdout: $(inputs.input_h5parm.basename)-losoto.log
stderr: $(inputs.input_h5parm.basename)-losoto_err.log
