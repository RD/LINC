class: CommandLineTool
cwlVersion: v1.2
id: mslin2circ
label: mslin2circ
baseCommand:
  - mslin2circ.py
inputs:
  - id: msin
    type: Directory
    doc: Input measurement set
  - id: inms
    type: string
    inputBinding:
      position: 1
      prefix: --inms
    doc: Input measurement set and column name
  - id: outms
    default: ''
    type: string?
    inputBinding:
      position: 1
      prefix: --outms
    doc: Output MS
  - id: reverse
    type: boolean?
    default: false
    doc: Convert from circular to linear
    inputBinding:
      position: 1
      prefix: --reverse
  - id: skipmetadata
    default: false
    type: boolean?
    inputBinding:
      position: 1
      prefix: --skipmetadata
    doc: Skip setting the metadata correctly
  - id: weights
    default: false
    type: boolean?
    inputBinding:
      position: 1
      prefix: --weights
    doc: Weights are updated to reflect the combined polarization (cannot be undone with -r)
outputs:
  - id: msout
    doc: MS set output
    type: Directory
    outputBinding:
      glob: $(inputs.msin.basename)
  - id: logfile
    type: File[]
    outputBinding:
      glob: 'mslin2circ*.log'
hints:
 - class: DockerRequirement
   dockerPull: astronrd/linc
requirements:
 - class: InitialWorkDirRequirement
   listing:
    - entry: $(inputs.msin)
      writable: true
 - class: InplaceUpdateRequirement
   inplaceUpdate: true 
 - class: InlineJavascriptRequirement
stdout: mslin2circ.log
stderr: mslin2circ_err.log
