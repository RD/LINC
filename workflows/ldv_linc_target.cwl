#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

requirements:
  - class: ScatterFeatureRequirement
  - class: SubworkflowFeatureRequirement

inputs:
  - id: surls
    type: string[]
  - id: solutions
    type: string
  - id: avg_timeresolution_concat
    type: int?
    default: 4
  - id: avg_freqresolution_concat
    type: string?
    default: 48.82kHz

outputs:
  - id: cal_solutions
    type: File
    outputSource:
    - linc_target/solutions
  - id: summary
    type: File
    outputSource:
    - linc_target/summary
  - id: calibrated_data
    type: File[]
    outputSource:
    - compress_calibrated_data/compressed
  - id: inspection_plots
    type: File
    outputSource:
    - compress_inspection_plots/compressed
  - id: log_files
    type: File
    outputSource:
    - compress_logs/compressed
  - id: quality
    type: Any
    outputSource:
    - format_quality/quality
steps:
  - id: format_quality
    in: 
    - id: plots
      source: list_inspection_plots/output
    out:
    - id: quality
    run: ../steps/format_quality.cwl
  - id: fetch_data
    in:
    - id: surl_link
      source: surls
    scatter: surl_link
    run: ../steps/fetch_data.cwl
    out:
    - id: uncompressed
  - id: fetch_solutions
    in: 
    - id: surl_link
      source: solutions
    run: ../steps/fetch_file.cwl
    out:
    - id: downloaded
  - id: linc_target
    in:
    - id: msin
      source: fetch_data/uncompressed
    - id: cal_solutions
      source: fetch_solutions/downloaded
    - id: avg_timeresolution_concat
      source: avg_timeresolution_concat
    - id: avg_freqresolution_concat
      source: avg_freqresolution_concat
    run: HBA_target.cwl
    out:
    - id: solutions
    - id: calibrated_data
    - id: summary
    - id: inspection_plots
    - id: log_files
  - id: compress_inspection_plots
    in: 
    - id: directory
      source: linc_target/inspection_plots
    out:
    - id: compressed
    run: ../steps/compress.cwl
  - id: list_inspection_plots
    in:
    - id: input
      source: linc_target/inspection_plots
    out:
    - id: output
    run: ../steps/list_directory_files.cwl
  - id: split_calibrated_data
    in: 
    - id: input
      source: linc_target/calibrated_data
    out:
    - id: output
    run: ../steps/list_directory.cwl
  - id: compress_calibrated_data
    in:
    - id: directory
      source: split_calibrated_data/output
    out:
    - id: compressed
    run: ../steps/compress.cwl
    scatter: directory
  - id: compress_logs
    in:
    - id: directory
      source: linc_target/log_files
    out:
    - id: compressed
    run: ../steps/compress.cwl