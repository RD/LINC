class: Workflow
cwlVersion: v1.2
id: HBA_target_VLBI
label: HBA_target_VLBI
inputs:
  - id: msin
    type: Directory[]
  - id: cal_solutions
    type: File
  - id: refant
    type: string?
    default: 'CS00.*'
  - id: flag_baselines
    type: string[]?
    default: []
  - id: process_baselines_target
    type: string?
    default: '[CR]S*&'
  - id: filter_baselines
    type: string?
    default: '*&'
  - id: do_smooth
    type: boolean?
    default: false
  - id: rfistrategy
    type:
      - File?
      - string?
    default: '$LINC_DATA_ROOT/rfistrategies/lofar-hba-wideband.lua'
  - id: min_unflagged_fraction
    type: float?
    default: 0.5
  - id: compression_bitrate
    type: int?
    default: 16
  - id: raw_data
    type: boolean?
    default: false
  - id: propagatesolutions
    type: boolean?
    default: true
  - id: demix_sources
    type: string[]?
    default:
      - VirA_4_patch
      - CygAGG
      - CasA_4_patch
      - TauAGG
  - id: demix_timeres
    type: float?
    default: 10
  - id: demix_freqres
    type: string?
  - id: demix
    type: boolean?
  - id: apply_tec
    type: boolean?
    default: false
  - id: apply_clock
    type: boolean?
    default: true
  - id: apply_phase
    type: boolean?
    default: false
  - id: apply_RM
    type: boolean?
    default: true
  - id: apply_beam
    type: boolean?
    default: true
  - id: gsmcal_step
    type: string?
    default: 'phase'
  - id: updateweights
    type: boolean?
    default: true
  - id: max_dp3_threads
    type: int?
    default: 10
  - id: memoryperc
    type: int?
    default: 20
  - id: min_separation
    type: int?
    default: 30
  - id: A-Team_skymodel
    type: File?
  - id: target_skymodel
    type: File?
  - id: use_target
    type: boolean?
    default: true
  - id: skymodel_source
    type: string?
    default: 'TGSS'
  - id: avg_timeresolution
    type: int?
    default: 1
  - id: avg_freqresolution
    type: string?
    default: 12.21kHz
  - id: avg_timeresolution_concat
    type: int?
    default: 8
  - id: avg_freqresolution_concat
    type: string?
    default: 97.64kHz
  - id: num_SBs_per_group
    type: int?
  - id: calib_nchan
    type: int?
  - id: reference_stationSB
    type: int?
    default: null
  - id: ionex_server
    type: string?
    default: 'http://chapman.upc.es/'
  - id: ionex_prefix
    type: string?
    default: 'UQRG'
  - id: proxy_server
    type: string?
    default: null
  - id: proxy_port
    type: int?
    default: null
  - id: proxy_type
    type: string?
    default: null
  - id: proxy_user
    type: string?
    default: null
  - id: proxy_pass
    type: string?
    default: null
  - id: clip_sources
    type: string[]?
    default:
      - VirA_4_patch
      - CygAGG
      - CasA_4_patch
      - TauAGG
  - id: clipAteam
    type: boolean?
    default: true
  - id: lbfgs_historysize
    type: int?
  - id: lbfgs_robustdof
    type: float?
  - id: aoflag_reorder
    type: boolean?
    default: false
  - id: aoflag_chunksize
    type: int?
    default: 2000
  - id: aoflag_freqconcat
    type: boolean?
    default: true
  - id: selfcal
    type: boolean?
    default: false
  - id: selfcal_strategy
    type: string?
    default: 'HBA'
  - id: selfcal_hba_imsize
    type: int[]?
    default: [20000,20000]
  - id: selfcal_hba_uvlambdamin
    type: float?
    default: 200.
  - id: selfcal_region
    type: File?
  - id: chunkduration
    type: float?
    default: 0.0
  - id: wsclean_tmpdir
    type: string?
  - id: make_structure_plot
    type: boolean?
    default: true
  - id: skymodel_fluxlimit
    type: float?
  - id: output_fullres_data
    type: boolean?
    default: true
outputs:
  - id: calibrated_data
    outputSource:
      - save_results/dir
    type: Directory
  - id: log_files
    outputSource:
      - save_logfiles/dir
    type: Directory
  - id: inspection_plots
    outputSource:
      - save_inspection/dir
    type: Directory
  - id: summary
    outputSource:
      - linc/summary_file
    type: File
  - id: solutions
    outputSource:
      - linc/solutions
    type: File
steps:
  - id: linc
    in:
      - id: msin
        source:
          - msin
      - id: cal_solutions
        source: cal_solutions
      - id: refant
        source: refant
      - id: flag_baselines
        source:
          - flag_baselines
      - id: process_baselines_target
        source: process_baselines_target
      - id: filter_baselines
        source: filter_baselines
      - id: do_smooth
        source: do_smooth
      - id: rfistrategy
        source: rfistrategy
      - id: min_unflagged_fraction
        source: min_unflagged_fraction
      - id: compression_bitrate
        source: compression_bitrate
      - id: raw_data
        source: raw_data
      - id: propagatesolutions
        source: propagatesolutions
      - id: apply_tec
        source: apply_tec
      - id: apply_clock
        source: apply_clock
      - id: apply_phase
        source: apply_phase
      - id: apply_RM
        source: apply_RM
      - id: apply_beam
        source: apply_beam
      - id: demix_sources
        source: demix_sources
      - id: demix_freqres
        source: demix_freqres
      - id: demix_timeres
        source: demix_timeres
      - id: demix
        source: demix
      - id: updateweights
        source: updateweights
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: memoryperc
        source: memoryperc
      - id: min_separation
        source: min_separation
      - id: A-Team_skymodel
        source: A-Team_skymodel
      - id: target_skymodel
        source: target_skymodel
      - id: use_target
        source: use_target
      - id: skymodel_source
        source: skymodel_source
      - id: avg_timeresolution
        source: avg_timeresolution
      - id: avg_freqresolution
        source: avg_freqresolution
      - id: avg_timeresolution_concat
        source: avg_timeresolution_concat
      - id: avg_freqresolution_concat
        source: avg_freqresolution_concat
      - id: num_SBs_per_group
        source: num_SBs_per_group
      - id: calib_nchan
        source: calib_nchan
      - id: reference_stationSB
        source: reference_stationSB
      - id: ionex_server
        source: ionex_server
      - id: ionex_prefix
        source: ionex_prefix
      - id: proxy_server
        source: proxy_server
      - id: proxy_port
        source: proxy_port
      - id: proxy_type
        source: proxy_type
      - id: proxy_user
        source: proxy_user
      - id: proxy_pass
        source: proxy_pass
      - id: clip_sources
        source: clip_sources
      - id: clipAteam
        source: clipAteam
      - id: gsmcal_step
        source: gsmcal_step
      - id: lbfgs_historysize
        source: lbfgs_historysize
      - id: lbfgs_robustdof
        source: lbfgs_robustdof
      - id: aoflag_reorder
        source: aoflag_reorder
      - id: aoflag_chunksize
        source: aoflag_chunksize
      - id: aoflag_freqconcat
        source: aoflag_freqconcat
      - id: selfcal
        source: selfcal
      - id: selfcal_strategy
        source: selfcal_strategy
      - id: selfcal_hba_imsize
        source: selfcal_hba_imsize
      - id: selfcal_region
        source: selfcal_region
      - id: chunkduration
        source: chunkduration
      - id: wsclean_tmpdir
        source: wsclean_tmpdir
      - id: make_structure_plot
        source: make_structure_plot
      - id: skymodel_fluxlimit
        source: skymodel_fluxlimit
      - id: selfcal_hba_uvlambdamin
        source: selfcal_hba_uvlambdamin
      - id: output_fullres_data
        source: output_fullres_data
    out:
      - id: logfiles
      - id: msout
      - id: solutions
      - id: inspection
      - id: summary_file
    run: ./linc_target.cwl
    label: linc_target
  - id: save_logfiles
    in:
      - id: files
        linkMerge: merge_flattened
        source:
          - linc/logfiles
      - id: sub_directory_name
        default: logs
    out:
      - id: dir
    run: ./../steps/collectfiles.cwl
    label: save_logfiles
  - id: save_inspection
    in:
      - id: files
        linkMerge: merge_flattened
        source:
          - linc/inspection
      - id: sub_directory_name
        default: inspection
    out:
      - id: dir
    run: ./../steps/collectfiles.cwl
    label: save_inspection
  - id: save_results
    in:
      - id: files
        linkMerge: merge_flattened
        source:
          - linc/msout
      - id: sub_directory_name
        default: results
    out:
      - id: dir
    run: ./../steps/collectfiles.cwl
    label: save_results
requirements:
  - class: SubworkflowFeatureRequirement
  - class: MultipleInputFeatureRequirement
