class: Workflow
cwlVersion: v1.2
id: dp3_prep_cal
label: dp3_prep_cal
inputs:
  - id: max_dp3_threads
    type: int?
    default: 10
  - id: baselines_to_flag
    type: string[]?
    default: []
  - id: elevation_to_flag
    type: string?
    default: '0deg..15deg'
  - id: min_amplitude_to_flag
    type: float?
    default: 1e-30
  - id: memoryperc
    type: int?
    default: 20
  - id: raw_data
    type: boolean?
    default: false
  - id: demix
    type: boolean?
    default: false
  - id: msin
    type: Directory
  - id: msin_baseline
    type: string?
    default: '*&'
  - id: skymodel
    type:
      - File
      - Directory
  - id: timeresolution
    type: int?
    default: 4
  - id: freqresolution
    type: string?
    default: '48.82kHz'
  - id: demix_timeres
    type: float?
    default: 10
  - id: demix_freqres
    type: string?
    default: '48.82kHz'
  - id: demix_baseline
    type: string?
    default: '*&'
  - id: target_source
    type: string?
    default: ''
  - id: ntimechunk
    type: int?
    default: 16
  - id: subtract_sources
    type: string[]?
    default: []
  - id: lbfgs_historysize
    type: int?
    default: 10
  - id: lbfgs_robustdof
    type: float?
    default: 200
outputs:
  - id: msout
    outputSource:
      - dp3_execute/msout
    type: Directory
  - id: logfile
    outputSource:
      - concat_logfiles_dp3/output
    type: File
  - id: flagged_fraction_dict
    outputSource:
      - dp3_execute/flagged_fraction_dict
    type: string
  - id: instrument_tables
    outputSource:
      - dp3_execute/instrument_table
    type: Directory?
steps:
  - id: define_parset
    in:
      - id: raw_data
        source: raw_data
      - id: demix
        source: demix
      - id: filter_baselines
        source: msin_baseline
      - id: memoryperc
        source: memoryperc
      - id: baselines_to_flag
        source:
          - baselines_to_flag
      - id: elevation_to_flag
        source: elevation_to_flag
      - id: min_amplitude_to_flag
        source: min_amplitude_to_flag
      - id: timeresolution
        source: timeresolution
      - id: freqresolution
        source: freqresolution
      - id: process_baselines_cal
        source: demix_baseline
      - id: demix_timeres
        source: demix_timeres
      - id: demix_freqres
        source: demix_freqres
      - id: target_source
        source: target_source
      - id: subtract_sources
        source:
          - subtract_sources
      - id: ntimechunk
        source: ntimechunk
      - id: lbfgs_historysize
        source: lbfgs_historysize
      - id: lbfgs_robustdof
        source: lbfgs_robustdof
    out:
      - id: output
    run: ../../steps/dp3_make_parset_cal.cwl
    label: make_parset
  - id: dp3_execute
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: parset
        source: define_parset/output
      - id: msin
        source: msin
      - id: msout_name
        source: msin
        valueFrom: $("out_"+self.basename)
      - id: autoweight
        source: raw_data
      - id: baseline
        source: msin_baseline
      - id: output_column
        default: DATA
      - id: input_column
        default: DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: skymodel
        source: skymodel
    out:
      - id: msout
      - id: instrument_table
      - id: flagged_fraction_dict
      - id: logfile
    run: ../../steps/dp3_prep_cal.cwl
    label: DP3.Execute
  - id: concat_logfiles_dp3
    in:
      - id: file_list
        source:
          - dp3_execute/logfile
      - id: file_prefix
        default: dp3
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_dp3
requirements:
  - class: SubworkflowFeatureRequirement
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
