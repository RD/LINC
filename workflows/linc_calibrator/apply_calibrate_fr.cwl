class: Workflow
cwlVersion: v1.2
id: apply_calibrate
label: apply_calibrate
inputs:
  - id: max_dp3_threads
    type: int?
  - id: msin
    type: Directory
  - id: do_smooth
    type: boolean?
    default: false
  - id: flagunconverged
    type: boolean?
    default: false
  - id: propagatesolutions
    type: boolean?
    default: true
  - id: input_h5parm
    type: File
outputs:
  - id: msout
    outputSource:
      - calib_cal/msout ## needed due to cwltool issue 1785
    type: Directory
  - id: BLsmooth.log
    outputSource:
      - BLsmooth/logfile
    type: File
  - id: apply_cal.log
    outputSource:
      - concat_logfiles_applycal/output
    type: File
  - id: calib_cal.log
    outputSource:
      - concat_logfiles_calib_cal/output
    type: File
  - id: outh5parm
    outputSource:
      - calib_cal/h5parm
    type: File
steps:
  - id: remove_col
    in:
      - id: msin
        source: msin
      - id: task
        default: "ALTER TABLE"
      - id: command
        default: "DELETE COLUMN CIRC_PHASEDIFF_DATA, FR_MODEL_DATA"
    out:
      - id: msout
      - id: logfile
    run: ../../steps/taql.cwl
    label: remove_col
  - id: applyFR
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: remove_col/msout
      - id: msin_datacolumn
        default: CORRECTED_DATA
      - id: parmdb
        source: input_h5parm
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: solset
        default: calibrator
      - id: correction
        default: faraday
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    label: applyFR
  - id: BLsmooth
    in:
      - id: msin
        source: applyFR/msout
      - id: do_smooth
        source: do_smooth
      - id: in_column_name
        default: CORRECTED_DATA
    out:
      - id: msout
      - id: logfile
    run: ../../steps/blsmooth.cwl
    label: BLsmooth
  - id: calib_cal
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: BLsmooth/msout
        valueFrom: $([self])
      - id: msin_datacolumn
        default: SMOOTHED_DATA
      - id: modeldatacolumns
        default: 
          - 'MODEL_DATA'
      - id: flagunconverged
        source: flagunconverged
      - id: propagate_solutions
        source: propagatesolutions
      - id: mode
        default: diagonal
    out:
      - id: msout ## needed due to cwltool issue 1785
      - id: h5parm
      - id: logfile
    run: ../../steps/ddecal.cwl
  - id: concat_logfiles_calib_cal
    in:
      - id: file_list
        source:
          - calib_cal/logfile
      - id: file_prefix
        default: calib_cal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib_cal
  - id: concat_logfiles_applycal
    in:
      - id: file_list
        source:
          - applyFR/logfile
      - id: file_prefix
        default: applycal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_applycal
requirements: []
