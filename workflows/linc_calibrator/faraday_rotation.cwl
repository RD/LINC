class: Workflow
cwlVersion: v1.2
id: faraday_rotation
label: faraday_rotation
inputs:
  - id: refAnt
    type: string?
    default: 'CS001HBA0'
  - id: input_h5parm
    type: File
  - id: soltabname
    type: string?
    default: 'faraday'
outputs:
  - id: output_h5parm
    outputSource:
      - losoto_residual/output_h5parm
    type: File
  - id: logfiles
    outputSource:
      - losoto_duplicate/log
      - losoto_reset/log
      - losoto_faraday/log
      - losoto_residual/log
    type: File[]
    linkMerge: merge_flattened
steps:
  - id: losoto_duplicate
    in:
      - id: input_h5parm
        source: input_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabOut
        default: phaseOrig
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Duplicate.cwl
  - id: losoto_reset
    in:
      - id: input_h5parm
        source: losoto_duplicate/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: pol
        default: YY
      - id: dataVal
        default: 0.0
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Reset.cwl
  - id: losoto_faraday
    in:
      - id: input_h5parm
        source: losoto_reset/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabOut
        source: soltabname
      - id: refAnt
        source: refAnt
      - id: maxResidual
        default: 1
      - id: freq.minmaxstep
        default:
          - 20e6
          - 1e9
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Faraday.cwl
  - id: losoto_residual
    in:
      - id: input_h5parm
        source: losoto_faraday/output_h5parm
      - id: soltab
        default: sol000/phase000
      - id: soltabsToSub
        source: soltabname
        valueFrom: $([self])
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Residual.cwl

requirements: []
