class: Workflow
cwlVersion: v1.2
id: apply_pa
label: apply_pa
inputs:
  - id: max_dp3_threads
    type: int?
  - id: msin
    type: Directory
  - id: input_h5parm
    type: File
  - id: do_smooth
    type: boolean?
    default: false
  - id: instrument
    type: string?
    default: 'HBA'
outputs:
  - id: msout
    outputSource:
      - prepare_fr/msout
    type: Directory
  - id: apply_cal.log
    outputSource:
      - concat_logfiles_applycal/output
    type: File
  - id: applybeam.log
    outputSource:
      - concat_logfiles_applybeam/output
    type: File
  - id: BLsmooth.log
    outputSource:
      - prepare_fr/BLsmooth.log
    type: File
  - id: model_fr.log
    outputSource:
      - prepare_fr/model_fr.log
    type: File

steps:
  - id: applyPA
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: msin
      - id: msin_datacolumn
        default: DATA
      - id: parmdb
        source: input_h5parm
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: solset
        default: calibrator
      - id: correction
        default: polalign
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    label: applyPA
  - id: applybeam
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin_datacolumn
        default: CORRECTED_DATA
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: updateweights
        default: true
      - id: invert
        default: true
      - id: beammode
        default: element
      - id: usechannelfreq
        default: false
      - id: msin
        source: applyPA/msout
      - id: type
        default: applybeam
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applybeam.cwl
    label: applybeam
  - id: prepare_fr
    in:
      - id: msin
        source: applybeam/msout
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: do_smooth
        source: do_smooth
      - id: instrument
        source: instrument
      - id: in_column_name
        default: DATA
    out:
      - id: msout
      - id: model_fr.log
      - id: BLsmooth.log
    run: ./prep_fr.cwl
    label: prepare_FR
  - id: concat_logfiles_applybeam
    in:
      - id: file_list
        source:
          - applybeam/logfile
      - id: file_prefix
        default: applybeam
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_applybeam
  - id: concat_logfiles_applycal
    in:
      - id: file_list
        source:
          - applyPA/logfile
      - id: file_prefix
        default: applycal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_applycal
requirements: []
