class: Workflow
cwlVersion: v1.2
id: prep_fr
label: prepare_fr
inputs:
  - id: max_dp3_threads
    type: int?
  - id: msin
    type: Directory
  - id: instrument
    type: string? 
  - id: do_smooth
    type: boolean?
  - id: in_column_name
    type: string?
    default: CORRECTED_DATA
outputs:
  - id: msout
    outputSource:
      - get_fr_model/msout
    type: Directory
  - id: model_fr.log
    outputSource:
      - concat_logfiles_model/output
    type: File
  - id: BLsmooth.log
    outputSource:
      - BLsmooth/logfile
    type: File
steps:
  - id: add_circ_phasedif
    in:
      - id: msin
        source: msin
      - id: msin_datacolumn
        source: in_column_name
      - id: msout_datacolumn
        default: CIRC_PHASEDIFF_DATA
      - id: max_dp3_threads
        source: max_dp3_threads
    out:
      - id: msout
      - id: logfile
    run: ../../steps/dp3_addcol.cwl
    label: add_circ_phasedif
  - id: BLsmooth
    in:
      - id: msin
        source: add_circ_phasedif/msout
      - id: do_smooth
        source: do_smooth
      - id: in_column_name
        default: CIRC_PHASEDIFF_DATA
      - id: out_column
        default: CIRC_PHASEDIFF_DATA
      - id: restore
        default: true
      - id: nofreq
        default: true
      - id: chunks
        default: 1
      - id: ncpu
        default: 8
      - id: ionfactor
        source: instrument
        valueFrom: '$(self == "LBA" ? 1e-2 : 0.2e-3)'
    out:
      - id: msout
      - id: logfile
    run: ../../steps/blsmooth.cwl
    label: BLsmooth
  - id: mslin2circ
    in:
      - id: msin
        source: BLsmooth/msout
      - id: inms
        source: msin
        valueFrom: $(self.basename+":CIRC_PHASEDIFF_DATA")
      - id: outms
        source: msin
        valueFrom: $(self.basename+":CIRC_PHASEDIFF_DATA")
      - id: skipmetadata
        default: true
    out:
      - id: msout
      - id: logfile
    run: ../../steps/mslin2circ.cwl
    label: mslin2circ
  - id: get_circ_phasedif
    in:
      - id: msin
        source: mslin2circ/msout
      - id: command
        default: "SET CIRC_PHASEDIFF_DATA[,0]=0.5*EXP(1.0i*(PHASE(CIRC_PHASEDIFF_DATA[,0])-PHASE(CIRC_PHASEDIFF_DATA[,3]))), CIRC_PHASEDIFF_DATA[,3]=CIRC_PHASEDIFF_DATA[,0], CIRC_PHASEDIFF_DATA[,1]=0+0i, CIRC_PHASEDIFF_DATA[,2]=0+0i"
    out:
      - id: msout
      - id: logfile
    run: ../../steps/taql.cwl
    label: get_circ_phasedif
  - id: add_fr_model
    in:
      - id: msin
        source: get_circ_phasedif/msout
      - id: msin_datacolumn
        default: DATA
      - id: msout_datacolumn
        default: FR_MODEL_DATA
      - id: max_dp3_threads
        source: max_dp3_threads
    out:
      - id: msout
      - id: logfile
    run: ../../steps/dp3_addcol.cwl
    label: add_fr_model
  - id: get_fr_model
    in:
      - id: msin
        source: add_fr_model/msout
      - id: command
        default: "SET FR_MODEL_DATA[,0]=0.5+0i, FR_MODEL_DATA[,1]=0.0+0i, FR_MODEL_DATA[,2]=0.0+0i, FR_MODEL_DATA[,3]=0.5+0i"
    out:
      - id: msout
      - id: logfile
    run: ../../steps/taql.cwl
    label: get_fr_model
  - id: concat_logfiles_model
    in:
      - id: file_list
        source:
          - add_circ_phasedif/logfile
          - mslin2circ/logfile
          - get_circ_phasedif/logfile
          - add_fr_model/logfile
          - get_fr_model/logfile
        linkMerge: merge_flattened
      - id: file_prefix
        default: model_fr
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_model
requirements:
  - class: InlineJavascriptRequirement
  - class: StepInputExpressionRequirement