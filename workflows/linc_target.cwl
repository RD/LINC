class: Workflow
cwlVersion: v1.2
id: linc_target
label: linc_target
inputs:
  - id: msin
    type: Directory[]
  - id: cal_solutions
    type: File
  - id: refant
    type: string?
    default: CS00.*
  - id: flag_baselines
    type: string[]?
    default: []
  - id: process_baselines_target
    type: string?
    default: '[CR]S*&'
  - id: filter_baselines
    type: string?
    default: '[CR]S*&'
  - id: do_smooth
    type: boolean?
    default: false
  - id: rfistrategy
    type:
      - File?
      - string?
    default: '$LINC_DATA_ROOT/rfistrategies/lofar-hba-wideband.lua'
  - id: min_unflagged_fraction
    type: float?
    default: 0.5
  - id: compression_bitrate
    type: int?
    default: 16
  - id: raw_data
    type: boolean?
    default: false
  - id: propagatesolutions
    type: boolean?
    default: true
  - id: demix_sources
    type: string[]?
    default:
      - VirA_4_patch
      - CygAGG
      - CasA_4_patch
      - TauAGG
  - id: demix_timeres
    type: float?
    default: 10
  - id: demix_freqres
    type: string?
    default: '48.82kHz'
  - id: demix
    type: boolean?
  - id: apply_tec
    type: boolean?
    default: false
  - id: apply_clock
    type: boolean?
    default: true
  - id: apply_phase
    type: boolean?
    default: false
  - id: apply_RM
    type: boolean?
    default: true
  - id: apply_beam
    type: boolean?
    default: true
  - id: gsmcal_step
    type: string?
    default: phase
  - id: updateweights
    type: boolean?
    default: true
  - id: max_dp3_threads
    type: int?
    default: 10
  - id: memoryperc
    type: int?
    default: 20
  - id: min_separation
    type: int?
    default: 30
  - id: A-Team_skymodel
    type: File?
  - id: target_skymodel
    type: File?
  - id: use_target
    type: boolean?
    default: true
  - id: skymodel_source
    type: string?
    default: TGSS
  - id: avg_timeresolution
    type: int?
    default: 4
  - id: avg_freqresolution
    type: string?
    default: 48.82kHz
  - id: avg_timeresolution_concat
    type: int?
    default: 8
  - id: avg_freqresolution_concat
    type: string?
    default: 97.64kHz
  - id: num_SBs_per_group
    type: int?
  - id: calib_nchan
    type: int?
    default: 0
  - id: reference_stationSB
    type: int?
    default: null
  - id: ionex_server
    type: string?
    default: 'ftp://gssc.esa.int/gnss/products/ionex/'
  - id: ionex_prefix
    type: string?
    default: UQRG
  - id: proxy_server
    type: string?
    default: null
  - id: proxy_port
    type: int?
    default: null
  - id: proxy_type
    type: string?
    default: null
  - id: proxy_user
    type: string?
    default: null
  - id: proxy_pass
    type: string?
    default: null
  - id: clip_sources
    type: string[]?
    default:
      - VirA_4_patch
      - CygAGG
      - CasA_4_patch
      - TauAGG
  - id: clipAteam
    type: boolean?
    default: true
  - id: lbfgs_historysize
    type: int?
    default: 10
  - id: lbfgs_robustdof
    type: float?
    default: 200
  - id: aoflag_reorder
    type: boolean?
    default: false
  - id: aoflag_chunksize
    type: int?
    default: 2000
  - id: aoflag_freqconcat
    type: boolean?
    default: true
  - id: selfcal
    type: boolean?
    default: false
  - id: selfcal_strategy
    type: string?
    default: 'HBA'
  - id: selfcal_hba_imsize
    type: int[]?
    default: [20000,20000]
  - id: selfcal_hba_uvlambdamin
    type: float?
    default: 200
  - id: chunkduration
    type: float?
    default: 0.0
  - id: wsclean_tmpdir
    type: string?
  - id: make_structure_plot
    type: boolean?
    default: true
  - id: selfcal_region
    type: File?
  - id: skymodel_fluxlimit
    type: float?
  - id: output_fullres_data
    type: boolean?
    default: false
outputs:
  - id: inspection
    outputSource:
      - prep/inspection
      - gsmcal/inspection
      - finalize/inspection
    type: File[]
    linkMerge: merge_flattened
  - id: solutions
    outputSource:
      - finalize/solutions
    type: File
  - id: msout
    outputSource:
      - finalize/msout
    type: Directory[]
  - id: logfiles
    outputSource:
      - prep/logfiles
      - gsmcal/logfiles
      - finalize/logfiles
    type: File[]
    linkMerge: merge_flattened
  - id: summary_file
    outputSource:
      - finalize/summary_file
    type: File
steps:
  - id: get_targetname
    in:
      - id: msin
        linkMerge: merge_flattened
        source:
          - msin
    out:
      - id: targetname
      - id: logfile
    run: ../steps/get_targetname.cwl
    label: get_targetname
  - id: prep
    in:
      - id: msin
        source:
          - msin
      - id: cal_solutions
        source: cal_solutions
      - id: flag_baselines
        source:
          - flag_baselines
      - id: process_baselines_target
        source: process_baselines_target
      - id: num_SBs_per_group
        source: num_SBs_per_group
      - id: reference_stationSB
        source: reference_stationSB
      - id: filter_baselines
        source: filter_baselines
      - id: raw_data
        source: raw_data
      - id: demix_sources
        source:
          - demix_sources
      - id: demix_freqres
        source: demix_freqres
      - id: demix_timeres
        source: demix_timeres
      - id: demix
        source: demix
      - id: clipAteam
        source: clipAteam
      - id: apply_tec
        source: apply_tec
      - id: apply_clock
        source: apply_clock
      - id: apply_phase
        source: apply_phase
      - id: apply_RM
        source: apply_RM
      - id: apply_beam
        source: apply_beam
      - id: updateweights
        source: updateweights
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: memoryperc
        source: memoryperc
      - id: min_separation
        source: min_separation
      - id: A-Team_skymodel
        source: A-Team_skymodel
      - id: avg_timeresolution
        source: avg_timeresolution
      - id: avg_freqresolution
        source: avg_freqresolution
      - id: ionex_server
        source: ionex_server
      - id: ionex_prefix
        source: ionex_prefix
      - id: proxy_server
        source: proxy_server
      - id: proxy_port
        source: proxy_port
      - id: proxy_type
        source: proxy_type
      - id: proxy_user
        source: proxy_user
      - id: proxy_pass
        source: proxy_pass
      - id: clip_sources
        source: clip_sources
      - id: target_skymodel
        source: target_skymodel
      - id: targetname
        source: get_targetname/targetname
      - id: use_target
        source: use_target
      - id: skymodel_source
        source: skymodel_source
      - id: lbfgs_historysize
        source: lbfgs_historysize
      - id: lbfgs_robustdof
        source: lbfgs_robustdof
      - id: skymodel_fluxlimit
        source: skymodel_fluxlimit
    out:
      - id: compare_stations_filter
      - id: outh5parm
      - id: inspection
      - id: msout
      - id: check_Ateam_separation.json
      - id: prep_flags_join_out
      - id: initial_flags_join_out
      - id: logfiles
      - id: target_sourcedb
      - id: total_bandwidth
      - id: midfreq
      - id: groupnames
      - id: filenames
      - id: out_demix_sources
      - id: out_clip_sources
      - id: out_demix
    run: ./linc_target/prep.cwl
    label: prep
  - id: gsmcal
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: prep/msout
      - id: filter_baselines
        source: process_baselines_target
      - id: target_skymodel
        source: prep/target_sourcedb
      - id: targetname
        source: get_targetname/targetname
      - id: do_smooth
        source: do_smooth
      - id: propagatesolutions
        source: propagatesolutions
      - id: avg_timeresolution_concat
        source: avg_timeresolution_concat
      - id: avg_freqresolution_concat
        source: avg_freqresolution_concat
      - id: min_unflagged_fraction
        source: min_unflagged_fraction
      - id: refant
        source: refant
      - id: rfistrategy
        source: rfistrategy
      - id: aoflag_reorder
        source: aoflag_reorder
      - id: aoflag_chunksize
        source: aoflag_chunksize
      - id: aoflag_freqconcat
        source: aoflag_freqconcat
      - id: skymodel_source
        source: skymodel_source
      - id: gsmcal_step
        source: gsmcal_step
      - id: process_baselines_target
        source: process_baselines_target
      - id: insolutions
        source: prep/outh5parm
      - id: selfcal
        source: selfcal
      - id: selfcal_strategy
        source: selfcal_strategy
      - id: selfcal_hba_imsize
        source: selfcal_hba_imsize
      - id: chunkduration
        source: chunkduration
      - id: wsclean_tmpdir
        source: wsclean_tmpdir
      - id: total_bandwidth
        source: prep/total_bandwidth
      - id: midfreq
        source: prep/midfreq
      - id: groupnames
        source: prep/groupnames
      - id: filenames
        source: prep/filenames
      - id: selfcal_region
        source: selfcal_region
      - id: calib_nchan
        source: 
          - calib_nchan
          - selfcal
        valueFrom: '$(self[0] == null ? (self[1] ? null : 0) : self[0])'
      - id: selfcal_hba_uvlambdamin
        source: selfcal_hba_uvlambdamin
    out:
      - id: msout
      - id: outh5parm
      - id: outsolutions
      - id: filter_out
      - id: bad_antennas
      - id: outh5parm_logfile
      - id: Ateam_flags_join_out
      - id: inspection
      - id: logfiles
      - id: removed_bands
      - id: out_refant
    run: ./linc_target/gsmcal.cwl
    label: gsmcal
  - id: finalize
    in:
      - id: msin
        source:
          - gsmcal/msout
      - id: msin_hires
        source: prep/msout
      - id: input_h5parm
        source: gsmcal/outh5parm
      - id: inh5parm_logfile
        source: gsmcal/outh5parm_logfile
      - id: gsmcal_step
        source: gsmcal_step
      - id: process_baselines_target
        source: process_baselines_target
      - id: filter_baselines
        source: gsmcal/filter_out
      - id: compare_stations_filter
        source: prep/compare_stations_filter
      - id: bad_antennas
        source: gsmcal/bad_antennas
      - id: insolutions
        source: gsmcal/outsolutions
      - id: compression_bitrate
        source: compression_bitrate
      - id: skymodel_source
        source: skymodel_source
      - id: total_bandwidth
        source: prep/total_bandwidth
      - id: check_Ateam_separation.json
        source: prep/check_Ateam_separation.json
      - id: flags
        linkMerge: merge_flattened
        source:
          - prep/initial_flags_join_out
          - prep/prep_flags_join_out
          - gsmcal/Ateam_flags_join_out
      - id: clip_sources
        source: prep/out_clip_sources
      - id: demix
        source: prep/out_demix
      - id: demix_sources
        source: prep/out_demix_sources
      - id: removed_bands
        source: gsmcal/removed_bands
      - id: min_unflagged_fraction
        source: min_unflagged_fraction
      - id: refant
        source: gsmcal/out_refant
      - id: targetname
        source: get_targetname/targetname
      - id: wsclean_tmpdir
        source: wsclean_tmpdir
      - id: make_structure_plot
        source: make_structure_plot
      - id: apply_RM
        source: apply_RM
      - id: ionex_prefix
        source: ionex_prefix
      - id: apply_solutions_fullres
        source: output_fullres_data
      - id: groupnames
        source: prep/groupnames
      - id: filenames
        source: prep/filenames
      - id: chunkduration
        source: chunkduration
    out:
      - id: msout
      - id: solutions
      - id: logfiles
      - id: inspection
      - id: summary_file
    run: ./linc_target/finalize.cwl
    label: finalize
requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement
  - class: MultipleInputFeatureRequirement
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
