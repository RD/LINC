class: Workflow
cwlVersion: v1.2
id: predict_calibrate_fr
label: predict_calibrate_fr
inputs:
  - id: max_dp3_threads
    type: int?
  - id: msin
    type: Directory
  - id: skymodel
    type:
      - File
      - Directory
  - id: propagatesolutions
    type: boolean?
    default: true
  - id: antennaconstraint
    type: string[]?
  - id: instrument
    type: string?
    default: 'LBA'
outputs:
  - id: msout
    outputSource:
      - remove_col/msout
    type: Directory
  - id: add_corrected_col.log
    outputSource:
      - concat_logfiles_addcol/output
    type: File
  - id: predict_targ.log
    outputSource:
      - concat_logfiles_predict/output
    type: File
  - id: model_targ.log
    outputSource:
      - concat_logfiles_model/output
    type: File
  - id: calib_targ.log
    outputSource:
      - concat_logfiles_calib_targ/output
    type: File
  - id: outh5parm
    outputSource:
      - calib_targ/h5parm
    type: File
  - id: BLsmooth.log
    outputSource:
      - prepare_fr/BLsmooth.log
    type: File
steps:
  - id: add_corrected_col
    in:
      - id: msin
        source: msin
      - id: max_dp3_threads
        source: max_dp3_threads
    out:
      - id: msout
      - id: logfile
    run: ../../steps/dp3_addcol.cwl
    label: add_corrected_col
  - id: predict_targ
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: add_corrected_col/msout
      - id: msout_datacolumn
        default: MODEL_DATA
      - id: sources_db
        source: skymodel
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: beammode
        default: 'array_factor'
      - id: usebeammodel
        default: true
      - id: usechannelfreq
        default: true
      - id: onebeamperpatch
        default: true
    out:
      - id: msout
      - id: logfile
    run: ../../steps/predict.cwl
    label: predict_targ
  - id: set_corrected
    in:
      - id: msin
        source: predict_targ/msout
      - id: command
        default: "set CORRECTED_DATA = DATA"
    out:
      - id: msout
      - id: logfile
    run: ../../steps/taql.cwl
    label: set_corrected
  - id: prepare_fr
    in:
      - id: msin
        source: set_corrected/msout
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: instrument
        source: instrument
      - id: do_smooth
        default: false
    out:
      - id: msout
      - id: model_fr.log
      - id: BLsmooth.log
    run: ../linc_calibrator/prep_fr.cwl
  - id: calib_targ
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: prepare_fr/msout
        valueFrom: $([self])
      - id: msin_datacolumn
        default: CIRC_PHASEDIFF_DATA
      - id: modeldatacolumns
        default: 
          - 'FR_MODEL_DATA'
      - id: mode
        default: diagonalphase
      - id: uvlambdamin
        default: 100.
      - id: minvisratio
        default: 0.5
      - id: maxiter
        default: 500
      - id: solint
        default: 30
      - id: tolerance
        default: 1e-3
      - id: propagate_solutions
        source: propagatesolutions
      - id: propagate_converged_only
        default: true
      - id: smoothnessconstraint
        default: 5e6
      - id: smoothnessreffrequency
        default: 54e6
      - id: antennaconstraint
        source: antennaconstraint
    out:
      - id: msout
      - id: h5parm
      - id: logfile
    run: ../../steps/ddecal.cwl
    label: calib_targ
  - id: remove_col
    in:
      - id: msin
        source: calib_targ/msout
      - id: task
        default: "ALTER TABLE"
      - id: command
        default: "DELETE COLUMN CIRC_PHASEDIFF_DATA, FR_MODEL_DATA"
    out:
      - id: msout
      - id: logfile
    run: ../../steps/taql.cwl
    label: remove_col
  - id: concat_logfiles_predict
    in:
      - id: file_list
        source:
          - predict_targ/logfile
      - id: file_prefix
        default: predict_targ
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_predict
  - id: concat_logfiles_calib_targ
    in:
      - id: file_list
        source:
          - calib_targ/logfile
          - remove_col/logfile
        linkMerge: merge_flattened
      - id: file_prefix
        default: calib_targ_faraday
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib_targ
  - id: concat_logfiles_addcol
    in:
      - id: file_list
        source:
          - add_corrected_col/logfile
      - id: file_prefix
        default: add_corrected_col
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_addcol
  - id: concat_logfiles_model
    in:
      - id: file_list
        source:
          - set_corrected/logfile
          - prepare_fr/model_fr.log
        linkMerge: merge_flattened
      - id: file_prefix
        default: model_targ_fr
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_model
requirements:
  - class: InlineJavascriptRequirement
  - class: StepInputExpressionRequirement