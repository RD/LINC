class: Workflow
cwlVersion: v1.2
id: apply_calibrate_tec
label: apply_calibrate_tec
inputs:
  - id: max_dp3_threads
    type: int?
  - id: msin
    type: Directory
  - id: input_h5parm
    type: File
  - id: do_smooth
    type: boolean?
  - id: propagatesolutions
    type: boolean?
  - id: correction
    type: string
  - id: msin_baseline
    type: string?
  - id: antennaconstraint
    type: string[]?
  - id: antennaconstraint_superterp
    type: string[]?
  - id: antennaconstraint_CR
    type: string[]?
  - id: execute
    type: boolean?
outputs:
  - id: msout
    outputSource: 
      - calib_targ_amp/msout
      - calib_targ2/msout
    pickValue: first_non_null
    type: Directory
  - id: BLsmooth1.log
    outputSource:
      - concat_logfiles_BLsmooth/output
    type: File
  - id: BLsmooth2.log
    outputSource:
      - BLsmooth2/logfile
    type: File
  - id: apply_targ.log
    outputSource:
      - concat_logfiles_apply_targ/output
    type: File
  - id: calib_targ1.log
    outputSource:
      - concat_logfiles_calib_targ1/output
    type: File
  - id: apply_targ1.log
    outputSource:
      - concat_logfiles_apply_targ1/output
    type: File
  - id: calib_targ2.log
    outputSource:
      - concat_logfiles_calib_targ2/output
    type: File
  - id: apply_targ2.log
    outputSource:
      - concat_logfiles_apply_targ2/output
    pickValue: all_non_null
    type: File
  - id: calib_targ_amp.log
    outputSource:
      - concat_logfiles_calib_targ_amp/output
    type: File
    pickValue: all_non_null
  - id: outh5parm1
    outputSource:
      - calib_targ1/h5parm
    type: File
  - id: outh5parm2
    outputSource:
      - calib_targ2/h5parm
    type: File
  - id: outh5parm_amp
    outputSource:
      - calib_targ_amp/h5parm
    type: File
    pickValue: all_non_null
steps:
  - id: apply_targ
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: msin
      - id: msin_datacolumn
        default: CORRECTED_DATA
      - id: parmdb
        source: input_h5parm
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: correction
        source: correction
      - id: solset
        default: target
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    label: apply_targ
  - id: BLsmooth1
    in:
      - id: msin
        source: apply_targ/msout
      - id: do_smooth
        source: do_smooth
      - id: in_column_name
        default: CORRECTED_DATA
      - id: restore
        default: true
    out:
      - id: msout
      - id: logfile
    run: ../../steps/blsmooth.cwl
    label: BLsmooth1
  - id: BLsmooth_model
    in:
      - id: msin
        source: BLsmooth1/msout
      - id: do_smooth
        source: do_smooth
      - id: in_column_name
        default: MODEL_DATA
      - id: out_column
        default: MODEL_DATA
      - id: restore
        default: true
    out:
      - id: msout
      - id: logfile
    run: ../../steps/blsmooth.cwl
    label: BLsmooth_model
  - id: calib_targ1
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: BLsmooth_model/msout
        valueFrom: $([self])
      - id: msin_datacolumn
        default: SMOOTHED_DATA
      - id: msin_baseline
        source: msin_baseline
      - id: modeldatacolumns
        default: 
          - 'MODEL_DATA'
      - id: mode
        default: tec
      - id: uvlambdamin
        default: 100.
      - id: uvmmax
        default: 80e3
      - id: maxiter
        default: 300
      - id: solint
        default: 15
      - id: nchan
        default: 8
      - id: stepsize
        default: 0.2
      - id: tolerance
        default: 1e-3
      - id: propagate_solutions
        source: propagatesolutions
      - id: approximatetec
        default: true
      - id: maxapproxiter
        default: 250
      - id: approxtolerance
        default: 1e3
      - id: antennaconstraint
        source: antennaconstraint_superterp
    out:
      - id: msout
      - id: h5parm
      - id: logfile
    run: ../../steps/ddecal.cwl
    label: calib_targ1
  - id: apply_targ1
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: calib_targ1/msout
      - id: msin_datacolumn
        default: CORRECTED_DATA
      - id: parmdb
        source: calib_targ1/h5parm
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: correction
        default: tec000
      - id: missingantennabehavior
        default: unit
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    label: apply_targ1
  - id: BLsmooth2
    in:
      - id: msin
        source: apply_targ1/msout
      - id: do_smooth
        source: do_smooth
      - id: in_column_name
        default: CORRECTED_DATA
      - id: restore
        default: true
    out:
      - id: msout
      - id: logfile
    run: ../../steps/blsmooth.cwl
    label: BLsmooth2
  - id: calib_targ2
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: BLsmooth2/msout
        valueFrom: $([self])
      - id: msin_datacolumn
        default: SMOOTHED_DATA
      - id: modeldatacolumns
        default: 
          - 'MODEL_DATA'
      - id: mode
        default: tec
      - id: uvlambdamin
        default: 100.
      - id: uvmmax
        default: 80e3
      - id: maxiter
        default: 300
      - id: solint
        default: 1
      - id: nchan
        default: 4
      - id: stepsize
        default: 0.2
      - id: tolerance
        default: 1e-3
      - id: propagate_solutions
        source: propagatesolutions
      - id: approximatetec
        default: true
      - id: maxapproxiter
        default: 250
      - id: approxtolerance
        default: 1e3
      - id: antennaconstraint
        source: antennaconstraint_CR
    out:
      - id: msout
      - id: h5parm
      - id: logfile
    run: ../../steps/ddecal.cwl
    label: calib_targ2
  - id: apply_targ2
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: calib_targ2/msout
      - id: msin_datacolumn
        default: CORRECTED_DATA
      - id: parmdb
        source: calib_targ2/h5parm
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: correction
        default: tec000
      - id: execute
        source: execute
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    label: apply_targ2
    when: $(inputs.execute)
  - id: calib_targ_amp
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: 
          - apply_targ2/msout
        valueFrom: $([self])
        pickValue: all_non_null
      - id: msin_datacolumn
        default: CORRECTED_DATA
      - id: modeldatacolumns
        default: 
          - 'MODEL_DATA'
      - id: mode
        default: diagonal
      - id: uvlambdamin
        default: 100.
      - id: uvmmax
        default: 80e3
      - id: minvisratio
        default: 0.5
      - id: maxiter
        default: 500
      - id: solint
        default: 120
      - id: nchan
        default: 16
      - id: tolerance
        default: 1e-3
      - id: propagate_solutions
        source: propagatesolutions
      - id: smoothnessconstraint
        default: 3e6
      - id: antennaconstraint
        source: antennaconstraint
      - id: execute
        source: execute
    out:
      - id: msout
      - id: h5parm
      - id: logfile
    run: ../../steps/ddecal.cwl
    when: $(inputs.execute)
    label: calib_targ_amp
  - id: concat_logfiles_apply_targ
    in:
      - id: file_list
        source:
          - apply_targ/logfile
        linkMerge: merge_flattened
      - id: file_prefix
        default: apply_targ
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_apply_targ
  - id: concat_logfiles_BLsmooth
    in:
      - id: file_list
        source:
          - BLsmooth1/logfile
          - BLsmooth_model/logfile
      - id: file_prefix
        default: BLsmooth_model
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_BLsmooth
  - id: concat_logfiles_calib_targ1
    in:
      - id: file_list
        source:
          - calib_targ1/logfile
      - id: file_prefix
        default: calib_targ_slowtec
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib_targ1
  - id: concat_logfiles_apply_targ1
    in:
      - id: file_list
        source:
          - apply_targ1/logfile
        linkMerge: merge_flattened
      - id: file_prefix
        default: apply_targ_slowtec
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_apply_targ1
  - id: concat_logfiles_calib_targ2
    in:
      - id: file_list
        source:
          - calib_targ2/logfile
      - id: file_prefix
        default: calib_targ_fasttec
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib_targ2
  - id: concat_logfiles_apply_targ2
    in:
      - id: file_list
        source:
          - apply_targ2/logfile
        linkMerge: merge_flattened
        pickValue: all_non_null
      - id: file_prefix
        default: apply_targ_slowtec
      - id: execute
        source: execute
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    when: $(inputs.execute)
    label: concat_logfiles_apply_targ2
  - id: concat_logfiles_calib_targ_amp
    in:
      - id: file_list
        source:
          - calib_targ_amp/logfile
        pickValue: all_non_null
      - id: file_prefix
        default: calib_targ_amp
      - id: execute
        source: execute
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    when: $(inputs.execute)
    label: concat_logfiles_calib_targ_amp
requirements: []
