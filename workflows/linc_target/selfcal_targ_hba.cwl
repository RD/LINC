class: Workflow
cwlVersion: v1.2
id: selfcal_target
label: selfcal_target
inputs:
  - id: max_dp3_threads
    type: int?
  - id: msin
    type: Directory[]
  - id: propagatesolutions
    type: boolean?
    default: true
  - id: preapply_h5parm
    type: File[]
  - id: gsmcal_step
    type: string?
    default: 'phaseonly'
  - id: insolutions
    type: File
  - id: selfcal_hba_imsize
    type: int[]?
    default: [20000,20000]
  - id: wsclean_tmpdir
    type: string?
    default: '/tmp'
  - id: process_baselines_target
    type: string?
    default: '[CR]S*&'
  - id: bad_antennas
    type: string?
    default: '[CR]S*&'
  - id: skymodel_source
    type: string?
    default: 'TGSS'
  - id: refant
    type: string?
    default: 'CS001HBA0'
    doc: |
      Reference antenna for phases. By default None.
  - id: calib_nchan
    type: int?
    default: 1
  - id: calib_uvlambdamin
    type: float?
    default: 200.
  - id: targetname
    type: string?
    default: 'pointing'
outputs:
  - id: msout
    outputSource:
      - calib_targ/msout ## needed due to cwltool issue 1785
    type: Directory[]
  - id: outh5parm
    outputSource:
      - calib_targ/h5parm
    type: File
  - id: image_before
    outputSource:
      - image_target/image
    type: File
  - id: outsolutions
    outputSource:
      - write_solutions/outh5parm
    type: File
  - id: inspection
    outputSource:
      - losoto_plot_gsm_P/output_plots
      - losoto_plot_gsm_P2/output_plots
      - losoto_plot_gsm_Pd/output_plots
      - losoto_plot_gsm_Pd2/output_plots
    type: File[]
    linkMerge: merge_flattened
  - id: logfiles
    outputSource:
      - concat_logfiles_losoto/output
      - concat_logfiles_applygsm/output
      - concat_logfiles_image_target/output
      - concat_logfiles_self_calibrate/output
    linkMerge: merge_flattened
    type: File[]
steps:
  - id: h5parm_collector_gsm
    in:
      - id: h5parmFiles
        source: preapply_h5parm
        linkMerge: merge_flattened
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: clobber
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/H5ParmCollector.cwl
    label: h5parm_collector_gsm
  - id: add_missing_stations
    in:
      - id: h5parm
        source: h5parm_collector_gsm/outh5parm
      - id: refh5parm
        source: insolutions
      - id: solset
        default: sol000
      - id: refsolset
        default: target
      - id: soltab_in
        default: phase000
      - id: soltab_out
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $(self.join(''))
      - id: filter
        source: process_baselines_target
      - id: bad_antennas
        source: bad_antennas
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/add_missing_stations.cwl
    label: add_missing_stations
  - id: write_solutions
    in:
      - id: h5parmFile
        source: add_missing_stations/outh5parm
      - id: outsolset
        default: target
      - id: insoltab
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $(self.join(''))
      - id: input_file
        source: insolutions
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: history
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parmcat.cwl
    label: write_solutions

  - id: apply_gsmcal
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: msin
      - id: msin_datacolumn
        default: DATA
      - id: parmdb
        source: write_solutions/outh5parm
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: solset
        default: target
      - id: correction
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $(self.join(''))
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    scatter:
      - msin
    label: apply_gsmcal

  - id: image_target
    in:
      - id: msin
        source: apply_gsmcal/msout
      - id: image_scale
        default: 1.5asec
      - id: auto_threshold
        default: 0.5
      - id: image_size
        source: selfcal_hba_imsize
      - id: niter
        default: 285000
      - id: nmiter
        default: 12
      - id: multiscale
        default: true
      - id: use-wgridder
        default: true
      - id: mgain
        default: 0.8
      - id: parallel-reordering
        default: 4
      - id: parallel-deconvolution
        default: 4096
      - id: channels-out
        default: 6
      - id: join-channels
        default: True
      - id: fit-spectral-pol
        default: 3
      - id: weighting
        default: briggs -1.5
      - id: minuv-l
        default: 0
      - id: tempdir
        source: wsclean_tmpdir
      - id: image_name
        source:
          - targetname
          - skymodel_source
        valueFrom: $(self[0] + '_' + self[1])
      - id: no_model_update
        default: false
      # This can be turned on if we do a separate predict after, not sure if that would be faster.
      # - id: baseline_averaging
      #   default: 0.4908738521234052
      - id: save_source_list
        default: true
    out:
      - id: msout
      - id: dirty_image
      - id: image
      - id: logfile
      - id: sourcelist
    run: ../../steps/wsclean.cwl
    label: image_target
  # END image target field

  # START selfcal
  - id: calib_targ
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: image_target/msout
      - id: msin_datacolumn
        default: DATA
      - id: uvlambdamin
        source: calib_uvlambdamin
      - id: mode
        source: gsmcal_step
        valueFrom: '$(self == "phase" ? "phaseonly" : self)'
      - id: maxiter
        default: 50
      - id: solint
        default: 1
      - id: nchan
        source: calib_nchan
      - id: tolerance
        default: 1e-3
      - id: propagate_solutions
        source: propagatesolutions
      - id: usebeammodel
        default: true
      - id: usechannelfreq
        default: true
      - id: beammode
        default: array_factor
      - id: smoothnessconstraint
        default: 5e6
      - id: smoothnessreffrequency
        default: 12e7
      - id: extradatacolumns
        default:
          - 'MODEL_DATA'
      - id: reusemodel
        default:
          - 'MODEL_DATA'
    out:
      - id: msout ## needed due to cwltool issue 1785
      - id: h5parm
      - id: logfile
    run: ../../steps/ddecal_array.cwl
  # END selfcal

  # START Plot selfcal solutions
  - id: losoto_plot_gsm_P
    in:
      - id: input_h5parm
        source:
          - h5parm_collector_gsm/outh5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        source: skymodel_source
        valueFrom: $("ph_"+self+"_")
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_gsm_P

  - id: losoto_plot_gsm_P2
    in:
      - id: input_h5parm
        source:
          - h5parm_collector_gsm/outh5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: axisInCol
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        source: skymodel_source
        valueFrom: $("ph_"+self+"_")
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_gsm_P2

  - id: losoto_plot_gsm_Pd
    in:
      - id: input_h5parm
        source:
          - h5parm_collector_gsm/outh5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: axisDiff
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        source: skymodel_source
        valueFrom: $("ph_"+self+"_poldif")
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_gsm_Pd

  - id: losoto_plot_gsm_Pd2
    in:
      - id: input_h5parm
        source:
          - h5parm_collector_gsm/outh5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: axisDiff
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        source: skymodel_source
        valueFrom: $("ph_"+self+"_poldif_")
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_gsm_Pd2
  # END plot selfcal solutions

  - id: concat_logfiles_losoto
    in:
      - id: file_list
        source:
          - h5parm_collector_gsm/outh5parm
          - losoto_plot_gsm_P/logfile
          - losoto_plot_gsm_P2/logfile
          - losoto_plot_gsm_Pd/logfile
          - losoto_plot_gsm_Pd2/logfile
          - add_missing_stations/log
          - write_solutions/log
        linkMerge: merge_flattened
      - id: file_prefix
        default: losoto_gsmcal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_losoto

  - id: merge_array_files
    in:
      - id: input
        source: apply_gsmcal/logfile
    out:
      - id: output
    run: ../../steps/merge_array_files.cwl
    label: merge_array_files

  - id: concat_logfiles_applygsm
    in:
      - id: file_list
        source:
          - merge_array_files/output
        linkMerge: merge_flattened
      - id: file_prefix
        default: apply_gsmcal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_applygsm

  - id: concat_logfiles_image_target
    in:
      - id: file_list
        source:
          - image_target/logfile
        linkMerge: merge_flattened
      - id: file_prefix
        default: image_target
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_image_target

  - id: concat_logfiles_self_calibrate
    in:
      - id: file_list
        source:
          - calib_targ/logfile
        linkMerge: merge_flattened
      - id: file_prefix
        default: self_calibrate_target
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_self_calibrate

requirements:
  - class: ScatterFeatureRequirement
  - class: SubworkflowFeatureRequirement
