class: Workflow
cwlVersion: v1.2
id: tec_and_amp
label: TEC'n'amp
inputs:
  - id: max_dp3_threads
    type: int?
  - id: msin
    type: Directory[]
  - id: insolutions
    type: File
  - id: do_smooth
    type: boolean?
  - id: propagatesolutions
    type: boolean?
  - id: antennaconstraint
    type: string[]?
  - id: antennaconstraint_superterp
    type: string[]?
  - id: antennaconstraint_CR
    type: string[]?
  - id: refant
    type: string?
  - id: msin_baseline
    type: string?
  - id: skymodel_source
    type: string?
    default: 'GSM'
  - id: gsmcal_step
    type: string?
    default: 'tec'
  - id: process_baselines_target
    type: string?
    default: '[CR]S*&'
  - id: bad_antennas
    type: string?
    default: '[CR]S*&'
  - id: correction
    type: string
  - id: iteration
    type: string
outputs:
  - id: msout
    outputSource:
      - apply_calibrate_tec/msout
    type: Directory[]
  - id: outh5parm
    outputSource:
      - apply_calibrate_tec/outh5parm2
    type: File[]
    pickValue: all_non_null
  - id: outsolutions
    outputSource:
      - write_solutions_amp/outh5parm
      - write_solutions1/outh5parm
    type: File
    pickValue: first_non_null
  - id: logfile
    outputSource:
      - concat_logfiles_apply_targ/output
      - concat_logfiles_apply_targ1/output
      - concat_logfiles_apply_targ2/output
      - concat_logfiles_BLsmooth/output
      - concat_logfiles_BLsmooth_model/output
      - concat_logfiles_calib_targ1/output
      - concat_logfiles_calib_targ2/output
      - concat_logfiles_calib_targ_amp/output
      - concat_logfiles_losoto_tec1/output
      - concat_logfiles_losoto_tec2/output
      - concat_logfiles_losoto_amp/output
    linkMerge: merge_flattened
    type: File[]
    pickValue: all_non_null
  - id: inspection
    outputSource:
      - losoto_plot_tec1/output_plots
      - losoto_plot_tec2/output_plots
      - losoto_plot_amp/output_plots
      - losoto_plot_phase/output_plots
      - losoto_plot_phase2/output_plots
      - losoto_plot_phasediff/output_plots
      - losoto_plot_ampSmooth/output_plots
    linkMerge: merge_flattened
    type: File[]
    pickValue: all_non_null
steps:
  - id: apply_calibrate_tec
    in:
      - id: msin
        source: msin
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: input_h5parm
        source: insolutions
      - id: do_smooth
        source: do_smooth
      - id: propagatesolutions
        source: propagatesolutions
      - id: antennaconstraint
        source: antennaconstraint
      - id: antennaconstraint_superterp
        source: antennaconstraint_superterp
      - id: antennaconstraint_CR
        source: antennaconstraint_CR
      - id: correction
        source: correction
      - id: msin_baseline
        source: msin_baseline
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: msout
      - id: BLsmooth1.log
      - id: BLsmooth2.log
      - id: apply_targ.log
      - id: calib_targ1.log
      - id: apply_targ1.log
      - id: calib_targ2.log 
      - id: apply_targ2.log
      - id: calib_targ_amp.log
      - id: outh5parm1
      - id: outh5parm2
      - id: outh5parm_amp
    run: ./apply_calibrate_tec.cwl
    scatter:
      - msin
    label: apply_calibrate_tec
  - id: h5parm_collector_tec1
    in:
      - id: h5parmFiles
        source:
          - apply_calibrate_tec/outh5parm1
        linkMerge: merge_flattened
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: clobber
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/H5ParmCollector.cwl
    label: H5parm_collector_tec1
  - id: add_missing_stations1
    in:
      - id: h5parm
        source: h5parm_collector_tec1/outh5parm
      - id: refh5parm
        source: insolutions
      - id: solset
        default: sol000
      - id: refsolset
        default: target
      - id: soltab_in
        source: gsmcal_step
        valueFrom: $(self+'000')
      - id: soltab_out
        source:
          - skymodel_source
          - gsmcal_step
          - iteration
        valueFrom: $("slow"+self.join(''))
      - id: filter
        source: process_baselines_target
      - id: bad_antennas
        source: bad_antennas
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/add_missing_stations.cwl
    label: add_missing_stations1
  - id: write_solutions1
    in:
      - id: h5parmFile
        source: add_missing_stations1/outh5parm
      - id: outsolset
        default: target
      - id: insoltab
        source:
          - skymodel_source
          - gsmcal_step
          - iteration
        valueFrom: $("slow"+self.join(''))
      - id: input_file
        source: insolutions
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: history
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parmcat.cwl
    label: write_solutions1
  - id: h5parm_collector_tec2
    in:
      - id: h5parmFiles
        source:
          - apply_calibrate_tec/outh5parm2
        linkMerge: merge_flattened
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: clobber
        default: true
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/H5ParmCollector.cwl
    when: $(inputs.execute)
    label: H5parm_collector_tec2
  - id: add_missing_stations2
    in:
      - id: h5parm
        source:
          - h5parm_collector_tec2/outh5parm
        pickValue: all_non_null
      - id: refh5parm
        source: insolutions
      - id: solset
        default: sol000
      - id: refsolset
        default: target
      - id: soltab_in
        source: gsmcal_step
        valueFrom: $(self+'000')
      - id: soltab_out
        source:
          - skymodel_source
          - gsmcal_step
          - iteration
        valueFrom: $(self.join(''))
      - id: filter
        source: process_baselines_target
      - id: bad_antennas
        source: bad_antennas
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/add_missing_stations.cwl
    when: $(inputs.execute)
    label: add_missing_stations2
  - id: write_solutions2
    in:
      - id: h5parmFile
        source:
          - add_missing_stations2/outh5parm
        pickValue: all_non_null
      - id: outsolset
        default: target
      - id: insoltab
        source:
          - skymodel_source
          - gsmcal_step
          - iteration
        valueFrom: $(self.join(''))
      - id: input_file
        source: write_solutions1/outh5parm
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: history
        default: true
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parmcat.cwl
    when: $(inputs.execute)
    label: write_solutions2
  - id: h5parm_collector_amp
    in:
      - id: h5parmFiles
        source:
          - apply_calibrate_tec/outh5parm_amp
        linkMerge: merge_flattened
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: clobber
        default: true
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/H5ParmCollector.cwl
    when: $(inputs.execute)
    label: H5parm_collector_amp
  - id: add_missing_stations_amp
    in:
      - id: h5parm
        source:
          - losoto_amplitude/output_h5parm
        pickValue: all_non_null
      - id: refh5parm
        source: insolutions
      - id: solset
        default: sol000
      - id: refsolset
        default: target
      - id: soltab_in
        default: amplitudeSmooth
      - id: soltab_out
        source: skymodel_source
        valueFrom: $(self+"amplitude")
      - id: filter
        source: process_baselines_target
      - id: bad_antennas
        source: bad_antennas
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/add_missing_stations.cwl
    when: $(inputs.execute)
    label: add_missing_stations_amp
  - id: write_solutions_amp
    in:
      - id: h5parmFile
        source: 
          - add_missing_stations_amp/outh5parm
        pickValue: all_non_null
      - id: outsolset
        default: target
      - id: insoltab
        source: skymodel_source
        valueFrom: $(self+"amplitude")
      - id: input_file
        source:
          - write_solutions2/outh5parm
        pickValue: all_non_null
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: history
        default: true
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parmcat.cwl
    when: $(inputs.execute)
    label: write_solutions_amp
  - id: losoto_plot_tec1
    in:
      - id: input_h5parm
        source: h5parm_collector_tec1/outh5parm
      - id: soltab
        default: sol000/tec000
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: minmax
        default:
          - -0.5
          - 0.5
      - id: prefix
        source:
          - gsmcal_step
          - iteration
        valueFrom: $("slow"+self.join('')+"_")
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_tec1
  - id: losoto_plot_tec2
    in:
      - id: input_h5parm
        source:
          - h5parm_collector_tec2/outh5parm
        pickValue: all_non_null
      - id: soltab
        default: sol000/tec000
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: minmax
        default:
          - -0.5
          - 0.5
      - id: prefix
        source:
          - gsmcal_step
          - iteration
        valueFrom: $(self.join('')+"_")
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    when: $(inputs.execute)
    label: losoto_plot_tec2
  - id: losoto_plot_amp
    in:
      - id: input_h5parm
        source: 
          - h5parm_collector_amp/outh5parm
        pickValue: all_non_null
      - id: soltab
        default: sol000/amplitude000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: true
      - id: prefix
        default: amp_
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    when: $(inputs.execute)
    label: losoto_plot_amp
  - id: losoto_plot_phase
    in:
      - id: input_h5parm
        source:
          - h5parm_collector_amp/outh5parm
        pickValue: all_non_null
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: true
      - id: prefix
        default: ph_
      - id: refAnt
        source: refant
      - id: minmax
        default:
          - -3.14
          - +3.14
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    when: $(inputs.execute)
    label: losoto_plot_phase
  - id: losoto_plot_phase2
    in:
      - id: input_h5parm
        source:
          - h5parm_collector_amp/outh5parm
        pickValue: all_non_null
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: axisInCol
        default: pol
      - id: plotFlag
        default: true
      - id: prefix
        default: ph_
      - id: refAnt
        source: refant
      - id: minmax
        default:
          - -3.14
          - +3.14
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    when: $(inputs.execute)
    label: losoto_plot_phase2
  - id: losoto_plot_phasediff
    in:
      - id: input_h5parm
        source:
          - h5parm_collector_amp/outh5parm
        pickValue: all_non_null
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: true
      - id: prefix
        default: ph_poldif
      - id: axisDiff
        default: pol
      - id: refAnt
        source: refant
      - id: minmax
        default:
          - -1.0
          - +1.0
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    when: $(inputs.execute)
    label: losoto_plot_phasediff
  - id: losoto_amplitude
    in:
      - id: input_h5parm
        source: 
          - h5parm_collector_amp/outh5parm
        pickValue: all_non_null
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: output_h5parm
      - id: logfiles
    run: ./amplitude.cwl
    when: $(inputs.execute)
    label: losoto_amplitude
  - id: losoto_plot_ampSmooth
    in:
      - id: input_h5parm
        source:
          - losoto_amplitude/output_h5parm
        pickValue: all_non_null
      - id: soltab
        default: sol000/amplitudeSmooth
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: plotFlag
        default: true
      - id: prefix
        default: ampSmooth_
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    when: $(inputs.execute)
    label: losoto_plot_ampSmooth
  - id: concat_logfiles_apply_targ
    in:
      - id: file_list
        source:
          - apply_calibrate_tec/apply_targ1.log
        linkMerge: merge_flattened
      - id: file_prefix
        source:
          - correction
          - iteration
        valueFrom: $("apply_targ_"+self.join(''))
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_apply_targ
  - id: concat_logfiles_BLsmooth_model
    in:
      - id: file_list
        source:
          - apply_calibrate_tec/BLsmooth1.log
      - id: file_prefix
        source: iteration
        valueFrom: $("BLsmooth_model"+self)
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_BLsmooth_model
  - id: concat_logfiles_BLsmooth
    in:
      - id: file_list
        source:
          - apply_calibrate_tec/BLsmooth2.log
      - id: file_prefix
        source: iteration
        valueFrom: $("BLsmooth"+self)
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_BLsmooth
  - id: concat_logfiles_calib_targ1
    in:
      - id: file_list
        source:
          - apply_calibrate_tec/calib_targ1.log
      - id: file_prefix
        source: iteration
        valueFrom: $("calib_targ_slowtec"+self)
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib_targ1
  - id: concat_logfiles_apply_targ1
    in:
      - id: file_list
        source:
          - apply_calibrate_tec/apply_targ1.log
        linkMerge: merge_flattened
      - id: file_prefix
        source: iteration
        valueFrom: $("apply_targ_slowtec"+self)
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_apply_targ1
  - id: concat_logfiles_calib_targ2
    in:
      - id: file_list
        source:
          - apply_calibrate_tec/calib_targ2.log
      - id: file_prefix
        source: iteration
        valueFrom: $("calib_targ_fasttec"+self)
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib_targ2
  - id: concat_logfiles_apply_targ2
    in:
      - id: file_list
        source:
          - apply_calibrate_tec/apply_targ2.log
        linkMerge: merge_flattened
      - id: file_prefix
        source: iteration
        valueFrom: $("apply_targ_fasttec"+self)
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    when: $(inputs.execute)
    label: concat_logfiles_apply_targ2
  - id: concat_logfiles_calib_targ_amp
    in:
      - id: file_list
        source:
          - apply_calibrate_tec/calib_targ_amp.log
      - id: file_prefix
        default: calib_targ_amp
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    when: $(inputs.execute)
    label: concat_logfiles_calib_targ_amp
  - id: concat_logfiles_losoto_tec1
    in:
      - id: file_list
        source:
          - h5parm_collector_tec1/log
          - losoto_plot_tec1/logfile
          - add_missing_stations1/log
          - write_solutions1/log
        linkMerge: merge_flattened
      - id: file_prefix
        source: iteration
        valueFrom: $("losoto_slowtec"+self)
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_losoto_tec1
  - id: concat_logfiles_losoto_tec2
    in:
      - id: file_list
        source:
          - h5parm_collector_tec2/log
          - losoto_plot_tec2/logfile
          - add_missing_stations2/log
          - write_solutions2/log
        linkMerge: merge_flattened
        pickValue: all_non_null
      - id: file_prefix
        source: iteration
        valueFrom: $("losoto_fasttec"+self)
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    when: $(inputs.execute)
    label: concat_logfiles_losoto_tec2
  - id: concat_logfiles_losoto_amp
    in:
      - id: file_list
        source:
          - h5parm_collector_amp/log
          - losoto_plot_amp/logfile
          - losoto_plot_phase/logfile
          - losoto_plot_phase2/logfile
          - losoto_plot_phasediff/logfile
          - losoto_amplitude/logfiles
          - losoto_plot_ampSmooth/logfile
          - add_missing_stations_amp/log
          - write_solutions_amp/log
        linkMerge: merge_flattened
        pickValue: all_non_null
      - id: file_prefix
        default: losoto_amp
      - id: execute
        source: iteration
        valueFrom: '$(self=="1" ? true : false)'
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    when: $(inputs.execute)
    label: concat_logfiles_losoto_amp
requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement