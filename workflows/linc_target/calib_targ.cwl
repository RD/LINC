class: Workflow
cwlVersion: v1.2
id: calibrate_target
label: calibrate_target
inputs:
  - id: max_dp3_threads
    type: int?
  - id: msin
    type: Directory
  - id: skymodel
    type:
      - File?
      - Directory?
  - id: do_smooth
    type: boolean?
    default: false
  - id: propagatesolutions
    type: boolean?
    default: true
  - id: gsmcal_step
    type: string?
    default: 'phase'
  - id: smoothnessconstraint
    type: float?
    default: 0.
  - id: smoothnessreffrequency
    type: float?
    default: 0.
  - id: beammode
    type: string?
  - id: nchan
    type: int?
    default: 1
  - id: model_column
    type: string[]?
    default: []
  - id: uvlambdamin
    type: float?
outputs:
  - id: msout
    outputSource:
      - calib_targ/msout
    type: Directory
  - id: BLsmooth.log
    outputSource:
      - BLsmooth/logfile
    type: File
  - id: gaincal.log
    outputSource:
      - concat_logfiles_gaincal/output
    type: File
  - id: outh5parm
    outputSource:
      - calib_targ/h5parm
    type: File
steps:
  - id: BLsmooth
    in:
      - id: msin
        source: msin
      - id: do_smooth
        source: do_smooth
      - id: in_column_name
        default: DATA
      - id: restore
        default: true
    out:
      - id: msout
      - id: logfile
    run: ../../steps/blsmooth.cwl
    label: BLsmooth
  - id: calib_targ
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: BLsmooth/msout
        valueFrom: $([self])
      - id: msin_datacolumn
        default: SMOOTHED_DATA
      - id: uvlambdamin
        source: uvlambdamin
      - id: mode
        source: gsmcal_step
        valueFrom: '$(self == "phase" ? "phaseonly" : self)'
      - id: sourcedb
        source: skymodel
      - id: maxiter
        default: 50
      - id: solint
        default: 1
      - id: nchan
        source: nchan
      - id: tolerance
        default: 1e-3
      - id: propagate_solutions
        source: propagatesolutions
      - id: usebeammodel
        default: true
      - id: usechannelfreq
        default: true
      - id: beammode
        source: beammode
      - id: smoothnessconstraint
        source: smoothnessconstraint
      - id: smoothnessreffrequency
        source: smoothnessreffrequency
      - id: modeldatacolumns
        source: model_column
    out:
      - id: msout
      - id: h5parm
      - id: logfile
    run: ../../steps/ddecal.cwl
  - id: concat_logfiles_gaincal
    in:
      - id: file_list
        source:
          - calib_targ/logfile
      - id: file_prefix
        default: gaincal
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_gaincal
requirements:
  - class: MultipleInputFeatureRequirement
  - class: InlineJavascriptRequirement
  - class: StepInputExpressionRequirement
