class: Workflow
cwlVersion: v1.2
id: selfcal_target
label: selfcal_target
inputs:
  - id: max_dp3_threads
    type: int?
  - id: msin
    type: Directory[]
  - id: skymodel
    type:
      - File
      - Directory
  - id: propagatesolutions
    type: boolean?
    default: true
  - id: refant
    type: string?
    default: 'CS001LBA'
  - id: do_smooth
    type: boolean?
  - id: skymodel_source
    type: string?
    default: 'GSM'
  - id: gsmcal_step
    type: string?
    default: 'tec'
  - id: process_baselines_target
    type: string?
    default: '[CR]S*&'
  - id: bad_antennas
    type: string?
    default: '[CR]S*&'
  - id: insolutions
    type: File
  - id: targetname
    type: string?
    default: 'pointing'
  - id: wsclean_tmpdir
    type: string?
    default: '/tmp'
  - id: total_bandwidth
    type: float?
    default: 50
  - id: midfreq
    type: float?
    default: 50e6
  - id: selfcal_region
    type: File?
  - id: rfistrategy
    type:
      - File?
      - string?
  - id: aoflag_reorder
    type: boolean?
    default: false
  - id: aoflag_chunksize
    type: int?
    default: 2000
  - id: aoflag_freqconcat
    type: boolean?
    default: true
  - id: execute
    type: boolean?
    default: true
outputs:
  - id: msout
    outputSource:
      - tec_and_amp_2/msout
    type: Directory[]
  - id: outh5parm
    outputSource:
      - tec_and_amp_2/outh5parm
    type: File[]
  - id: outsolutions
    outputSource:
      - tec_and_amp_2/outsolutions
    type: File
  - id: logfiles
    outputSource:
      - fr/logfile
      - tec_and_amp/logfile
      - concat_logfiles_apply_amp_1/output
      - imaging_subtract/logfile
      - concat_logfiles_apply_amp_2/output
      - tec_and_amp_2/logfile
    linkMerge: merge_flattened
    type: File[]
  - id: inspection
    outputSource:
      - fr/inspection
      - tec_and_amp/inspection
      - imaging_subtract/inspection
      - tec_and_amp_2/inspection
    type: File[]
    linkMerge: merge_flattened
steps:
  - id: compare_station_list
    in:
      - id: msin
        source: msin
      - id: station_list
        default: "[CS001LBA,CS002LBA,CS003LBA,CS004LBA,CS005LBA,CS006LBA,CS007LBA,CS011LBA,CS013LBA,CS017LBA,CS021LBA,CS024LBA,CS026LBA,CS028LBA,CS030LBA,CS031LBA,CS032LBA,CS101LBA,CS103LBA,CS201LBA,CS301LBA,CS302LBA,CS401LBA,CS501LBA]"
    out:
      - id: match_out
      - id: msout ## needed due to cwltool issue 1785
    run: ../../steps/compare_station_list.cwl
    label: compare_station_list
  - id: compare_station_list_superterp
    in:
      - id: msin
        source: msin
      - id: station_list
        default: "[CS001LBA,CS002LBA,CS003LBA,CS004LBA,CS005LBA,CS006LBA,CS007LBA]"
    out:
      - id: match_out
    run: ../../steps/compare_station_list.cwl
    label: compare_station_list_superterp
  - id: compare_station_list_CR
    in:
      - id: msin
        source: msin
      - id: station_list
        default: "[CS001LBA,CS002LBA,CS003LBA,CS004LBA,CS005LBA,CS006LBA,CS007LBA,CS011LBA,CS013LBA,CS017LBA,CS021LBA,CS024LBA,CS026LBA,CS028LBA,CS030LBA,CS031LBA,CS032LBA,CS101LBA,CS103LBA,CS201LBA,CS301LBA,CS302LBA,CS401LBA,CS501LBA,RS106LBA,RS205LBA,RS305LBA,RS306LBA,RS503LBA]"
    out:
      - id: match_out
    run: ../../steps/compare_station_list.cwl
    label: compare_station_list_CR
  - id: compare_station_list_R
    in:
      - id: msin
        source: msin
      - id: station_list
        default: "[RS208LBA,RS210LBA,RS307LBA,RS310LBA,RS406LBA,RS407LBA,RS409LBA,RS508LBA,RS509LBA]"
      - id: filter
        default: "[CR]*&&"
    out:
      - id: match_out
    run: ../../steps/compare_station_list.cwl
    label: compare_station_list_R
  - id: fr
    in:
      - id: msin
        source: compare_station_list/msout # workaround for CWL bug (https://github.com/common-workflow-language/cwltool/issues/1785)
      - id: skymodel
        source: skymodel
      - id: refant
        source: refant
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: propagatesolutions
        source: propagatesolutions
      - id: antennaconstraint
        source: compare_station_list_superterp/match_out
        valueFrom: $(["["+self.replaceAll('*;',',').replaceAll('*','').replaceAll(';','').replaceAll('&','')+"]"])
        label: remove baseline syntax characters and convert it to a list
      - id: insolutions
        source: insolutions
      - id: process_baselines_target
        source: process_baselines_target
      - id: bad_antennas
        source: bad_antennas
      - id: instrument
        default: 'LBA'
    out:
       - id: msout
       - id: outsolutions
       - id: inspection
       - id: logfile
    run: ./fr.cwl
  - id: tec_and_amp
    in:
      - id: msin
        source: fr/msout
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: insolutions
        source: fr/outsolutions
      - id: do_smooth
        source: do_smooth
      - id: propagatesolutions
        source: propagatesolutions
      - id: antennaconstraint
        source: compare_station_list/match_out
        valueFrom: $(["["+self.replaceAll('*;',',').replaceAll('*','').replaceAll(';','').replaceAll('&','')+"]"]) #remove baseline syntax characters and convert it to a list
      - id: antennaconstraint_superterp
        source: compare_station_list_superterp/match_out
        valueFrom: $(["["+self.replaceAll('*;',',').replaceAll('*','').replaceAll(';','').replaceAll('&','')+"]"]) #remove baseline syntax characters and convert it to a list
      - id: antennaconstraint_CR
        source: compare_station_list_CR/match_out
        valueFrom: $(["["+self.replaceAll('*;',',').replaceAll('*','').replaceAll(';','').replaceAll('&','')+"]"]) #remove baseline syntax characters and convert it to a list
      - id: msin_baseline
        source: compare_station_list_R/match_out
        valueFrom: $(self.replaceAll(';',';!')) #remove negation character of the baseline syntax, but keep it of type string
      - id: refant
        source: refant
      - id: skymodel_source
        source: skymodel_source
      - id: gsmcal_step
        source: gsmcal_step
      - id: process_baselines_target
        source: process_baselines_target
      - id: bad_antennas
        source: bad_antennas
      - id: correction
        default: faraday
      - id: iteration
        default: "1"
    out:
       - id: msout
       - id: outh5parm
       - id: outsolutions
       - id: inspection
       - id: logfile
    run: ./tec_and_amp.cwl
    label: tec_and_amp
  - id: apply_targ_amp
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: tec_and_amp/msout
      - id: msin_datacolumn
        default: CORRECTED_DATA
      - id: parmdb
        source: tec_and_amp/outsolutions
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: solset
        default: target
      - id: correction
        source: skymodel_source
        valueFrom: $(self+"amplitude")
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    scatter:
      - msin
    label: apply_targ_amp
  - id: imaging_subtract
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: apply_targ_amp/msout
      - id: targetname
        source: targetname
      - id: wsclean_tmpdir
        source: wsclean_tmpdir
      - id: total_bandwidth
        source: total_bandwidth
      - id: midfreq
        source: midfreq
      - id: selfcal_region
        source: selfcal_region
      - id: rfistrategy
        source: rfistrategy
      - id: aoflag_reorder
        source: aoflag_reorder
      - id: aoflag_chunksize
        source: aoflag_chunksize
      - id: aoflag_freqconcat
        source: aoflag_freqconcat
      - id: parmdb
        source: tec_and_amp/outsolutions
      - id: skymodel_source
        source: skymodel_source
    out:
      - id: msout
      - id: logfile
      - id: inspection
    run: ./imaging_subtract.cwl
    label: imaging_subtract
  - id: apply_targ_amp_2
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: imaging_subtract/msout
      - id: msin_datacolumn
        default: CORRECTED_DATA
      - id: parmdb
        source: tec_and_amp/outsolutions
      - id: msout_datacolumn
        default: CORRECTED_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: solset
        default: target
      - id: correction
        source: skymodel_source
        valueFrom: $(self+"amplitude")
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    scatter:
      - msin
    label: apply_targ_amp_2
  - id: tec_and_amp_2
    in:
      - id: msin
        source: apply_targ_amp_2/msout
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: insolutions
        source: tec_and_amp/outsolutions
      - id: do_smooth
        source: do_smooth
      - id: propagatesolutions
        source: propagatesolutions
      - id: antennaconstraint
        source: compare_station_list/match_out
        valueFrom: $(["["+self.replaceAll('*;',',').replaceAll('*','').replaceAll(';','').replaceAll('&','')+"]"])
      - id: antennaconstraint_superterp
        source: compare_station_list_superterp/match_out
        valueFrom: $(["["+self.replaceAll('*;',',').replaceAll('*','').replaceAll(';','').replaceAll('&','')+"]"])
      - id: antennaconstraint_CR
        source: compare_station_list_CR/match_out
        valueFrom: $(["["+self.replaceAll('*;',',').replaceAll('*','').replaceAll(';','').replaceAll('&','')+"]"])
      - id: msin_baseline
        source: compare_station_list_R/match_out
        valueFrom: $(self.replaceAll(';',';!'))
      - id: refant
        source: refant
      - id: skymodel_source
        source: skymodel_source
      - id: gsmcal_step
        source: gsmcal_step
      - id: process_baselines_target
        source: process_baselines_target
      - id: bad_antennas
        source: bad_antennas
      - id: correction
        default: faraday
      - id: iteration
        default: "2"
    out:
       - id: msout
       - id: outh5parm
       - id: outsolutions
       - id: inspection
       - id: logfile
    run: ./tec_and_amp.cwl
    label: tec_and_amp_2
  - id: merge_array_files_amp_1
    in:
      - id: input
        source: apply_targ_amp/logfile
    out:
      - id: output
    run: ../../steps/merge_array_files.cwl
    label: merge_array_files_amp_1
  - id: merge_array_files_amp_2
    in:
      - id: input
        source: apply_targ_amp_2/logfile
    out:
      - id: output
    run: ../../steps/merge_array_files.cwl
    label: merge_array_files_amp_2
  - id: concat_logfiles_apply_amp_1
    in:
      - id: file_list
        source:
          - merge_array_files_amp_1/output
        linkMerge: merge_flattened
      - id: file_prefix
        default: apply_targ_amp_1
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_apply_amp_2
  - id: concat_logfiles_apply_amp_2
    in:
      - id: file_list
        source:
          - merge_array_files_amp_2/output
        linkMerge: merge_flattened
      - id: file_prefix
        default: apply_targ_amp_2
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_apply_amp_2
requirements:
  - class: ScatterFeatureRequirement
  - class: SubworkflowFeatureRequirement