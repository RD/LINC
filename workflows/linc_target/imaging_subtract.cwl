class: Workflow
cwlVersion: v1.2
id: imaging_subtract
label: imaging_subtract
inputs:
  - id: max_dp3_threads
    type: int?
  - id: msin
    type: Directory[]
  - id: targetname
    type: string?
    default: 'pointing'
  - id: wsclean_tmpdir
    type: string?
    default: '/tmp'
  - id: total_bandwidth
    type: float?
    default: 50
  - id: midfreq
    type: float?
    default: 50e6
  - id: selfcal_region
    type: File?
  - id: rfistrategy
    type:
      - File?
      - string?
  - id: aoflag_reorder
    type: boolean?
    default: false
  - id: aoflag_chunksize
    type: int?
    default: 2000
  - id: aoflag_freqconcat
    type: boolean?
    default: true
  - id: parmdb
    type: File
  - id: skymodel_source
    type: string?
    default: 'GSM'
outputs:
  - id: msout
    outputSource:
      - recreate_model/msout
    type: Directory[]
  - id: logfile
    outputSource:
      - concat_logfiles_predict/output
      - concat_logfiles_hires/output
      - concat_logfiles_lowres/output
      - concat_logfiles_aoflag/output
      - concat_logfiles_large/output
      - concat_logfiles_corrupt/output
      - concat_logfiles_recreate/output
    linkMerge: merge_flattened
    type: File[]
    pickValue: all_non_null
  - id: inspection
    outputSource:
      - make_mask_image/image
      - make_mask/mask
      - blank_image_reg/image
      - blank_image_reg_1/image
      - make_beam_reg/region
      - blank_image_reg_2/output_image
      - image_hires/image
      - image_lowres/image
      - image_large/image
    linkMerge: merge_flattened
    type: File[]
    pickValue: all_non_null
steps:
  - id: getFWHM
    in:
      - id: msin
        source: msin
      - id: beamfreq
        source: midfreq
    out:
      - id: fwhm
      - id: msout
    run: ../../steps/getFWHM.cwl
    label: getFWHM
  - id: make_mask_image
    in:
      - id: msin
        source: getFWHM/msout # workaround for CWL bug (https://github.com/common-workflow-language/cwltool/issues/1785)
      - id: image_name
        source: targetname
        valueFrom: '$(self+"_mask")'
      - id: image_size
        source: getFWHM/fwhm
        valueFrom: '$((Math.round(2.1 * self * 3600 / 10.)) % 2 > 0 ? [Math.round(2.1 * self * 3600 / 10.) + 1 , Math.round(2.1 * self * 3600 / 10.) + 1] : [Math.round(2.1 * self * 3600 / 10.), Math.round(2.1 * self * 3600 / 10.)])' ## calculation/estimate of the image size based on the FWHM at the central frequency and using a factor of 2.1 to increase the image size beyond the FWHM and 10 to divide it by the pixel size in arcseconds.
      - id: image_scale
        default: '10arcsec'
      - id: weighting
        default: 'briggs -0.3'
      - id: niter
        default: 1000000
      - id: minuv-l
        default: 30
      - id: parallel-gridding
        default: 2
      - id: maxuv-l
        default: 4500
      - id: mgain
        default: 0.85
      - id: parallel-deconvolution
        default: 512
      - id: local_rms
        default: true
      - id: auto_threshold
        default: 4
      - id: join-channels
        source: total_bandwidth
        valueFrom: '$(Math.round(self/4.e6) > 1)'
      - id: fit-spectral-pol
        source: total_bandwidth
        valueFrom: '$(self > 25e6 ? 5 : Math.round(self/4.e6) > 1 ? 3 : false)'
      - id: channels-out
        source: total_bandwidth
        valueFrom: '$(Math.round(self/4.e6) > 0 ? Math.round(self/4.e6) : 1)'
      - id: deconvolution-channels
        source: total_bandwidth
        valueFrom: '$(self > 25e6 ? 5 : Math.round(self/4.e6) > 1 ? 3 : false)'
      - id: use-wgridder
        default: true
      - id: tempdir
        source: wsclean_tmpdir
    out:
      - id: dirty_image
      - id: psf_image
      - id: model
      - id: image
      - id: logfile
      - id: msout
    run: ../../steps/wsclean.cwl
    label: make_mask_image
  - id: make_mask
    in:
      - id: image
        source: make_mask_image/image
      - id: mask_name
        source: targetname
        valueFrom: '$(self+".mask")'
      - id: atrous_do
        default: true
      - id: threshpix
        default: 5
      - id: frequency
        source: midfreq
    out:
      - id: mask
      - id: logfile
    run: ../../steps/make_mask.cwl
    label: make_mask
  - id: blank_image_reg
    in:
      - id: input_image
        source: make_mask/mask
      - id: region
        source: selfcal_region
      - id: blankval
        default: 1.0
    out:
      - id: image
      - id: logfile
    run: ../../steps/blank_image_reg.cwl
    label: blank_image_reg
    when: $(inputs.region != null)
  - id: image_hires
    in:
      - id: msin
        source: make_mask_image/msout # workaround for CWL bug (https://github.com/common-workflow-language/cwltool/issues/1785)
      - id: image_name
        source: targetname
        valueFrom: '$(self+"_hires")'
      - id: save_source_list
        default: true
      - id: image_size
        source: getFWHM/fwhm
        valueFrom: '$((Math.round(2.1 * self * 3600 / 10.)) % 2 > 0 ? [Math.round(2.1 * self * 3600 / 10.) + 1 , Math.round(2.1 * self * 3600 / 10.) + 1] : [Math.round(2.1 * self * 3600 / 10.), Math.round(2.1 * self * 3600 / 10.)])' ## calculation/estimate of the image size based on the FWHM at the central frequency and using a factor of 2.1 to increase the image size beyond the FWHM and 10 to divide it by the pixel size in arcseconds.
      - id: image_scale
        default: '10arcsec'
      - id: weighting
        default: 'briggs -0.3'
      - id: no_model_update
        default: true
      - id: niter
        default: 1000000
      - id: minuv-l
        default: 30
      - id: parallel-gridding
        default: 2
      - id: baseline_averaging
        default: getFWHM/fwhm
        valueFrom: '$(1.87e3 * 60000. * 2. * Math.PI / (24 * 60 * 60 * Math.round(2.1 * self * 3600 / 10.)) < 1 ? false : 1.87e3 * 60000. * 2. * Math.PI / (24 * 60 * 60 * Math.round(2.1 * self * 3600 / 10.)) > 10 ? 10 : 1.87e3 * 60000. * 2. * Math.PI / (24 * 60 * 60 * Math.round(2.1 * self * 3600 / 10.)))'
      - id: maxuv-l
        default: 4500
      - id: mgain
        default: 0.85
      - id: parallel-deconvolution
        default: 512
      - id: auto_threshold
        default: 3
      - id: fits_mask
        source:
          - make_mask/mask
          - blank_image_reg/image
        pickValue: first_non_null
      - id: fits_model
        source: make_mask_image/model
      - id: fits_image
        source: make_mask_image/image
      - id: join-channels
        source: total_bandwidth
        valueFrom: '$(Math.round(self/4.e6) > 1)'
      - id: fit-spectral-pol
        source: total_bandwidth
        valueFrom: '$(self > 25e6 ? 5 : Math.round(self/4.e6) > 1 ? 3 : false)'
      - id: channels-out
        source: total_bandwidth
        valueFrom: '$(Math.round(self/4.e6) > 0 ? Math.round(self/4.e6) : 1)'
      - id: multiscale
        default: true
      - id: multiscale_scale_bias
        default: 0.6
      - id: deconvolution-channels
        source: total_bandwidth
        valueFrom: '$(self > 25e6 ? 5 : Math.round(self/4.e6) > 1 ? 3 : false)'
      - id: use-wgridder
        default: true
      - id: tempdir
        source: wsclean_tmpdir
      - id: do_predict
        default: false
      - id: reuse_dirty
        source: make_mask_image/dirty_image
      - id: reuse_psf
        source: make_mask_image/psf_image
    out:
      - id: msout
      - id: image
      - id: model
      - id: logfile
    run: ../../steps/wsclean.cwl
    label: image_hires
  - id: predict_hires
    in:
      - id: msin
        source: image_hires/msout # workaround for CWL bug (https://github.com/common-workflow-language/cwltool/issues/1785)
      - id: image_name
        source: targetname
        valueFrom: '$(self+"_hires")'
      - id: image_size
        source: getFWHM/fwhm
        valueFrom: '$((Math.round(2.1 * self * 3600 / 10.)) % 2 > 0 ? [Math.round(2.1 * self * 3600 / 10.) + 1 , Math.round(2.1 * self * 3600 / 10.) + 1] : [Math.round(2.1 * self * 3600 / 10.), Math.round(2.1 * self * 3600 / 10.)])' ## calculation/estimate of the image size based on the FWHM at the central frequency and using a factor of 2.1 to increase the image size beyond the FWHM and 10 to divide it by the pixel size in arcseconds.
      - id: image_scale
        default: '10arcsec'
      - id: padding
        default: 1.8
      - id: fits_model
        source: image_hires/model
      - id: fits_image
        source: image_hires/image
      - id: channels-out
        source: total_bandwidth
        valueFrom: '$(Math.round(self/4.e6) > 0 ? Math.round(self/4.e6) : 1)'
      - id: tempdir
        source: wsclean_tmpdir
      - id: do_predict
        default: true
    out:
      - id: msout
      - id: logfile
    run: ../../steps/wsclean.cwl
    label: predict_hires
  - id: subtract_hires
    in:
      - id: msin
        source: predict_hires/msout
      - id: command
        default: "set CORRECTED_DATA = CORRECTED_DATA - MODEL_DATA"
    out:
      - id: msout
      - id: logfile
    run: ../../steps/taql_copy.cwl
    label: subtract_hires
    scatter:
      - msin
  - id: image_tmp
    in:
      - id: msin
        source: subtract_hires/msout
      - id: image_name
        source: targetname
        valueFrom: '$(self+"_tmp")'
      - id: image_size
        source: getFWHM/fwhm
        valueFrom: '$((Math.round(2.1 * self * 3600 / 10.)) % 2 > 0 ? [Math.round(2.1 * self * 3600 / 10.) + 1 , Math.round(2.1 * self * 3600 / 10.) + 1] : [Math.round(2.1 * self * 3600 / 10.), Math.round(2.1 * self * 3600 / 10.)])'
      - id: image_scale
        default: '30arcsec'
      - id: use-wgridder
        default: true
      - id: tempdir
        source: wsclean_tmpdir
    out:
      - id: msout
      - id: image
      - id: logfile
    run: ../../steps/wsclean.cwl
    label: image_tmp
  - id: make_beam_reg
    in:
      - id: input_ms
        source: msin
      - id: outfile
        source: targetname
        valueFrom: '$(self+".reg")'
      - id: fwhm
        source: getFWHM/fwhm
    out:
      - id: region
      - id: logfile
    run: ../../steps/make_beam_reg.cwl
    label: make_beam_reg
  - id: blank_image_reg_1
    in:
      - id: input_image
        source: image_tmp/image
      - id: outfile
        source: targetname
        valueFrom: '$(self+"_blank1.fits")'
      - id: region
        source: make_beam_reg/region
      - id: blankval
        default: 0.0
    out:
      - id: image
      - id: output_image
      - id: logfile
    run: ../../steps/blank_image_reg.cwl
    label: blank_image_reg_1
  - id: blank_image_reg_2
    in:
      - id: input_image
        source: blank_image_reg_1/output_image
      - id: outfile
        source: targetname
        valueFrom: '$(self+"_blank2.fits")'
      - id: region
        source: make_beam_reg/region
      - id: blankval
        default: 1.0
      - id: inverse
        default: 1
    out:
      - id: output_image
      - id: logfile
    run: ../../steps/blank_image_reg.cwl
    label: blank_image_reg_2
  - id: image_lowres
    in:
      - id: msin
        source: image_tmp/msout # workaround for CWL bug (https://github.com/common-workflow-language/cwltool/issues/1785)
      - id: image_name
        source: targetname
        valueFrom: '$(self+"_lowres")'
      - id: image_size
        source: getFWHM/fwhm
        valueFrom: '$((Math.round(2.1 * self * 3600 / 10.)) % 2 > 0 ? [Math.round(2.1 * self * 3600 / 10.) + 1 , Math.round(2.1 * self * 3600 / 10.) + 1] : [Math.round(2.1 * self * 3600 / 10.), Math.round(2.1 * self * 3600 / 10.)])'
      - id: image_scale
        default: '30arcsec'
      - id: weighting
        default: 'briggs -0.3'
      - id: no_model_update
        default: true
      - id: niter
        default: 50000
      - id: minuv-l
        default: 30
      - id: parallel-gridding
        default: 4
      - id: maxuvw-m
        default: 6000
      - id: taper-gaussian
        default: '200arcsec'
      - id: baseline_averaging
        default: getFWHM/fwhm
        valueFrom: '$(1.87e3 * 60000. * 2. * Math.PI / (24 * 60 * 60 * Math.round(2.1 * self * 3600 / 10.)) < 1 ? false : 1.87e3 * 60000. * 2. * Math.PI / (24 * 60 * 60 * Math.round(2.1 * self * 3600 / 10.)) > 10 ? 10 : 1.87e3 * 60000. * 2. * Math.PI / (24 * 60 * 60 * Math.round(2.1 * self * 3600 / 10.)))'
      - id: mgain
        default: 0.85
      - id: parallel-deconvolution
        default: 512
      - id: auto_threshold
        default: 1.5
      - id: local_rms
        default: true
      - id: auto_mask
        default: 3.0
      - id: fits_mask
        source: blank_image_reg_2/output_image
      - id: join-channels
        source: total_bandwidth
        valueFrom: '$(Math.round(self/2.e6) > 1)'
      - id: channels-out
        source: total_bandwidth
        valueFrom: '$(Math.round(self/2.e6) > 0 ? Math.round(self/2.e6) : 1)'
      - id: use-wgridder
        default: true
      - id: tempdir
        source: wsclean_tmpdir
    out:
      - id: msout
      - id: model
      - id: image
      - id: logfile
    run: ../../steps/wsclean.cwl
    label: image_lowres
  - id: predict_lowres
    in:
      - id: msin
        source: image_lowres/msout # workaround for CWL bug (https://github.com/common-workflow-language/cwltool/issues/1785)
      - id: image_name
        source: targetname
        valueFrom: '$(self+"_lowres")'
      - id: image_size
        source: getFWHM/fwhm
        valueFrom: '$((Math.round(2.1 * self * 3600 / 10.)) % 2 > 0 ? [Math.round(2.1 * self * 3600 / 10.) + 1 , Math.round(2.1 * self * 3600 / 10.) + 1] : [Math.round(2.1 * self * 3600 / 10.), Math.round(2.1 * self * 3600 / 10.)])'
      - id: image_scale
        default: '30arcsec'
      - id: padding
        default: 1.8
      - id: fits_model
        source: image_lowres/model
      - id: fits_image
        source: image_lowres/image
      - id: channels-out
        source: total_bandwidth
        valueFrom: '$(Math.round(self/2.e6) > 0 ? Math.round(self/2.e6) : 1)'
      - id: tempdir
        source: wsclean_tmpdir
      - id: do_predict
        default: true
    out:
      - id: msout
      - id: logfile
    run: ../../steps/wsclean.cwl
    label: predict_lowres
  - id: subtract_lowres
    in:
      - id: msin
        source: predict_lowres/msout
      - id: command
        default: "set CORRECTED_DATA = CORRECTED_DATA - MODEL_DATA"
    out:
      - id: msout
      - id: logfile
    run: ../../steps/taql.cwl
    label: subtract_lowres
    scatter:
      - msin
  - id: aoflag_residual
    in:
      - id: msin
        source: subtract_lowres/msout
      - id: verbose
        default: true
      - id: concatenate-frequency
        source: aoflag_freqconcat
      - id: strategy
        source: rfistrategy
      - id: reorder
        source: aoflag_reorder
      - id: chunk-size
        source: aoflag_chunksize
    out:
      - id: output_ms
      - id: logfile
    run: ../../steps/aoflag.cwl
    label: aoflag_residual
  - id: image_large
    in:
      - id: msin
        source: aoflag_residual/output_ms
      - id: image_name
        source: targetname
        valueFrom: '$(self+"_large")'
      - id: image_size
        default:
          - 2000
          - 2000
      - id: image_scale
        default: '20arcsec'
      - id: no_fit_beam
        default: true
      - id: circular_beam
        default: true
      - id: beam_size
        default: 180.0
      - id: multiscale
        default: true
      - id: multiscale_scales
        default: '0,4,8,16,32,64'
      - id: weighting
        default: 'briggs -0.3'
      - id: no_model_update
        default: true
      - id: niter
        default: 10000
      - id: minuv-l
        default: 20
      - id: maxuvw-m
        default: 5000
      - id: taper-gaussian
        default: '180arcsec'
      - id: baseline_averaging
        default: getFWHM/fwhm
        valueFrom: '$(1.87e3 * 60000. * 2. * Math.PI / (24 * 60 * 60 * Math.round(2.1 * self * 3600 / 10.)) < 1 ? false : 1.87e3 * 60000. * 2. * Math.PI / (24 * 60 * 60 * Math.round(2.1 * self * 3600 / 10.)) > 10 ? 10 : 1.87e3 * 60000. * 2. * Math.PI / (24 * 60 * 60 * Math.round(2.1 * self * 3600 / 10.)))'
      - id: mgain
        default: 0.85
      - id: parallel-deconvolution
        default: 512
      - id: auto_threshold
        default: 0.5
      - id: local_rms
        default: true
      - id: auto_mask
        default: 1.5
      - id: join-channels
        source: total_bandwidth
        valueFrom: '$(Math.round(self/4.e6) > 1)'
      - id: channels-out
        source: total_bandwidth
        valueFrom: '$(Math.round(self/4.e6) > 0 ? Math.round(self/4.e6) : 1)'
      - id: use-wgridder
        default: true
      - id: tempdir
        source: wsclean_tmpdir
    out:
      - id: image
      - id: msout
      - id: logfile
    run: ../../steps/wsclean.cwl
    label: image_large
  - id: corrupt_model
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: image_large/msout
      - id: parmdb
        source: parmdb
      - id: skymodel_source
        source: skymodel_source
    out:
      - id: msout
      - id: logfile
    scatter:
      - msin
    run: ./corrupt_model.cwl
    label: corrupt_model
  - id: recreate_model
    in:
      - id: msin
        source: corrupt_model/msout
      - id: image_name
        source: targetname
        valueFrom: '$(self+"_mask")'
      - id: image_size
        source: getFWHM/fwhm
        valueFrom: '$((Math.round(2.1 * self * 3600 / 10.)) % 2 > 0 ? [Math.round(2.1 * self * 3600 / 10.) + 1 , Math.round(2.1 * self * 3600 / 10.) + 1] : [Math.round(2.1 * self * 3600 / 10.), Math.round(2.1 * self * 3600 / 10.)])'
      - id: image_scale
        default: '10arcsec'
      - id: fits_model
        source: image_hires/model
      - id: channels-out
        source: total_bandwidth
        valueFrom: '$(Math.round(self/4.e6) > 0 ? Math.round(self/4.e6) : 1)'
      - id: use-wgridder
        default: true
      - id: tempdir
        source: wsclean_tmpdir
    out:
      - id: msout
      - id: logfile
    run: ../../steps/wsclean.cwl
    label: recreate_model
  - id: concat_logfiles_predict
    in:
      - id: file_list
        source:
          - make_mask_image/logfile
          - make_mask/logfile
          - blank_image_reg/logfile
        linkMerge: merge_flattened
        pickValue: all_non_null
      - id: file_prefix
        default: selfcal_predict
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_makemask
  - id: merge_array_files_hires
    in:
      - id: input
        source: subtract_hires/logfile
    out:
      - id: output
    run: ../../steps/merge_array_files.cwl
    label: merge_array_files_hires
  - id: merge_array_files_lowres
    in:
      - id: input
        source: subtract_lowres/logfile
    out:
      - id: output
    run: ../../steps/merge_array_files.cwl
    label: merge_array_files_lowres
  - id: concat_logfiles_hires
    in:
      - id: file_list
        source:
          - image_hires/logfile
          - predict_hires/logfile
          - merge_array_files_hires/output
        linkMerge: merge_flattened
      - id: file_prefix
        default: selfcal_subtract_hires
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_subtract
  - id: concat_logfiles_lowres
    in:
      - id: file_list
        source:
          - image_tmp/logfile
          - make_beam_reg/logfile
          - blank_image_reg_1/logfile
          - blank_image_reg_2/logfile
          - image_lowres/logfile
          - predict_lowres/logfile
          - merge_array_files_lowres/output
        linkMerge: merge_flattened
      - id: file_prefix
        default: selfcal_subtract_lowres
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_lowres
  - id: concat_logfiles_aoflag
    in:
      - id: file_list
        source:
          - aoflag_residual/logfile
        linkMerge: merge_flattened
      - id: file_prefix
        default: aoflag_residual
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_aoflag
  - id: concat_logfiles_large
    in:
      - id: file_list
        source:
          - image_large/logfile
        linkMerge: merge_flattened
      - id: file_prefix
        default: selfcal_large
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_large
  - id: concat_logfiles_corrupt
    in:
      - id: file_list
        source:
          - corrupt_model/logfile
        linkMerge: merge_flattened
      - id: file_prefix
        default: corrupt_model
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_corrupt
  - id: concat_logfiles_recreate
    in:
      - id: file_list
        source:
          - recreate_model/logfile
        linkMerge: merge_flattened
      - id: file_prefix
        default: recreate_model
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_recreate
requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement