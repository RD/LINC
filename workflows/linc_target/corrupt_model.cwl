class: Workflow
cwlVersion: v1.2
id: corrupt_model
label: corrupt_model
inputs:
  - id: msin
    type: Directory
  - id: max_dp3_threads
    type: int?
  - id: parmdb
    type: File
  - id: skymodel_source
    type: string?
    default: 'GSM'
outputs:
  - id: msout
    outputSource:
      - subtract_model/msout
    type: Directory
  - id: logfile
    outputSource: concat_logfiles_corrupt/output
    type: File

steps:
  - id: corrupt_slowtec
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: msin
      - id: msin_datacolumn
        default: MODEL_DATA
      - id: parmdb
        source: parmdb
      - id: msout_datacolumn
        default: MODEL_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: correction
        source:
          - skymodel_source
        valueFrom: $("slow"+self+"tec1")
      - id: solset
        default: target
      - id: missingantennabehavior
        default: 'unit'
      - id: corrupt
        default: true
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    label: corrupt_slowtec
  - id: corrupt_tec
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: corrupt_slowtec/msout
      - id: msin_datacolumn
        default: MODEL_DATA
      - id: parmdb
        source: parmdb
      - id: msout_datacolumn
        default: MODEL_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: correction
        source:
          - skymodel_source
        valueFrom: $(self+"tec1")
      - id: solset
        default: target
      - id: corrupt
        default: true
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    label: corrupt_tec
  - id: corrupt_fr
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: corrupt_tec/msout
      - id: msin_datacolumn
        default: MODEL_DATA
      - id: parmdb
        source: parmdb
      - id: msout_datacolumn
        default: MODEL_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: correction
        default: "faraday"
      - id: solset
        default: target
      - id: corrupt
        default: true
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    label: corrupt_fr
  - id: corrupt_amp
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: corrupt_fr/msout
      - id: msin_datacolumn
        default: MODEL_DATA
      - id: parmdb
        source: parmdb
      - id: msout_datacolumn
        default: MODEL_DATA
      - id: storagemanager
        default: Dysco
      - id: databitrate
        default: 0
      - id: correction
        source: skymodel_source
        valueFrom: $(self+"amplitude")
      - id: solset
        default: target
      - id: corrupt
        default: true
    out:
      - id: msout
      - id: logfile
    run: ../../steps/applycal.cwl
    label: corrupt_amp
  - id: subtract_model
    in:
      - id: msin
        source: corrupt_amp/msout
      - id: command
        default: "set CORRECTED_DATA = DATA - MODEL_DATA"
    out:
      - id: msout
      - id: logfile
    run: ../../steps/taql.cwl
    label: subtract_model
  - id: concat_logfiles_corrupt
    in:
      - id: file_list
        source:
          - corrupt_slowtec/logfile
          - corrupt_tec/logfile
          - corrupt_fr/logfile
          - corrupt_amp/logfile
          - subtract_model/logfile
        linkMerge: merge_flattened
      - id: file_prefix
        default: corrupt_model
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_corrupt
requirements: []
