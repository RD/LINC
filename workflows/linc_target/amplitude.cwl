class: Workflow
cwlVersion: v1.2
id: losoto_amplitude
label: losoto_amplitude
inputs:
  - id: input_h5parm
    type: File
outputs:
  - id: output_h5parm
    outputSource:
      - norm/output_h5parm
    type: File
  - id: logfiles
    outputSource:
      - bkp/log
      - replicate/logfile
      - norm/logfile
    type: File[]
    linkMerge: merge_flattened
steps:
  - id: bkp
    in:
      - id: input_h5parm
        source: input_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: soltabOut
        default: amplitudeSmooth
    out:
      - id: output_h5parm
      - id: log
    run: ../../steps/LoSoTo.Duplicate.cwl
  - id: replicate
    in:
      - id: input_h5parm
        source: bkp/output_h5parm
      - id: soltab
        default: sol000/amplitudeSmooth
      - id: axisReplicate
        default: ant
      - id: fromCell
        default: first
    out:
      - id: output_h5parm
      - id: logfile
    run: ../../steps/LoSoTo.Replicate.cwl
    label: replicate
  - id: norm
    in:
      - id: input_h5parm
        source: replicate/output_h5parm
      - id: soltab
        default: sol000/amplitude000
      - id: normVal
        default: 1.0
      - id: axesToNorm
        default:
          - time
          - freq
          - ant
      - id: log
        default: true
    out:
      - id: output_h5parm
      - id: logfile
    run: ../../steps/LoSoTo.Norm.cwl
requirements: []
