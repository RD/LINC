class: Workflow
cwlVersion: v1.2
id: fr
label: FR_targ
inputs:
  - id: msin
    type: Directory[]
  - id: skymodel
    type:
      - File
      - Directory
  - id: refant
    type: string?
    default: 'CS001LBA'
  - id: max_dp3_threads
    type: int?
  - id: propagatesolutions
    type: boolean?
  - id: antennaconstraint
    type: string[]?
  - id: insolutions
    type: File
  - id: process_baselines_target
    type: string?
    default: '[CR]S*&'
  - id: bad_antennas
    type: string?
    default: '[CR]S*&'
  - id: instrument
    type: string?
    default: 'LBA'
outputs:
  - id: outsolutions
    outputSource:
      - write_solutions/outh5parm
    type: File
  - id: inspection
    outputSource:
      - losoto_plot_P3/output_plots
      - losoto_plot_Pr/output_plots
      - losoto_plot_fr/output_plots
    type: File[]
    linkMerge: merge_flattened
  - id: logfile
    outputSource:
      - concat_logfiles_addcol/output
      - concat_logfiles_calib_targ/output
      - concat_logfiles_fr/output
      - concat_logfiles_model/output
      - concat_logfiles_predict/output
    type: File[]
  - id: msout
    outputSource:
      - predict_calibrate_fr/msout
    type: Directory[]
steps:
  - id: predict_calibrate_fr
    in:
      - id: max_dp3_threads
        source: max_dp3_threads
      - id: msin
        source: msin
      - id: skymodel
        source: skymodel
      - id: propagatesolutions
        source: propagatesolutions
      - id: antennaconstraint
        source: antennaconstraint
      - id: instrument
        source: instrument
    out:
      - id: msout
      - id: add_corrected_col.log
      - id: predict_targ.log
      - id: model_targ.log
      - id: calib_targ.log
      - id: outh5parm
    run: ./predict_calibrate_fr.cwl
    scatter:
      - msin
  - id: h5parm_collector
    in:
      - id: h5parmFiles
        source:
          - predict_calibrate_fr/outh5parm
        linkMerge: merge_flattened
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: clobber
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/H5ParmCollector.cwl
    label: H5parm_collector
  - id: faraday_rot
    in:
      - id: refAnt
        source: refant
      - id: input_h5parm
        source: h5parm_collector/outh5parm
      - id: soltabname
        default: rotation000
    out:
      - id: output_h5parm
      - id: logfiles
    run: ../linc_calibrator/faraday_rotation.cwl
    label: FaradayRot
  - id: losoto_plot_P3
    in:
      - id: input_h5parm
        source: add_missing_stations/outh5parm
      - id: soltab
        default: sol000/phaseOrig
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: pol
        default: XX
      - id: prefix
        default: fr_ph_
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_P3
  - id: losoto_plot_Pr
    in:
      - id: input_h5parm
        source: add_missing_stations/outh5parm
      - id: soltab
        default: sol000/phase000
      - id: axesInPlot
        default:
          - time
          - freq
      - id: axisInTable
        default: ant
      - id: axisDiff
        default: pol
      - id: minmax
        default:
          - -3.14
          - 3.14
      - id: plotFlag
        default: true
      - id: refAnt
        source: refant
      - id: prefix
        default: fr_ph_res
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_Pd
  - id: losoto_plot_fr
    in:
      - id: input_h5parm
        source: add_missing_stations/outh5parm
      - id: soltab
        default: sol000/faraday
      - id: axesInPlot
        default:
          - time
      - id: axisInTable
        default: ant
      - id: refAnt
        source: refant
      - id: prefix
        default: fr
    out:
      - id: output_plots
      - id: logfile
      - id: parset
    run: ../../steps/LoSoTo.Plot.cwl
    label: losoto_plot_fr
  - id: add_missing_stations
    in:
      - id: h5parm
        source: faraday_rot/output_h5parm
      - id: refh5parm
        source: insolutions
      - id: solset
        default: sol000
      - id: refsolset
        default: target
      - id: soltab_in
        default: rotation000
      - id: soltab_out
        default: faraday
      - id: filter
        source: process_baselines_target
      - id: bad_antennas
        source: bad_antennas
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/add_missing_stations.cwl
    label: add_missing_stations
  - id: write_solutions
    in:
      - id: h5parmFile
        source: add_missing_stations/outh5parm
      - id: outsolset
        default: target
      - id: insoltab
        default: faraday
      - id: input_file
        source: insolutions
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: history
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parmcat.cwl
    label: write_solutions
  - id: concat_logfiles_fr
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - h5parm_collector/log
          - faraday_rot/logfiles
          - losoto_plot_P3/logfile
          - losoto_plot_Pr/logfile
          - losoto_plot_fr/logfile
          - add_missing_stations/log
          - write_solutions/log
      - id: file_prefix
        default: losoto_FR_targ
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_FR
  - id: concat_logfiles_predict
    in:
      - id: file_list
        source:
          - predict_calibrate_fr/predict_targ.log
      - id: file_prefix
        default: predict_targ
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_predict
  - id: concat_logfiles_calib_targ
    in:
      - id: file_list
        source:
          - predict_calibrate_fr/calib_targ.log
        linkMerge: merge_flattened
      - id: file_prefix
        default: calib_targ_fr
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_calib_targ
  - id: concat_logfiles_addcol
    in:
      - id: file_list
        source:
          - predict_calibrate_fr/add_corrected_col.log
      - id: file_prefix
        default: add_corrected_col
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_addcol
  - id: concat_logfiles_model
    in:
      - id: file_list
        source:
          - predict_calibrate_fr/model_targ.log
        linkMerge: merge_flattened
      - id: file_prefix
        default: model_targ_fr
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_model
requirements:
  - class: SubworkflowFeatureRequirement
  - class: ScatterFeatureRequirement
  - class: MultipleInputFeatureRequirement
