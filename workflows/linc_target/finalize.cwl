class: Workflow
cwlVersion: v1.2
id: finalize
label: finalize
inputs:
  - id: refant
    type: string?
    default: 'CS001HBA0'
  - id: min_unflagged_fraction
    type: float?
    default: 0.5
  - id: removed_bands
    type: string[]?
    default: []
  - id: demix_sources
    type: string[]?
    default: []
  - id: demix
    type: boolean?
    default: false
  - id: clip_sources
    type: string[]?
    default: []
  - id: msin
    type: Directory[]
  - id: msin_hires
    type: Directory[]
  - id: input_h5parm
    type: File
  - id: inh5parm_logfile
    type: File
  - id: gsmcal_step
    type: string?
    default: 'phase'
  - id: process_baselines_target
    type: string?
    default: '[CR]S*&'
  - id: filter_baselines
    type: string?
    default: '[CR]S*&'
  - id: bad_antennas
    type: string?
    default: ''
  - id: insolutions
    type: File
  - id: compression_bitrate
    type: int?
    default: 16
  - id: skymodel_source
    type: string?
    default: 'TGSS'
  - id: total_bandwidth
    type: float?
    default: 50
  - id: check_Ateam_separation.json
    type: File
  - id: compare_stations_filter
    type: string?
    default: '[CR]S*&'
  - id: flags
    type: File[]
  - id: targetname
    type: string?
    default: 'pointing'
  - id: wsclean_tmpdir
    type: string?
    default: '/tmp'
  - id: make_structure_plot
    type: boolean?
    default: true
  - id: apply_RM
    type: boolean?
    default: true
  - id: ionex_prefix
    type: string?
    default: UQRG
  - id: apply_solutions_fullres
    type: boolean?
    default: false
  - id: groupnames
    type: string[]
  - id: filenames
    type: File
  - id: chunkduration
    type: float?
    default: 0.0
outputs:
  - id: msout
    outputSource:
      - apply_gsmcal_final/msout
      - merge_array_concat_hires/output
    type: Directory[]
    pickValue: all_non_null
    linkMerge: merge_flattened
  - id: solutions
    outputSource:
      - h5parm_pointingname/outh5parm
    type: File
  - id: logfiles
    outputSource:
      - concat_logfiles_applygsm_final/output
      - concat_logfiles_solutions/output
      - concat_logfiles_structure/output
      - concat_logfiles_wsclean/output
      - concat_logfiles_summary/output
      - concat_logfiles_uvplot/output
      - concat_logfiles_hires/output
    type: File[]
    linkMerge: merge_flattened
    pickValue: all_non_null
  - id: inspection
    outputSource:
      - structure_function/output_plot
      - wsclean/image
      - uvplot/output_image
    type: File[]
    linkMerge: merge_flattened
    pickValue: all_non_null
  - id: summary_file
    outputSource:
      - summary/summary_file
    type: File
steps:
  - id: add_missing_stations
    in:
      - id: h5parm
        source: input_h5parm
      - id: refh5parm
        source: insolutions
      - id: solset
        default: sol000
      - id: refsolset
        default: target
      - id: soltab_in
        source: gsmcal_step
        valueFrom: $(self == "phase" || self == "scalarphase" ? 'phase000':self+'000')
      - id: soltab_out
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $(self.join('')+"_final")
      - id: filter
        source: process_baselines_target
      - id: bad_antennas
        source: filter_baselines
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/add_missing_stations.cwl
    label: add_missing_stations
  - id: apply_gsmcal_final
    in:
      - id: msin
        source: msin
      - id: msout_name
        linkMerge: merge_flattened
        source:
          - msin
        valueFrom: $(self.nameroot+'_pre-cal.ms')
      - id: msin_datacolumn
        default: DATA
      - id: parmdb
        source: write_solutions/outh5parm
      - id: msout_datacolumn
        default: DATA
      - id: correction
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $(self.join('')+"_final")
      - id: solset
        default: target
      - id: storagemanager
        default: Dysco
      - id: databitrate
        source: compression_bitrate
    out:
      - id: msout
      - id: flagged_fraction_dict
      - id: logfile
    run: ../../steps/applytarget.cwl
    label: apply_gsmcal_final
    scatter:
      - msin
      - msout_name
    scatterMethod: dotproduct
  - id: check_removed_groups
    in:
      - id: groups_specification
        source: filenames
      - id: groupnames
        source: groupnames
      - id: removed_bands
        source: removed_bands
    out:
      - id: out_groups_specification
      - id: out_groupnames
    run: ../../steps/check_removed_groups.cwl
    label: check_removed_groups
  - id: apply_and_concat_hires
    in:
      - id: msin
        source: 
          - msin_hires
      - id: baselines
        source: bad_antennas
      - id: group_id
        source: check_removed_groups/out_groupnames
      - id: groups_specification
        source: check_removed_groups/out_groups_specification
      - id: filter_baselines
        source: process_baselines_target
      - id: chunkduration
        source: chunkduration
      - id: parmdb
        source: write_solutions/outh5parm
      - id: correction
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $(self.join('')+"_final")
      - id: solset
        default: target
      - id: databitrate
        source: compression_bitrate
      - id: steps
        default: 'filter,flagtransfer,applycal,count'
      - id: apply_solutions_fullres
        source: apply_solutions_fullres
      - id: flag_transfer_source_ms
        source: msin
    out:
      - id: msout
      - id: flagged_fraction_dict
      - id: dp3concat.log
    run: ./concat.cwl
    label: apply_and_concat_hires
    scatter:
      - group_id
      - flag_transfer_source_ms
    scatterMethod: dotproduct
    when: $(inputs.apply_solutions_fullres)
  - id: merge_array_concat_hires
    in:
      - id: input
        source:
          - apply_and_concat_hires/msout
        pickValue: all_non_null
      - id: apply_solutions_fullres
        source: apply_solutions_fullres
    out:
      - id: output
    run: ../../steps/merge_array.cwl
    label: merge_array_concat_hires
    when: $(inputs.apply_solutions_fullres)
  - id: final_flags_join
    in:
      - id: flagged_fraction_dict
        source:
          - apply_gsmcal_final/flagged_fraction_dict
      - id: filter_station
        default: ''
      - id: state
        default: 'final'
    out:
      - id: flagged_fraction_antenna
    run: ./../../steps/findRefAnt_join.cwl
    label: final_flags_join
  - id: average
    in:
      - id: msin
        source: apply_gsmcal_final/msout
      - id: msout_name
        linkMerge: merge_flattened
        source:
          - apply_gsmcal_final/msout
        valueFrom: $(self.nameroot+'_wsclean.ms')
      - id: msin_datacolumn
        default: DATA
      - id: overwrite
        default: false
      - id: storagemanager
        default: Dysco
      - id: avg_timestep
        default: 2
      - id: avg_freqstep
        default: 2
    out:
      - id: msout
      - id: logfile
    run: ../../steps/average.cwl
    label: average
    scatter:
      - msin
      - msout_name
    scatterMethod: dotproduct
  - id: summary
    in:
      - id: flagFiles
        source: 
          - flags
          - final_flags_join/flagged_fraction_antenna
        linkMerge: merge_flattened
      - id: pipeline
        default: 'LINC'
      - id: run_type
        default: 'target'
      - id: filter
        source: process_baselines_target
      - id: bad_antennas
        source:
          - filter_baselines
          - compare_stations_filter
        valueFrom: $(self.join(''))
      - id: Ateam_separation_file
        source: check_Ateam_separation.json
      - id: solutions
        source: h5parm_pointingname/outh5parm
      - id: clip_sources
        source: clip_sources
        valueFrom: "$(self.join(','))"
      - id: demix
        source: demix
        valueFrom: '$(self ? "True" : "False")'
      - id: demix_sources
        source: demix_sources
        valueFrom: "$(self.join(','))"
      - id: removed_bands
        source: removed_bands
        valueFrom: "$(self.join(','))"
      - id: min_unflagged_fraction
        source: min_unflagged_fraction
      - id: refant
        source: refant
      - id: ionex
        source:
          - apply_RM
          - ionex_prefix
        valueFrom: '$(self[0] ? self[1] : false)'
    out:
      - id: summary_file
      - id: logfile
    run: ../../steps/summary.cwl
    label: summary
  - id: uvplot
    in:
      - id: MSfiles
        source: apply_gsmcal_final/msout
      - id: output_name
        source: targetname
        valueFrom: $(self)_uv-coverage.png
      - id: title
        source: targetname
        valueFrom: '"uv coverage of the target pointing: $(self)"'
      - id: timeslots
        default: "0,20,0"
      - id: wideband
        default: true
    out:
      - id: output_image
      - id: logfile
    run: ../../steps/uvplot.cwl
    label: uvplot
  - id: wsclean
    in:
      - id: msin
        source:
          - average/msout
      - id: image_scale
        default: 15asec
      - id: auto_threshold
        default: 5
      - id: image_size
        default: [2500,2500]
      - id: niter
        default: 10000
      - id: nmiter
        default: 5
      - id: multiscale
        default: true
      - id: use-wgridder
        default: true
      - id: mgain
        default: 0.8
      - id: parallel-deconvolution
        default: 1500
      - id: parallel-reordering
        default: 4
      - id: channels-out
        source: total_bandwidth
        valueFrom: '$(Math.round(self/1e7) < 1 ? 1 : Math.round(self/1e7))'
      - id: join-channels
        source: total_bandwidth
        valueFrom: $(Math.round(self/1e7) > 1)
      - id: deconvolution-channels
        source: total_bandwidth
        valueFrom: '$(Math.round(self/1e7) > 1 ? (Math.round(self/1e7) > 3 ? 3 : Math.round(self/1e7)) : false )'
      - id: fit-spectral-pol
        source: total_bandwidth
        valueFrom: '$(Math.round(self/1e7) > 1 ? (Math.round(self/1e7) > 3 ? 3 : Math.round(self/1e7)) : false )'
      - id: taper-gaussian
        default: 40asec
      - id: weighting
        default: briggs -0.5
      - id: maxuvw-m
        default: 20000
      - id: tempdir
        source: wsclean_tmpdir
      - id: image_name
        source: targetname
      - id: no_model_update
        default: true
    out:
      - id: dirty_image
      - id: image
      - id: logfile
    run: ../../steps/wsclean.cwl
    label: wsclean
  - id: merge_array_files
    in:
      - id: input
        source:
          - apply_gsmcal_final/logfile
    out:
      - id: output
    run: ../../steps/merge_array_files.cwl
    label: merge_array_files
  - id: write_solutions
    in:
      - id: h5parmFile
        source: add_missing_stations/outh5parm
      - id: outsolset
        default: target
      - id: insoltab
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $(self.join('')+"_final")
      - id: input_file
        source: insolutions
      - id: squeeze
        default: true
      - id: verbose
        default: true
      - id: history
        default: true
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parmcat.cwl
    label: write_solutions
  - id: h5parm_pointingname
    in:
      - id: h5parmFile
        source: write_solutions/outh5parm
      - id: solsetName
        default: target
      - id: pointing
        source: targetname
    out:
      - id: outh5parm
      - id: log
    run: ../../steps/h5parm_pointingname.cwl
    label: h5parm_pointingname
  - id: structure_function
    in:
      - id: input_h5parm
        source: write_solutions/outh5parm
      - id: soltab
        source:
          - skymodel_source
          - gsmcal_step
        valueFrom: $('target/'+self.join('')+"_final")
      - id: plotName
        source: targetname
        valueFrom: $(self+'_structure.png')
      - id: execute
        source: make_structure_plot
    out:
      - id: output_plot
      - id: logfile
    run: ../../steps/LoSoTo.Structure.cwl
    label: structure_function
    when: $(inputs.soltab.includes("phase") && inputs.execute)
  - id: concat_logfiles_applygsm_final
    in:
      - id: file_list
        source:
          - merge_array_files/output
      - id: file_prefix
        default: apply_gsmcal_final
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_applygsm_final
  - id: concat_logfiles_solutions
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - inh5parm_logfile
          - add_missing_stations/log
          - write_solutions/log
          - h5parm_pointingname/log
      - id: file_prefix
        default: losoto_gsmcal_final
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_solutions
  - id: concat_logfiles_structure
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - structure_function/logfile
        pickValue: all_non_null
      - id: file_prefix
        source:
          - targetname
          - gsmcal_step
        valueFrom: $(self.join('_')+'_structure')
      - id: execute
        source: make_structure_plot
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_structure
    when: $(inputs.file_prefix.includes("phase") && inputs.execute)
  - id: concat_logfiles_wsclean
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - wsclean/logfile
      - id: file_prefix
        default: wsclean
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_wsclean
  - id: concat_logfiles_hires
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - apply_and_concat_hires/dp3concat.log
        pickValue: all_non_null
      - id: file_prefix
        default: apply_and_concat_hires
      - id: apply_solutions_fullres
        source: apply_solutions_fullres
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_hires
    when: $(inputs.apply_solutions_fullres)
  - id: concat_logfiles_summary
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - summary/logfile
      - id: file_prefix
        source: targetname
        valueFrom: $(self+'_summary')
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_summary
  - id: concat_logfiles_uvplot
    in:
      - id: file_list
        linkMerge: merge_flattened
        source:
          - uvplot/logfile
      - id: file_prefix
        default: uvplot
    out:
      - id: output
    run: ../../steps/concatenate_files.cwl
    label: concat_logfiles_uvplot
requirements:
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement
  - class: ScatterFeatureRequirement
  - class: MultipleInputFeatureRequirement
  - class: SubworkflowFeatureRequirement
