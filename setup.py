import os
from setuptools import setup

# Unfortunately, pyproject.toml does not support the keyword `data_files`, and
# it never will. Its use is discouraged, because installing files outside the
# scope of a Python package is strongly discouraged. However, the keyword
# itself is not deprecated, and can still be used in setup.py and setup.cfg.

data_files = []
for top in ("rfistrategies", "skymodels", "solutions", "steps", "workflows"):
    for root, _, files in os.walk(top):
        data_files.append(
            (
                os.path.join("share", "linc", root),
                [os.path.join(root, f) for f in files],
            )
        )

setup(data_files=data_files)
