# Developer notes

## Creating a release

Creating a LINC release is a bit more involved than simply pressing a button in the GitLab GUI. The main reason is that all the CWL files that contain a `dockerPull` line need to be updated on-the-fly to contain the proper reference to the release. This is taken care of by the CI/CD pipeline, but it is good to understand how this pipeline works under the hood. First, let's properly configure the GitLab project.

### Configure the project in the GitLab GUI

The CI/CD pipelines needs to have push rights to the Git repository. We use an access token to grant these rights. The following preparations need to be done only once (though they have to be repeated when the access token expires):

- Create a project access token (`Settings > Access tokens`), and grant it the `Maintainer` role and the `write_repository` scope. Copy the token directly after you've created it, because there is no way to retrieve it later on.
- Create a project variable (`Settings > CI/CD > Variables`) named `GIT_PUSH_TOKEN`, and set its value to the access token that you just created (and copied!). Mark the variable as masked and protected. This variable will be used when a release is created (see `.gitlab-ci.yml` file).

**NOTE**: The name of the access token will be used in the `Created by` column in the `Pipelines` view for pipelines that were triggered as a result of an automatic `git push` action.

### What does the CI/CD pipeline consider a release?

The CI/CD pipeline will consider every branch whose name starts with `releases/` a release branch. To create a release named `<my-release>`, you need to manually create a branch named `releases/<my-release>`.

**NOTE**:`<my-release>` must be a valid name according to the [PEP-440 versioning scheme](
https://peps.python.org/pep-0440/#version-scheme). For example, `v5.0rc1` is a valid name, whereas `ldv_404` is not. However, you can add a local identifier to the name, e.g. `v5.0rc1+ldv_404`.

### What does the CI/CD pipeline do with a release branch?

When the CI/CD pipeline is run on a release branch, it will, in addition to a normal CI/CD pipeline run, do the following:

- update all the CWL files containing a `dockerPull` line with the correct image tag, and commit the changes
- tag the release using `<my-release>` as tag
- deploy the docker image to [Docker Hub](https://hub.docker.com/repository/docker/astronrd/linc)
- if `<my-release>` is a versioned release:[^1]
  - undo the changes to the CWL files, and commit
  - merge the changes into the default branch

[^1]: A versioned release is a release whose name starts with a `V` or `v`.

The rationale behind the two extra steps for a versioned release is that we want the git tag to be visible on the default branch, so that `git describe --tags` will return the desired result.

**NOTE**: If a tag `<my-release>` already exists, the pipeline will bail out with an error. In order to let it run successfully, the tag `<my-release>` needs to be removed manually in the GitLab GUI. This is to avoid that an existing release will be clobbered!

### Creating the release in the GitLab GUI

Once the CI/CD pipeline has run, and the new release has been tagged, a new release can be created in the GitLab GUI. Navigate to "Deploy | Releases", click on the "Create new release" button, and proceed as you would normally do when creating a new release.
