#!/bin/sh
#
# Build a virtual environment that can be used to run the LINC pipeline

python3 -m venv venv
. venv/bin/activate

python3 -m pip install --upgrade pip
python3 -m pip install --upgrade wheel

python3 -m pip install -r requirements.txt

