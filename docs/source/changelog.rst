.. _changelog:

Changelog
=========

Version 5.0
-----------

    * New target workflow for LBA observations
    * New self-calibration mode for HBA observations
    * Better control of resources used
    * Optimised performance and flagging strategies for `aoflagger`_ implemented
    * Added options to skip Ateamclipping or plotting the structure function
    * Use `GSSC`_ instead of `CODE`_ as a default source for `RMextract`_.

Version 4.0
-----------

    * First re-written version of the pipeline based on the `Common Workflow Language`_ (CWL)
    * Additional diagnostics (UV-plots and summary file) and improved logging
    * Full software is based on python3
    * Easy-to-use with integrated Docker support
    * Pipeline was renamed into LOFAR Initial Calibration (LINC) Pipeline
    * Semi-automated demixing and clipping procedure
    * Documentation moved to https://linc.readthedocs.io/


Version 3.2 (last version based on the `genericpipeline`_ framework)
--------------------------------------------------------------------

    * Several bugfixes and minor improvements
    * Added high-resolution models for increased compatibility if using data including long baselines
    * Option to add back missing stations for the use with long-baseline data
    * Automated selection of the reference antenna

Version 3.0
-----------

    * Major overhaul of calibrator and target pipelines to support LBA and HBA data
    * Addition of production versions of calibrator, target, and image pipelines
    * Documentation moved to http://www.astron.nl/citt/prefactor

Version 2.0
-----------

    * applying Ionospheric RM corrections
    * grouping of subbands by actual frequency
    * speed and disk usage improvements by optimized usage of DP3
    * (optional) wide-band cleaning in Initial-Subtract
    * more diagnostic plots
    * documentation moved to GitHib wiki: https://github.com/lofar-astron/prefactor/wiki
    * automatic selection of calibrator sky model
    * automatic generation of TGSS sky model

Version 1.0
-----------

    * First release of the Pre-Facet-Calibration pipeline

.. _Common Workflow Language: https://www.commonwl.org/
.. _genericpipeline:  https://www.astron.nl/citt/genericpipeline/
.. _aoflagger: https://gitlab.com/aroffringa/aoflagger.git
.. _RMextract: https://github.com/lofar-astron/RMextract.git
.. _GSSC: ftp://gssc.esa.int/gnss/products/ionex/
.. _CODE: http://ftp.aiub.unibe.ch/CODE/