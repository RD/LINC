.. _help:

Getting help
============

**LINC** is a continuously maintained and developed software package.

If you need help please check all open and closed issues or report new issues on the `LINC issues`_ page.

.. _LINC issues: https://git.astron.nl/RD/LINC/-/issues?scope=all&state=all