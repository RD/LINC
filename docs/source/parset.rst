.. toctree::
   :hidden:
 
   parset_old
   
.. _parset:

Configuring **LINC**
=====================
.. note::

   If you are running the deprecated genericpipeline version of the pipeline (**prefactor** 3.2 or older), please check the :doc:`old instructions page<parset_old>`.
    
Preparing the configuration file
--------------------------------

The inputs for **LINC** are provided in the `JSON format`_. The only required input is the input data to process (for the calibrator pipeline).
A minimum input file may look like this::

    {
        "msin": [
                    {"class": "Directory", "path": "3C286/L228161_SB000_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB001_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB002_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB003_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB004_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB005_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB006_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB007_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB008_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB009_uv.MS"}
                ],
    }

There are more parameters you may want to adjust that can be added to this input JSON file. This is how an input looks like that uses all defaults of the HBA calibrator pipeline::

    {
        "msin": [
                    {"class": "Directory", "path": "3C286/L228161_SB000_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB001_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB002_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB003_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB004_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB005_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB006_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB007_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB008_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB009_uv.MS"}
                ],
        "refant": "CS00.*",
        "flag_baselines": [],
        "process_baselines_cal": "*&",
        "filter_baselines": "*&",
        "fit_offset_PA": false,
        "do_smooth": false,
        "rfi_strategy": "$LINC_DATA_ROOT/rfistrategies/lofar-hba-wideband.lua",
        "max2interpolate": 30,
        "ampRange": [0,0],
        "skip_international": true,
        "raw_data": false,
        "propagatesolutions": true,
        "flagunconverged": false,
        "maxStddev": -1.0,
        "solutions2transfer": null,
        "antennas2transfer": "[FUSPID].*",
        "do_transfer": false,
        "trusted_sources": "3C48,3C147,3C196,3C295,3C380",
        "demix_sources": ["VirA_4_patch", "CygAGG", "CasA_4_patch", "TauAGG"],
        "demix_freqres": "48.82kHz",
        "demix_timeres": 10,
        "demix": false,
        "ion_3rd": false,
        "clock_smooth": true,
        "tables2export": "clock",
        "max_dp3_threads": 10,
        "memoryperc": 20,
        "min_separation": 30,
        "max_separation_arcmin": 1.0,
        "calibrator_path_skymodel": null,
        "A-Team_skymodel": null,
        "avg_timeresolution": 4,
        "avg_freqresolution": "48.82kHz",
        "bandpass_freqresolution": "195.3125kHz",
        "lbfgs_historysize" : 10,
        "lbfgs_robustdof" : 200,
        "aoflag_reorder" : false,
        "aoflag_chunksize" : 2000,
        "remove_phase_wraps": false,
        "maxncpu_flag": 10,
        "instrument": "HBA"
    }

If you just want to alter one of the defaults it is sufficient to override it by specifing its new value the JSON input file::

    {
        "msin": [
                    {"class": "Directory", "path": "3C286/L228161_SB000_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB001_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB002_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB003_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB004_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB005_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB006_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB007_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB008_uv.MS"},
                    {"class": "Directory", "path": "3C286/L228161_SB009_uv.MS"}
                ],
        "demix": true,
    }

If you run the target pipeline you also need to provide the calibrator solution set::

    {
        "msin": [
                    {"class": "Directory", "path": "?????/L228163_SB000_uv.MS"},
                    {"class": "Directory", "path": "?????/L228163_SB001_uv.MS"},
                    {"class": "Directory", "path": "?????/L228163_SB002_uv.MS"},
                    {"class": "Directory", "path": "?????/L228163_SB003_uv.MS"},
                    {"class": "Directory", "path": "?????/L228163_SB004_uv.MS"},
                    {"class": "Directory", "path": "?????/L228163_SB005_uv.MS"},
                    {"class": "Directory", "path": "?????/L228163_SB006_uv.MS"},
                    {"class": "Directory", "path": "?????/L228163_SB007_uv.MS"},
                    {"class": "Directory", "path": "?????/L228163_SB008_uv.MS"},
                    {"class": "Directory", "path": "?????/L228163_SB009_uv.MS"}
                ],
        "cal_solutions": {"class": "File", "path": "cal_solutions.h5"}
    }

A detailed description of the input parameters can be found in the :doc:`calibrator` and :doc:`target` section.

.. _JSON format: https://www.json.org/json-en.html
